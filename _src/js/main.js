/*=========================================================================================
// INICIO MAIN JS
========================================================================================= */
jQuery(function ($) {
  $(document).ready(function () {



    //menu-toggle
    $(".topo__toggle").click(function (event) {
      event.preventDefault();
      if ($(this).hasClass('on')) {
        $(this).removeClass('on');
        // $(".menu").stop().fadeOut();
      } else {
        $(this).addClass('on');
        // $(".menu").stop().fadeIn();
      }
    });


    $('.btn-resp-rdp').on('click', function (e) {
      e.preventDefault();

      if ($(this).text() == "Exibir Menu") {
        $(this).text("Ocultar Menu");
      } else {
        $(this).text("Exibir Menu");
      }

      $(".rdp__menus").slideToggle();
    });

    $(".menu__nav").mouseenter(function () {
      $(".menu-item").addClass(".sub-menu");
    });

    // PREENCHE CAMPO NOME DO EMPREENDIMENTO
    var input = $('input[name="nome-emp"]');
    var form = input.closest('.form');
    var titEmp = form.data('emp');

    input.each(function (item, el) {
      $(el).val(titEmp);
    });

    /*=========================================================================================
    // LOGIN FORM
    =========================================================================================*/
    $('.btn-area__cliente').on('click', function (e) {
      var link = $(this).attr('href');

      if (!$(this).hasClass('btn-area__cliente--logado')) {
        // } else {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.form--login').toggleClass('is-active');
      }
    });


    /*=========================================================================================
    // IMG 360
    =========================================================================================*/
    var x1,
      y1,
      moving = false,
      $viewer = $('.plano_viewer'),
      c_x_deg = 0, // current x
      c_y_deg = 0,
      perspective = 450; // current y

    $viewer.each(function (item, el) {
      var $el = $(this),
        $cube = $el.find('.cube'),
        w_v = $el.width(),
        h_v = $el.height();

      $el.on('mousedown', function (e) {
        x1 = e.pageX - $(this).offset().left;
        y1 = e.pageY - $(this).offset().top;
        moving = true;
        e.preventDefault();
      });

      $cube.on('mousemove', function (e) {
        if (moving === true) {
          x2 = e.pageX - $el.offset().left;
          y2 = e.pageY - $el.offset().top;

          var dist_x = x2 - x1,
            dist_y = y2 - y1,
            perc_x = dist_x / w_v,
            perc_y = dist_y / h_v,
            deg_x = Math.atan2(dist_y, perspective) / Math.PI * 180,
            deg_y = -Math.atan2(dist_x, perspective) / Math.PI * 180,
            i,
            vendors = ['-webkit-', '-moz-', ''];

          c_x_deg += deg_x;
          c_y_deg += deg_y;
          c_x_deg = Math.min(90, c_x_deg);
          c_x_deg = Math.max(-90, c_x_deg);

          c_y_deg %= 360;

          deg_x = c_x_deg;
          deg_y = c_y_deg;

          for (i in vendors) {
            $cube.css(vendors[i] + 'transform', 'rotateX(' + deg_x + 'deg) rotateY(' + deg_y + 'deg)');
          }

          x1 = x2;
          y1 = y2;
        }
        e.preventDefault();
      }).on('mouseup', function (e) {
        moving = false;
        e.preventDefault();
      });
    });


    /*=========================================================================================
    // FIXED MENU
    =========================================================================================*/

    var $win = $(window);
    var homeTarget = $('.newsletter');
    var menu = $('.barra-top');
    var heightMenu = menu.height();

    function menuFixo(sizeScroll, target) {
      if (sizeScroll >= target) {
        menu.addClass('is-fixed animated fadeInUp').removeClass('');
      } else {
        menu.removeClass('fadeInUp is-fixed').addClass('');
      }
    }

    // Menu Scroll
    $win.on("scroll", function () {
      var scrollBottom = $(document).height() - $win.height() - $win.scrollTop();
      var heightHomeTarget = $win.height() - homeTarget.height();
      var target = (heightHomeTarget - heightMenu) + homeTarget.height();

      menuFixo(scrollBottom, target);
    });

    // AJAX CONTENT
    ajaxContent($('.tab__link.active'));

    $('#ajax-content').addClass('ajax-load');

    $("[data-ajax='gal']").click(function (e) {
      $("[data-ajax='gal']").removeClass('active');
      $(this).addClass('active');

      e.preventDefault();
      $('#ajax-content').addClass('ajax-load');
      ajaxContent(this);
    });

    function ajaxContent(link) {
      $link = $(link).attr('href');
      $target = $(link).data('ancora');

      $("#ajax-content").load($link + ' #' + $target, function () {
        $(this).removeClass('ajax-load');
        equalizeHeights('.emp-vert .icones');
        $('.owl-emp-gal').owlCarousel({
          margin: 20,
          nav: true,
          navText: false,
          dots: true,
          responsive: {
            0: {
              items: 1,
              mouseDrag: true,
            },
            768: {
              items: 1,
              mouseDrag: true,
            },
            1000: {
              items: 2,
              mouseDrag: false,
            }
          }
        });
      });
    }

    /*=========================================================================================
    // LAZY
    =========================================================================================*/

    var bLazy = new Blazy({
      offset: 0,
      error: function (ele) {
        var original = ele.getAttribute('data-src');
        ele.src = original;
      }
    });

    function init() {
      var imgDefer = document.getElementsByClassName('img-defer');
      for (var i = 0; i < imgDefer.length; i++) {
        if (imgDefer[i].getAttribute('data-src')) {
          imgDefer[i].setAttribute('src', imgDefer[i].getAttribute('data-src'));
        }
      }
    }

    window.onload = init;

    /*=========================================================================================
    // OWL
    ========================================================================================= */

    // Slide Topo
    $('#owl-slides').owlCarousel({
      items: 1,
      mouseDrag: true,
      nav: false,
      loop:true,
      navText: false,
      dots: true,
      autoplay: false,
      smartSpeed: 850,
      autoplaySpeed: 850,
      autoplayTimeout: 8000,
      lazyLoad: true,
    });

    // Slide 360
    $('#owl-360').owlCarousel({
      items: 1,
      mouseDrag: false,
      nav: true,
      navText: false,
      dots: true,
    });

    // Galerias Home - Empreendimentos Interna
    $('.owl-galeria').owlCarousel({
      items: 1,
      mouseDrag: false,
      nav: true,
      navText: false,
      dots: false,
      lazyLoad: true,
      loop: false,
    });

    $('.owl-int-galeria').owlCarousel({
      items: 1,
      dots: false,
      mouseDrag: true,
      nav: true,
      navText: false,
      lazyLoad: true,
    });

    // Depoimentos
    $('.owl-depoimentos').owlCarousel({
      items: 3,
      mouseDrag: true,
      nav: true,
      navText: false,
      dots: false,
      // loop: true,
    });

    // Avulsos
    $('.owl-avulsos').owlCarousel({
      items: 3,
      mouseDrag: true,
      nav: true,
      navText: false,
      dots: false,
      loop: true,
    });

    $('#galeria-andamento').owlCarousel({
      items: 3,
      mouseDrag: true,
      nav: false,
      navText: false,
      dots: true,
      // loop: true,
      margin: 50,
    });

    // $('.owl-parceiros').owlCarousel({
    //   items: 6,
    //   mouseDrag: true,
    //   nav: true,
    //   navText: false,
    //   dots: false,
    //   loop: true,
    //   margin: 20
    // });

    $('.owl-direferenciais').owlCarousel({
      mouseDrag: true,
      nav: true,
      navText: false,
      dots: false,
      responsive: {
        0: {
          items: 1,
          dots: true,
        },
        768: {
          items: 3
        },
        1280: {
          items: 6
        }
      }
    });

    $('#status-obra').owlCarousel({
      mouseDrag: true,
      nav: true,
      navText: false,
      dots: false,
      // loop: true,
      responsive: {
        0: {
          items: 1
        },
        768: {
          items: 3
        },
        1280: {
          items: 6
        }
      }
    });

    var customconfig = {
      items: 1,
      loop: false,
      lazyLoad: true,
      responsive: {
        0: {
          items: 1,
          dots: false,
          margin: 0,
          loop: true,
          mouseDrag: false,
        },

        768: {
          items: 2,
          dots: false,
          margin: 0,
          loop: true,
          margin: 20,
          mouseDrag: false,
        },

        1024: {
          items: 1,
          dots: false,
          margin: 0,
          loop: true,
          mouseDrag: false,
        }
      }
    }

    // CUSTOM OWL
    setaCustomOwl('#owl-noticias', 'seta--not', customconfig);

    function setaCustomOwl($element, $setas, $obj) {
      var $carousel = $($element);
      var $setaPrev = $('.seta--left.' + $setas);
      var $setaNext = $('.seta--right.' + $setas);
      var $default = {
        items: 1,
        dots: false,
        margin: 0,
        loop: true,
        mouseDrag: false,
        lazyLoad: true,
      };

      var $obj = ($obj != undefined) ? $obj : $default;

      $carousel.owlCarousel($obj);

      $setaPrev.click(function (e) {
        e.preventDefault();
        $carousel.trigger('prev.owl.carousel');
      });

      $setaNext.click(function (e) {
        e.preventDefault();
        $carousel.trigger('next.owl.carousel');
      });
    }

    /*=========================================================================================
    // YOUTUBE VIDEO
    =========================================================================================*/

    // $('.gallery_emp').magnificPopup({
    //   delegate: 'a',
    //   type: 'image',
    //   tLoading: 'Carregando imagem #%curr%...',
    //   mainClass: 'mfp-img-mobile',
    //   gallery: {
    //     enabled: true,
    //     navigateByImgClick: true,
    //     preload: [0,1]
    //   }
    // });

    $('.plantas__nav').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Carregando imagem #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1]
      }
    });

    // GALERIA EMPREENDIMENTOS
    var $gal = $('.open-galeria');

    $gal.on('click', function (e) {
      e.preventDefault();

      $(this).parent().find('.status-obra__galeria a:eq(0)').trigger('click');
    });

    $gal.each(function (event) {
      $(this).parent().find('.status-obra__galeria').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
          enabled: true
        }
      });
    });

    $('.open-modal').magnificPopup({
      removalDelay: 500,
      callbacks: {
        beforeOpen: function () {
          this.st.mainClass = this.st.el.attr('data-effect');
        },
        close: function () {
          resetForm();
        }
      },
      midClick: true
    });

    function resetForm() {
      var form = $('.wpcf7-form');
      var input = form.find('input');
      var msg = form.find('.wpcf7-response-output');

      form.removeClass('invalid sucess');
      input.removeClass('wpcf7-not-valid');
      msg.hide();
    }

    // FILE INPUT
    var fileInput = document.querySelector('.form-input-file');
    var fileInputText = document.querySelector('.form-input--file-text');

    if (fileInput) {
      fileInputTextContent = fileInputText.textContent;

      fileInput.addEventListener('change', function (e) {
        var value = e.target.value.length > 0 ? e.target.value : fileInputTextContent;

        fileInputText.textContent = value.replace('C:\\fakepath\\', '');
      });
    }

    /*=========================================================================================
    // MODAL CARIRI STORE
    =========================================================================================*/

    $('.popup-link').magnificPopup({
      removalDelay: 500,
      callbacks: {
        beforeOpen: function () {
          this.st.mainClass = this.st.el.attr('data-effect');
        },
      },
      midClick: true
    }).trigger('click');

    $('.open-modal-cariri').magnificPopup({
      removalDelay: 500,
      callbacks: {
        beforeOpen: function () {
          this.st.mainClass = this.st.el.attr('data-effect');
          var emp = this.st.el.attr('data-title'),
            empcode = this.st.el.attr('data-code'),
            filial = this.st.el.attr('data-filial'),
            modal = $('#lead-modal form');

          modal.find('#nome_emp').val(emp);
          modal.find('#emp_code').val(empcode);
          modal.find('#filial_code').val(filial);
          // getEmpDados(nome, empcode, filial)
        },
        close: function () {
          resetForm();
        }
      },
      midClick: true
    });

    /*=========================================================================================
    // MODAL ÁREA DO CLIENTE
    =========================================================================================*/

    //Editar Dados
    function getUserDados() {
      var modal = $('#editar-popup form');
      var nome = $('.cliente_nome').val(),
        cpf = $('.cliente_cpf').val(),
        rg = $('.cliente_rg').val(),
        telefone = $('.cliente_telefone').val(),
        email = $('.cliente_email').val(),
        endereco = $('.cliente_endereco').val(),
        logradouro = $('cliente_endereco_logradouro').val(),
        numero = $('.cliente_endereco_numero').val(),
        compl = $('.cliente_endereco_compl').val(),
        bairro = $('.cliente_bairro').val(),
        cep = $('.cliente_cep').val();
      cidade = $('.cliente_cidade').val(),
        uf = $('.cliente_uf').val(),
        pais = $('.cliente_endereco_pais').val();

      if (compl != '') {
        var full_address = logradouro + ', ' + numero + ' - ' + compl;
      } else {
        var full_address = logradouro + ', ' + numero;
      }

      modal.find('.nome input').val(nome);
      modal.find('.cpf input').val(cpf);
      modal.find('.rg input').val(rg);
      modal.find('.email input').val(email);
      modal.find('.telefone input').val(telefone);
      modal.find('.endereco input').val(endereco);
      modal.find('.bairro input').val(bairro);
      modal.find('.cep input').val(cep);
      modal.find('.cidade input').val(cidade);
      modal.find('.uf input').val(uf);
      modal.find('.pais input').val(pais);
    }

    $('.open-modal-cliente').magnificPopup({
      removalDelay: 500,
      callbacks: {
        beforeOpen: function () {
          this.st.mainClass = this.st.el.attr('data-effect');
          getUserDados();
        },
        close: function () {
          resetForm();
        }
      },
      midClick: true
    });

    //Assitência
    function getEmpDados(div) {
      var modal = $('#assistencia-popup form'),
        nome = $('.cliente_nome').val(),
        cpf = $('.cliente_cpf').val(),
        rg = $('.cliente_rg').val(),
        telefone = $('.cliente_telefone').val(),
        email = $('.cliente_email').val(),
        imovel_nome = $('.' + div + '_imovel_nome').val(),
        imovel_codigo = $('.' + div + '_imovel_codigo').val(),
        imovel_unidade = $('.' + div + '_imovel_unidade').val(),
        imovel_endereco = $('.' + div + '_imovel_endereco_emp').val(),
        imovel_contrato = $('.' + div + '_imovel_contrato').val();

      modal.find('.nome input').val(nome);
      modal.find('.cpf input').val(cpf);
      modal.find('.rg input').val(rg);
      modal.find('.email input').val(email);
      modal.find('.telefone input').val(telefone);
      modal.find('.empreendimento input').val(imovel_nome);
      modal.find('.unidade input').val(imovel_unidade);
      modal.find('.codigo input').val(imovel_codigo);
      modal.find('.contrato input').val(imovel_contrato);
      modal.find('.endereco_emp input').val(imovel_endereco);

      console.log(div);
    }

    $('.open-modal-assistencia').magnificPopup({
      removalDelay: 500,
      callbacks: {
        beforeOpen: function () {
          this.st.mainClass = this.st.el.attr('data-effect');
          getEmpDados(this.st.el.attr('data-contrato'));
        },
        close: function () {
          resetForm();
        }
      },
      midClick: true
    });

    $('.zoom-gallery').magnificPopup({
      delegate: 'a',
      type: 'image',
      closeOnContentClick: false,
      closeBtnInside: false,
      mainClass: 'mfp-with-zoom mfp-img-mobile',

      image: {
        verticalFit: true,
        titleSrc: function (item) {
          return item.el.attr('title');
        }
      },

      gallery: {
        enabled: true,
        tCounter: '%curr% de %total%'
      },

      zoom: {
        enabled: true,
        duration: 300, // don't foget to change the duration also in CSS
        opener: function (element) {
          return element.find('img');
        }
      }
    });

    //Alterar Senha
    $('.open-modal-pass').magnificPopup({
      removalDelay: 500,
      callbacks: {
        beforeOpen: function () {
          this.st.mainClass = this.st.el.attr('data-effect');
        },
        close: function () {
          limpaForm();
        }
      },
      midClick: true
    });

    function limpaForm() {
      $('#trocar-senha')[0].reset();
      $('.pass__msg').attr('class', 'pass__msg');
    }

    /*=========================================================================================
    // ACCORDION
    =========================================================================================*/

    var activeopen = $('.accordion-title.active');
    $('.accordion ' + activeopen.attr('href')).slideDown(300).addClass('open');

    function close_accordion_section() {
      $('.accordion .accordion-content').slideUp(300).removeClass('open');
      $('.accordion .accordion-title').removeClass('active');
    }

    $('.accordion-title').click(function (e) {
      var currentAttrValue = $(this).attr('href');

      if ($(this).hasClass('active')) {
        close_accordion_section();
      } else {
        close_accordion_section();
        $(this).addClass('active');
        $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
      }

      e.preventDefault();
    });
    /*=========================================================================================
    // MASKS CONTATO
    =========================================================================================*/

    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },

      spOptions = {
        clearIfNotMatch: true,
        onKeyPress: function (val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
      };

    $('input[type="tel"]').mask(SPMaskBehavior, spOptions);
    $('input[name="data-de-nascimento"]').mask('00/00/0000', { clearIfNotMatch: true });
    $('input[name="cep"]').mask('00000-000', { clearIfNotMatch: true });
    $('input[name="cpf"]').mask('000.000.000-00', { clearIfNotMatch: true });

    //Força Num Keyboard
    $('input#login, input#login2, input[name="data-de-nascimento"], input[name="cep"], input[name="cpf"]').attr('pattern', '[0-9]*');
    $('input#login, input#login2, input[name="data-de-nascimento"], input[name="cep"], input[name="cpf"]').attr('inputmode', 'numeric');

    $("#wpcf7_uf option:first").text('UF').addClass('disabled');
    $("#wpcf7_cidade option:first").text('Cidade').addClass('disabled');
    $("#wpcf7_cidade").attr('disabled', 'disabled');

    $('#wpcf7_uf, #wpcf7_cidade, #estado, #cidade').on("focus blur", function () { $(this).toggleClass('open'); });
    $('#wpcf7_cidade, #cidade').on("change", function () {
      var val = $(this).val();
      if (val === "") { $(this).removeClass('ativo'); } else { $(this).addClass('ativo'); }
    });

    $('input[name=perfil]').on("change", function () {
      var perfil = $('input[name=perfil]:checked').val();
      if (perfil == 'Corretor') {
        $('input[name=creci]').addClass('form__input--show')(500);
      } else {
        $('input[name=creci]').removeClass('form__input--show').fadeOut(500);
        $("input[name=creci]").val("");
      }
      console.log(perfil);
    });

    $('#wpcf7_uf').on("change", function () {
      var val = $("#wpcf7_uf option:selected").text();
      $('.estado input').attr('value', val);
    });

    // $("input[name*='cep']").mask("99999-999");

    /*=========================================================================================
    // SUA HOUSE
    =========================================================================================*/

    // $('.barra-top__item.barra-top__item--chat').on('click', function(){
    //     console.log('chat clicado');
    //     hc_chat(hc_empreendimento, '', '', '');
    // });

    /*=========================================================================================
    // EQUAL HEIGHT
    =========================================================================================*/

    function equalizeHeights(selector) {
      var heights = new Array();

      // Loop to get all element heights
      $(selector).each(function () {

        // Need to let sizes be whatever they want so no overflow on resize
        $(this).css('min-height', '0');
        $(this).css('max-height', 'none');
        $(this).css('height', 'auto');

        // Then add size (no units) to array
        heights.push($(this).height());
      });

      // Find max height of all elements
      var max = Math.max.apply(Math, heights);

      // Set all heights to max height
      $(selector).each(function () {
        $(this).css('height', max + 'px');
      });
    }

    function cloneHeight(original, clone) {
      $(clone).css('height', $(original).height() + 'px');
    }




    $(window).on('load resize', function () {
      equalizeHeights('.noticias__descritivo');
      equalizeHeights('.emp-vert__infos');
      equalizeHeights('.resp-card__infos');
      equalizeHeights('.emp-vert .icones');
      cloneHeight('.wr-empreendimento__content .text-side', '.wr-empreendimento__content .predio');

    

    });

    /*=========================================================================================
    // FORMS
    =========================================================================================*/

    document.addEventListener('wpcf7mailsent', function (event) {

      if ('1605' == event.detail.contactFormId) {
        var href = window.location.href;
        location.href = href + "?sucessocariri";
      }

      else if ('209' == event.detail.contactFormId || '210' == event.detail.contactFormId) {
        setTimeout(function () {
          $('.wpcf7-response-output').fadeOut('500', function () {
            var magnificPopup = $.magnificPopup.instance; magnificPopup.close();
          });
        }, 3200);
      }

      else if ('171' == event.detail.contactFormId) {
        var href = window.location.href;
        location.href = href + "?sucesso=trabalhe";
      }

      else if ('1103' == event.detail.contactFormId) {
        var href = window.location.href;
        location.href = href + "?sucesso=terreno";
      }

      else {
        var href = window.location.href;
        location.href = href + "?sucessocontato";
      }


    }, false);
































    $('.form').find('input, textarea').on('keyup blur focus', function (e) {

      var $this = $(this),
        label = $this.prev('label');

      if (e.type === 'keyup') {
        if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
      } else if (e.type === 'blur') {
        if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.removeClass('highlight');
        }
      } else if (e.type === 'focus') {

        if ($this.val() === '') {
          label.removeClass('highlight');
        }
        else if ($this.val() !== '') {
          label.addClass('highlight');
        }
      }

    });
























    $('.wrap-tabs .tab-selector li').on('click', function () {
      $('.wrap-tabs .tab-selector li').removeClass('active');
      $(this).addClass('active');
      seletor = $(this).attr('seletor');
      wrap = $(this).parents('.wrap-tabs');
      wrap.find('.content-tabs .content').removeClass('active');
      wrap.find('.content-tabs .content#' + seletor).addClass('active');
    })






    $('.slick-plantas').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      swipe: true,
      // fade: true,
      adaptiveHeight: true
    });
    $('.wr-empreendimento__plantas .next').click(function () {
      $('.slick-plantas').slick('slickNext');
    });
    $('.wr-empreendimento__plantas .prev').click(function () {
      $('.slick-plantas').slick('slickPrev');
    });


    $('li[data-slide]').click(function (e) {
      e.preventDefault();
      var slideno = $(this).data('slide');
      $('.slick-plantas').slick('slickGoTo', slideno - 1);
    });


    $('.slick-plantas').on('afterChange', function(event, slick, currentSlide, nextSlide){
   target =  $('.slider-wrap .slick-active').attr('data-slick-index');
   target  = parseInt(target) + 1;

   $('.wr-empreendimento__plantas .slides-tabs .target').removeClass('active');



   $('.wr-empreendimento__plantas .slides-tabs .target[data-slide="'+ target+'"]').addClass('active');

console.log(target)


  });


    var slidesToShow = 6;
    var slideWrapper = $(".map-slider");
    var slides = $(".map-slider .item");
    var totalSlides = slides.length;

    if (totalSlides > 0 && totalSlides < slidesToShow) {
        // Calculate how many additional elements are required
        var diff = slidesToShow - totalSlides;

        // Start cloning from position [0]
        var slideToClone = 0;

        for (var i = 0; i < diff; i++) {
            // Ensure the element about to be cloned exists
            if (slideToClone >= slides.length) {
                slideToClone = 0;
            }

            // Clone/append slide
            slides.eq(slideToClone).clone().appendTo(slideWrapper);

            // Increment iterator to copy next slide
            slideToClone++;
        }
    }

    // Finally, initialise slick
  //  slideWrapper.slick();


    slideWrapper.slick({
       slidesToShow: 5,
      infinite: true,
      slidesToScroll: 1,
      arrows: false,
      // fade: true,
      centerMode: true,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $('.map-slider-wrap .nav-slider .next').click(function () {
      $('.map-slider').slick('slickNext');
    });
    $('.map-slider-wrap .nav-slider .prev').click(function () {
      $('.map-slider').slick('slickPrev');
    });


    
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
  
      fixedContentPos: false
    });


    $('.zoom-gallery').magnificPopup({
      delegate: 'a',
      type: 'image',
      closeOnContentClick: false,
      closeBtnInside: false,
      mainClass: 'mfp-with-zoom mfp-img-mobile',
      image: {
        verticalFit: true,
        titleSrc: function(item) {
          return item.el.attr('title') + ' &middot;';
        }
      },
      gallery: {
        enabled: true
      },
      zoom: {
        enabled: true,
        duration: 300, // don't foget to change the duration also in CSS
        opener: function(element) {
          return element.find('img');
        }
      }
      
    });

    $('.image-popup-vertical-fit').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      mainClass: 'mfp-img-mobile',
      image: {
        verticalFit: true
      }
      
    });



    $('.image-popup-planta').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      closeBtnInside: true,
      fixedContentPos: true,
      mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
      image: {
        verticalFit: true,
        titleSrc: function(item) {
          return '<a href="'+item.el.attr('href')+'" download class="wr-btn  wr-btn--line wr-btn--download wr-btn--popup" tabindex="0">Baixar Planta</a>';
          // return '<a href="'+item.el.attr('href')+'" download>Baixar Planta</a>';
        }
      },
      zoom: {
        enabled: true,
        duration: 300 // don't foget to change the duration also in CSS
      }
    });


    /*=========================================================================================
    // CLOSE FUNCTION
    =========================================================================================*/

  });
});



