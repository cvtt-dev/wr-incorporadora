/*=========================================================================================
// INICIO AJAX JS
========================================================================================= */

jQuery(function($) {
    $(document).ready(function() {

/*=========================================================================================
//AJAX GLOBAL VARS
=========================================================================================*/

var SITE_URL  = $('.topo__logo a').attr('href'),
    AJAX_URL  = SITE_URL + '/wp-admin/admin-ajax.php',
    LOGIN_URL = SITE_URL + '/area-do-cliente';

/*=========================================================================================
//LOGIN
=========================================================================================*/

$('#area-do-cliente').submit(function() {
    var data    = {
        'action'  : 'getUser',
        'login'   : $('#login').val(),
        'senha'   : $('#senha').val(),
    }
    ajax_call(data);
});

$('#area-do-cliente2').submit(function() {
    var data    = {
        'action'  : 'getUser',
        'login'   : $('#login2').val(),
        'senha'   : $('#senha2').val(),
    }
    ajax_call(data);
});

function ajax_call(data){
    $.ajax({
        url: AJAX_URL,
        type: "POST",
        data: data,
        beforeSend: function() {
            $('.login__msg').attr('class','login__msg');
            $('.login__load').addClass('login__load--on');
            $('#area-do-cliente.btn').attr('disabled','disabled');
        },
        success: function(response) {

            var rsp = response;
            console.log('rsp: '+rsp);

            if(rsp == 1) {
                var msg = "Login realizado com sucesso! Iremos redirecioná-lo para a área de clientes.",
                    css = 'login__msg--sucesso';
                    $('.login__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                    $('#area-do-cliente .btn').removeAttr('disabled');
                    $('.login__load').removeClass('login__load--on');
                    window.location.href = LOGIN_URL;
            } else if(rsp == 2) {
                var msg = "Dados incorretos. Tente novamente",
                    css = 'login__msg--erro';
                $('.login__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                $('.login__msg').delay(1300).fadeOut(800);
                $('#area-do-cliente .btn').removeAttr('disabled');
                $('.login__load').removeClass('login__load--on');
            } else {
                var msg = "Nosso sistema está em manutenção. Favor tente novamente em alguns minutos.",
                    css = 'login__msg--erro';
                $('.login__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                $('.login__msg').delay(1300).fadeOut(800);
                $('#area-do-cliente .btn').removeAttr('disabled');
                $('.login__load').removeClass('login__load--on');
            }
        },
        error: function() {
            var msg = 'Nosso sistema está com problemas. Favor tente novamente em alguns minutos.',
                css = 'login__msg--erro';
                $('.login__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                $('.login__msg').delay(1300).fadeOut(800);
                $('#area-do-cliente .btn').removeAttr('disabled');
                $('.login__load').removeClass('login__load--on');
        }
    });
}

//Change Pass

$('#trocar-senha').submit(function() {
    var data    = {
        'action'  : 'getPass',
        'senha'   : $('#senha').val()
    }
    ajax_pass(data);
});

function ajax_pass(data){
    $.ajax({
        url: AJAX_URL,
        type: "POST",
        data: data,
        beforeSend: function() {
            $('.pass__msg').attr('class','pass__msg');
            $('.pass__msg').addClass('enviando').html('Enviando dados. Aguarde!').fadeIn(500);
            $('.login__load').addClass('login__load--on');
            $('#trocar-senha .btn').attr('disabled','disabled');
        },
        success: function(response) {
            var rsp   = response;
            console.log('rsp: '+rsp);

            function close(){
                var magnificPopup = $.magnificPopup.instance;magnificPopup.close();
            }

            if(rsp == 1) {
                var msg = "Senha Alterada com sucesso.",
                    css = 'sucesso';
                    $('.pass__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                    $('#trocar-senha .btn').removeAttr('disabled');
                    $('.login__load').removeClass('login__load--on');
                    setTimeout(close, 1500);
            } else if(rsp == 3) {
                var msg = "Cliente Inexistente.",
                    css = 'erro';
                    $('.pass__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                    $('#trocar-senha .btn').removeAttr('disabled');
                    $('.login__load').removeClass('login__load--on');
            } else{
                var msg = "Nosso sistema está com problemas. Favor tente novamente em alguns minutos.",
                    css = 'erro';
                    $('.pass__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                    $('#trocar-senha .btn').removeAttr('disabled');
                    $('.login__load').removeClass('login__load--on');
            }
        },
        error: function() {
            var msg = 'Nosso sistema está com problemas. Favor tente novamente em alguns minutos.',
                css = 'erro';
                $('.pass__msg').delay(200).addClass(css).html(msg).fadeIn(500);
                $('.pass__msg').delay(1300).fadeOut(800);
                $('#trocar-senha .btn').removeAttr('disabled');
                $('.login__load').removeClass('login__load--on');
        }
    });
}

/*=========================================================================================
// CIDADES/ESTADOS
=========================================================================================*/

$('#estado, #wpcf7_uf').on("change", function(){
    var cod_est = $(this).val(),
        id      =  $(this).attr('id');

    if(id == "estado") {var palco = $('#cidade');} else {var palco = $('#wpcf7_cidade');}
    getCidade(cod_est, palco);
    //console.log(palco);
});

function getCidade(cod_est, palco) {
    $.ajax({
        url: AJAX_URL,
        type:'POST',
        data: "action=getcidades&estado="+ cod_est,
        beforeSend: function() {
            palco.html('<option value="">Carregando...</option>');
            palco.addClass('disabled').attr('disabled','disabled');
        },
        success: function(html){

            palco.html(html);

            if(html == '<option value="">Cidade</option>')  {
                $('#wpcf7_uf').removeClass('ativo');
                palco.removeClass('ativo').attr('disabled','disabled');
            }
            else {
                $('#wpcf7_uf').addClass('ativo');
                palco.removeAttr('disabled');
            }
            // console.log(palco);

        }
    });
   return false;
}

/*=========================================================================================
// CLOSE FUNCTION
=========================================================================================*/
    });
});