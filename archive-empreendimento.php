<?php
	get_template_part('templates/html','header');
	$catSlug = $_GET['cat'];

	$categorias = get_terms( array(
		'taxonomy' => 'categorias',
		'hide_empty' => false,
		'exclude' => array(8),
	));
?>
<article class="pages pages--empreendimento">
    <header class="header-img">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/capa-empreendimentos.jpg" alt="">
	</header>

	<div class="abas__header abas__header--branco">
		<div class="container">
			<div class="header-tit header-tit--small">
				<h2 class="abas__tit tit-border">Empreendimentos WR</h2>
			</div>
		</div>

		<ul class="tab tab--cinza filters">
			<li class="tab__item">
				<a href="<?php echo get_post_type_archive_link('empreendimento'); ?>" data-ajax="gal" data-ancora="empreendimento-content" title="Todos" class="tab__link active">Todos</a>
			</li>
			<?php
				$i = 1;

				$my_new_array = array();

				foreach ($categorias as $term) {
				    $issue_date  = get_term_meta( $term->term_id, 'tax_ordem', true );
				    $my_new_array[$issue_date] = $term;
				}

				ksort( $my_new_array, SORT_NUMERIC );
				foreach ($my_new_array as $cat ) :
					$isActive = ( $catSlug === $cat->slug ) ? 'active' : '';
					$link = get_term_link($cat->slug, 'categorias');
			?>
			<li class="tab__item">
				<a href="<?php echo $link; ?>" class="tab__link" data-ajax="gal" data-ancora="empreendimento-content">
					<?php echo $cat->name; ?>
				</a>
			</li>
			<?php $i++; endforeach; ?>

			<li class="tab__item">
				<a  class="tab__link" href="<?php echo get_term_link('avulsos', 'categorias' ); ?>">Avulsos</a>
			</li>
		</ul>
	</div>

	<?php
	global $wp_query;
	$args = array(
		'post_type'  	 => 'empreendimento',
		'posts_per_page' => -1,
		'tax_query' => array(
        	'relation' => 'AND',
	        array(
	            'taxonomy' => 'categorias',
	            'field'    => 'term_id',
	            'terms'    => array(8),
	            'operator' => 'NOT IN',
	        ),
	    ),
	);
	query_posts(array_merge($wp_query->query, $args)); ?>

	<div class="container">
		<div id="ajax-content">
			<div id="empreendimento-content" class="emp-grid">
				<?php
					if (have_posts()) :
						while ( have_posts() ) : the_post();
							get_template_part('templates/empreendimento/html', 'emp-vertical');
						$i++; endwhile; wp_reset_postdata();
					else :
						echo '<div class="msg"><p>Nenhum empreendimento foi encontrado!</p></div>';
					endif;
				?>
			</div>
		</div>
	</div>
</article>

<?php get_template_part('templates/html','footer');?>