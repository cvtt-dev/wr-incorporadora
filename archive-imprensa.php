<?php get_template_part('templates/html','header');?>

<article class="pages pages--responsabilidade">
    <header class="header-img">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/capa-imprensa.jpg" alt="">
	</header>

	<div class="container">
		<div class="header-tit">
			<h2 class="abas__tit tit-border">Assessoria de Imprensa</h2>
		</div>

		<div class="respo-page">
			<section class="respo-page__content">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('templates/responsabilidade/html', 'respo-card'); ?>
				<?php endwhile; ?>
				<?php wp_pagenav($wp_query->max_num_pages); wp_reset_postdata(); ?>
			</section>
		</div>
	</div>

</article>

<?php get_template_part('templates/html','footer');?>