<div class="capa-interna">
    <?php #echo do_shortcode('[image_page]'); ?>
    <header class="capa-interna__item ">
        <div class="capa-interna__vertical ">
            <div class="capa-interna__middle ">
                <div class="container">
                    <h2 class="tit__2 tit__2--interno">
						<span>
						<?php
                        if(is_404()){
                            echo "Página não encontrada.";
                        }
                        elseif(is_tax('categoria')){    
                            echo single_cat_title();
                        }
                        elseif(is_search() || is_singular('post') ){
                            echo 'Resultado de Busca';
                        }
                        else{
                            the_title();
                        }
                        ?>
						</span>
                    </h2>

                    <?php #echo wp_breadcrumbs(); ?>
                </div>
            </div>
        </div>
    </header>
</div>
