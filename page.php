<?php get_template_part('templates/html', 'header'); ?>
<?php while (have_posts()) : the_post();
    $capa_mobile = get_post_meta(get_the_ID(), 'meta-thumbnail_mobile', false);
    $capa_desktop = get_post_meta(get_the_ID(), 'meta-thumbnail_desktop', false);
?>
    <section class="pages">
        <header class="header-img">
            <?php
            echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_mobile[0]) . '" class="class-capa-mobile header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_mobile[0]) . '">';
            if ($capa_desktop) {
                echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_desktop[0]) . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_desktop[0]) . '">';
            } else {
                echo '<img width="1920" height="367" alt="" data-src="' . the_post_thumbnail_url() . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . the_post_thumbnail_url() . '">';
            }
            ?>
        </header>

        <div class="container">
            <div class="pages__content">
                <h2 class="pages__tit"><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
        </div>
    </section>
<?php endwhile;
wp_reset_postdata(); ?>
<?php get_template_part('templates/html', 'footer'); ?>