<?php get_template_part('templates/html', 'header'); ?>
<section class="pages pages--notfound">
    <div class="text-wrap-404">
        <div class="container">
            <!-- <div class="pages__thumb">
            <img src="<?php //echo get_template_directory_uri(); 
                        ?>/assets/images/page-404.png" alt="Página Não Encontrada" />
        </div> -->

            <h3 class="erro-code">
                404
            </h3>

            <div class="pages__header">
                <h2 class="pages__headline">
                    Ops! Essa página não foi encontrada.
                </h2>
            </div>
            <div class="pages__wrap-btn">
                <a class="btn btn--red" href="<?php echo get_bloginfo('url'); ?>">Voltar para página inicial</a>
            </div>
        </div>
    </div>

    <figure class="bg-draw">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/bg-404.jpg" alt="">
    </figure>



    <style>
     

        h3.erro-code {
            text-align: center;
            font-size: 130px;
            margin: 0;
            padding: 0;
            color: #b80405;
            font-weight: 700;
        }

        .pages__wrap-btn {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        h2.pages__headline {
            color: #4e4e4e;
        }

        .pages__wrap-btn .btn {
            background-color: #fff;
        }

        .pages__wrap-btn .btn:hover {
            background-color: red;
            color: #fff
        }

        section.pages.pages--notfound {
            position: relative;
            min-height: 480px;
    height: 100vh;
    max-height: 640px;
        }

        .text-wrap-404 {
            position: absolute;
            left: 0;
            z-index: 999;
            top: 0;
            height: 100%;
            display: flex;
            width: 100%;
            align-items: center;
        }

        figure.bg-draw {
            position: absolute;
    left: 0;
    top: 0;
    margin: 0;
    padding: 0;
    opacity: 0.08;
    display: flex;
    height: 100%;
    width: 100%;
    min-height: 480px;
        }

        figure.bg-draw img {
            width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
    min-height: 480px;
        }
    </style>
</section>
<?php get_template_part('templates/html', 'footer'); ?>