<?php /* Template Name: Trabalhe Conosco  */ ?>
<?php
get_template_part('templates/html', 'header');
$settings = get_option('options_gerais');
$capa_mobile = get_post_meta(get_the_ID(), 'meta-thumbnail_mobile', false);
$capa_desktop = get_post_meta(get_the_ID(), 'meta-thumbnail_desktop', false);
?>
<?php while (have_posts()) : the_post(); ?>

	<article class="pages pages--trabalhe-conosco">
		<header class="header-img">
		<?php
            echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_mobile[0]) . '" class="class-capa-mobile header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_mobile[0]) . '">';
            if ($capa_desktop) {
                echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_desktop[0]) . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_desktop[0]) . '">';
            } else {
                echo '<img width="1920" height="367" alt="" data-src="' . the_post_thumbnail_url() . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . the_post_thumbnail_url() . '">';
            }
            ?>
		</header>

		<div class="container">

			<div class="header-tit header-tit--pages">
				<h2 class="tit-border"><?php the_title(); ?></h2>
				<div class="header-tit__desc"><?php the_content(); ?></div>
			</div>

			<div class="grid">
				<div class="column">
					<div class="form form--big">
						<?php echo do_shortcode('[contact-form-7 id="171" title="FORM - Trabalhe conosco"]'); ?>
					</div>
				</div>

				<div class="column">
					<div class="mapa">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bg-trabalhe-conosco.jpg" alt="" class="img-full">
					</div>

					<div class="block-address">
						<?php foreach ($settings['group_ends'] as $end) : ?>
							<address class="endereco">
								<div class="endereco__icon">
									<i class="icon icon-pin"></i>
								</div>

								<div class="endereco__infos">
									<?php if ($end["end_titulo"]) : ?><strong class="endereco__tit"><?php echo $end["end_titulo"]; ?></strong><?php endif; ?>

									<?php if ($end["end_telefone"]) : ?>
										<strong class="endereco__subtit"><?php echo $end["end_telefone"]; ?></strong>
									<?php endif; ?>

									<div class="endereco__desc">
										<p><?php echo $end["end_logradouro"] . ' - ' . $end["end_bairro"] . ' - ' . $end["end_cidade"] . ' - ' . $end["end_estado"] . ' - CEP: ' . $end["end_cep"]; ?></p>
									</div>
								</div>
							</address>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php endwhile;
wp_reset_postdata(); ?>
<?php get_template_part('templates/html', 'footer'); ?>