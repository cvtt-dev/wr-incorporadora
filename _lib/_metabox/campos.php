<?php

$prefix = 'wpcf_';
add_filter('rwmb_meta_boxes', 'wpcf_meta_boxes');
function wpcf_meta_boxes($meta_boxes)
{

    //==============================================
    // DEPOIMENTOS
    //==============================================

    $meta_boxes[] = array(
        'id'        => 'informacoes-par-depoimentos',
        'title'     => 'Informações para depoimentos',
        'pages'     =>  array('depoimentos'),
        'context'   => 'normal',
        'priority'  => 'high',
        'autosave'  => true,
        'fields'    =>   array(
            array(
                'id'    => 'desc_depoimento',
                'type'  => 'textarea',
                'name'  => 'Depoimento',
            ),
            array(
                'id'    => 'cargo_depoimento',
                'type'  => 'text',
                'name'  => 'Cargo/Profissão',
            ),
        ),
    );

    //==============================================
    // DOWNLOADS
    //==============================================

    $meta_boxes[] = array(
        'id'        => 'informacoes-para-download',
        'title'     => 'Informações para download',
        'pages'     =>   array('downloads'),
        'context'   => 'normal',
        'priority'  => 'high',
        'autosave'  => true,
        'fields'    => array(
            array(
                'id'               => 'arquivo_download',
                'type'             => 'file_advanced',
                'name'             => 'Arquivos para download',
                'max_file_uploads' => 1,
            ),
        ),
    );



    //==============================================
    // CARIRI STORE
    //==============================================

    // $meta_boxes[] = array(
    //     'id' => 'slides',
    //     'title' => 'Detalhes do Empreendimento',
    //     'pages' => array('cariri'),
    //     'context' => 'normal',
    //     'priority' => 'high',

    //     'fields' => array(
    //         array(
    //             'name'       => '',
    //             'id'         => 'cariri_preco',
    //             'desc'       => 'Preço do Empreendimento',
    //             'type'       => 'text',
    //             'admin_columns' => array('position' => 'after title', 'title' => 'Preço', 'sort' => false),
    //         ),
    //         // array(
    //         //     'name'       => 'Exibir Preço',
    //         //     'id'         => "cariri_exibir",
    //         //     'type'       => 'radio',
    //         //     'options'    => array('1' => 'Sim', '0' => 'Não'),
    //         //     'std'        => '1',
    //         // ),
    //     )
    // );


    //==============================================
    // PORTFÓLIOS
    //==============================================

    $meta_boxes[] = array(
        'id' => 'portfolio',
        'title' => 'Data de Lançamento',
        'pages' => array('portfolios'),
        'context' => 'side',
        'priority' => 'high',

        // List of meta fields
        'fields' => array(

            array(
                'id'    => 'lancamento_emp',
                'type'  => 'date',
                'name'  => 'Data',
                'required' => true
            ),
        ),
    );


    //==============================================
    // EMPREENDIMENTOS
    //==============================================

    // $meta_boxes[] = array(
    //     'id' => 'links',
    //     'title' => 'Dados CRM',
    //     'pages' => array('empreendimento','cariri'),
    //     'context' => 'side',
    //     'priority' => 'high',

    //     // List of meta fields
    //     'fields' => array(
    //         array(
    //             'name'  => 'Código do HouseCRM',
    //             'id'    => 'cod_crm',
    //             'type'  => 'text',
    //             'admin_columns' => array('position' => 'after title', 'title' => 'Código', 'sort' => false),
    //         ),
    //         array(
    //             'name'  => 'Filial HouseCRM',
    //             'id'    => 'filial_crm',
    //             'type'  => 'text',
    //             'admin_columns' => array('position' => 'after cod_crm', 'title' => 'Filial', 'sort' => true),
    //         ),
    //     ),
    // );

    $meta_boxes[] = array(
        'id'        => 'configuracoes-do-empreendimento',
        'title'     => 'Configurações do Empreendimento',
        'pages'     =>   array('empreendimento'),
        'context'   => 'side',
        'priority'  => 'high',
        'autosave'  => true,
        'fields'    => array(
            array(
                'id'      => 'destaque_emp',
                'name'    => 'É um destaque?',
                'type'    => 'radio',
                'options' => array('Não', 'Sim'),
                'std'     => array('0'),
            ),
            array(
                'id'               => 'logo_do_emp',
                'type'             => 'image_advanced',
                'name'             => 'Logo do Empreendimento',
                'max_file_uploads' => 1,
            ),
            array(
                'id'   => 'cor_emp',
                'name' => 'Cor do empreendimento',
                'type' => 'color',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'        => 'cariri-empreendimentos',
        'title'     => 'Detalhes do Empreendimento',
        'pages'     => array('cariri'),
        'context'   => 'side',
        'priority'  => 'high',
        'autosave'  => true,
        'fields'    => array(
            array(
                'id'    => 'bairro',
                'type'  => 'text',
                'name'  => 'Bairro',
            ),
            array(
                'id'    => 'vagas_cariri',
                'type'  => 'text',
                'name'  => 'Vagas de garagem',
            ),
            array(
                'id'        => 'areas_cariri',
                'type'      => 'group',
                'name'      => 'Áreas (m²)',
                'tab'       => 'info_tab',
                'fields'    => array(
                    array(
                        'id'          => 'area_do_emp_cariri',
                        'type'        => 'text',
                        'placeholder' => 'ex: 214,36 m²',
                        'clone'       => 1,
                    ),
                ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'        => 'outras-informacoes',
        'title'     => 'Outras informações',
        'pages'     =>   array('empreendimento'),
        'context'   => 'normal',
        'priority'  => 'high',
        'autosave'  => true,
        'fields'    => array(
            array(
                'id'               => 'bg_slide',
                'type'             => 'image_advanced',
                'name'             => 'Plano de fundo topo',
                'max_file_uploads' => 1,
                'tab'              => 'topo_tab',
            ),
            array(
                'id'    => 'tit_topo',
                'type'  => 'text',
                'name'  => 'Título do Topo',
                'tab'   => 'topo_tab',
            ),
            array(
                'id'    => 'sub_topo',
                'type'  => 'text',
                'name'  => 'Subtítulo do Topo',
                'desc'  => 'Opcional',
                'tab'   => 'topo_tab',
            ),

            // array(
            //     'id'    => 'youtube_link',
            //     'type'  => 'url',
            //     'name'  => 'Video de apresentação',
            //     'desc'  => 'adicione o link em video do youtube.',
            //     'tab'   => 'topo_tab',
            // ),
          
            array(
                'id'    => 'topo_vagas',
                'type'  => 'text',
                'name'  => 'Vagas de garagem',
                'tab'   => 'topo_tab',

            ),
            array(
                'id'    => 'topo_suites',
                'type'  => 'text',
                'name'  => 'Suítes',
                'tab'   => 'topo_tab',

            ),





            array(
                'id'    => 'topo_areas',
                'type'  => 'text',
                'name'      => 'Áreas (m²)',
                'tab'   => 'topo_tab',


            ),


            array(
                'id'          => 'form_interesse',
                'type'        => 'radio',
                'name'        => 'Remover Form Interesse?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab'         => 'topo_tab',
                'std'         => array('2'),
            ),
            array(
                'id'    => 'info_botao_topo',
                'type'  => 'heading',
                'name'  => 'Informações do botão',
                'tab'   => 'topo_tab',
            ),
            array(
                'id'      => 'text_do_botao',
                'type'    => 'text',
                'name'    => 'Texto do botão',
                'tab'     => 'topo_tab',
                'columns' => 4,
            ),
            array(
                'id'      => 'link_botao_topo',
                'type'    => 'text',
                'name'    => 'Link do botão',
                'tab'     => 'topo_tab',
                'columns' => 4,
            ),
            array(
                'id'          => 'tipo_botao_topo',
                'type'        => 'radio',
                'name'        => 'Tipo do botão',
                'options'     =>       array(
                    '_self'   => 'Interno',
                    '_targer' => 'Externo',
                ),
                'tab'         => 'topo_tab',
                'columns'     => 4,
                'std'         => array('_self'),
            ),


            array(
                'id'               => '',
                'type'             => 'heading',
                'name'             => 'Arquivos para Download',
            
                'tab'              => 'topo_tab',
            ),


            array(
                'id'               => 'folder_emp',
                'type'             => 'file_advanced',
                'name'             => 'Folder Empreendimento',
                'max_file_uploads' => 1,
                'tab'              => 'topo_tab',
            ),
            array(
                'id'               => 'ficha_emp',
                'type'             => 'file_advanced',
                'name'             => 'Ficha Completa',
                'max_file_uploads' => 1,
                'tab'              => 'topo_tab',
            ),

            array(
                'id'    => 'heading_tabela',
                'type'  => 'heading',
                'name'  => 'Tabelas de Valores',
                'tab'   => 'topo_tab',
            ),
            array(
                'id'     => 'tabelas_emp',
                'type'   => 'group',
                'name'   => '',
                'desc'   => '',
                'fields' => array(
                    array(
                        'id'               => 'tabela_desc',
                        'type'             => 'text',
                        'name'             => '',
                        'desc'             => 'Descrição do arquivo',
                    ),
                    array(
                        'id'               => 'tabela_file',
                        'type'             => 'file_advanced',
                        'name'             => '',
                        'desc'             => 'Tabela de Valores',
                        'max_file_uploads' => 1,
                    ),
                ),
                'clone'      => 1,
                'sort_clone' => 1,
                'tab'              => 'topo_tab',
            ),


            array(
              
                'type'  => 'heading',
                'name'  => 'Botão Coringa',
                'tab'   => 'topo_tab',
            ),


            array(
                'id'               => 'ficha_jkbtn_tit',
                'type'             => 'text',
                'name'             => 'Titulo',
              
                'tab'              => 'topo_tab',
            ),
            array(
                'id'               => 'ficha_jkbtn_link',
                'type'             => 'url',
                'name'             => 'Link',
              
                'tab'              => 'topo_tab',
            ),
            array(
                'id'          => 'ficha_jkbtn_target',
                'type'        => 'radio',
                'name'        => 'Link Externo?',
                'options'     => array(
                    '_blank'       => 'Sim',
                    '_self'       => 'Não',
                ),
                'tab'              => 'topo_tab',
                'std'         => array('_self'),
            ),







            array(
                'id'          => 'disab_info',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab'   => 'info_tab',
                'std'         => array('1'),
            ),

            array(
                'id'    => 'conteudo_emp',
                'type'  => 'wysiwyg',
                'name'  => 'Contéudo',
                'tab'   => 'info_tab',
                'raw'   => 1,
            ),



            array(
                'id'    => 'vagas',
                'type'  => 'text',
                'name'  => 'Vagas de garagem',
                'tab'   => 'info_tab',
            ),
            array(
                'id'    => 'suites',
                'type'  => 'text',
                'name'  => 'Suítes',
                'tab'   => 'info_tab',
            ),
            array(
                'id'        => 'areas',
                'type'      => 'group',
                'name'      => 'Áreas (m²)',
                'tab'       => 'info_tab',
                'fields'    => array(
                    array(
                        'id'          => 'area_do_emp',
                        'type'        => 'text',
                        'placeholder' => 'ex: 214,36 m²',
                        'clone'       => 1,
                    ),
                ),
            ),
            // array(
            //     'id'               => '360_emp',
            //     'type'             => 'text',
            //     'name'             => 'Link Imagens ',
            //     'max_file_uploads' => 1,
            //     'tab'              => '360_tab',
            // ),
            array(
                'id'          => 'disab_video',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
               
                'tab'   => 'video_tab',
                'std'         => array('1'),
            ),
            array(
                'id'    => 'youtube_tit',
                'type'  => 'text',
                'name'  => 'Titulo da Chamada',

                'tab'   => 'video_tab',
            ),
            array(
                'id'    => 'youtube_capa',
                'type'             => 'image_advanced',
                'name'             => 'Capa do Video',
                'max_file_uploads' => 1,
                'tab'   => 'video_tab',
                ),
            array(
                'id'    => 'youtube_text',
                'type'  => 'wysiwyg',
                'name'  => 'Texto de apoio',
                'tab'   => 'video_tab',
            ),
            array(
                'id'    => 'youtube_link',
                'type'  => 'url',
                'name'  => 'Video de apresentação',
                'desc'  => 'adicione o link em video do youtube.',
                'tab'   => 'video_tab',
            ),








            // array(
            //     'id'     => 'galeria_360_emp',
            //     'type'   => 'group',
            //     'name'   => '',
            //     'desc'   => 'Dimensão das imagens: <b>1536x1536</b> pixels',
            //     'fields' => array(
            //         array(
            //             'id'               => 'galeria_tit',
            //             'type'             => 'text',
            //             'name'             => '',
            //             'desc'             => 'Nome da Galeria',
            //         ),
            //         array(
            //             'id'               => 'imagens_360',
            //             'type'             => 'image_advanced',
            //             'name'             => '',
            //             'max_file_uploads' => 6,
            //             'desc'             => 'Ordem das imagens: Front, Left, Right, Back, Top, Bottom'
            //         ),
            //     ),
            //     'clone'      => 1,
            //     'sort_clone' => 1,
            //     'tab'        => '360_tab',
            // ),
            // array(
            //     'id'               => 'tour_emp',
            //     'type'             => 'text',
            //     'name'             => 'Link Tour Virtual',
            //     'tab'              => 'info_tab',
            // ),
         
            // array(
            //     'id'               => 'tabela_emp',
            //     'type'             => 'file_advanced',
            //     'name'             => 'Tabela de Valores',
            //     'max_file_uploads' => 1,
            //     'tab'              => 'info_tab',
            // ),
            // array(
            //     'id'    => 'titulo_video',
            //     'type'  => 'text',
            //     'name'  => 'Título do Vídeo',
            //     'tab'   => 'info_tab',
            // ),
            // array(
            //     'id'    => 'youtube_url',
            //     'type'  => 'textarea',
            //     'name'  => 'Iframe do youtube',
            //     'tab'   => 'info_tab',
            // ),
           
            // array(
            //     'id'    => 'lat_emp',
            //     'type'  => 'text',
            //     'name'  => 'Latitude',
            //     'tab'   => 'info_tab',
            // ),
            // array(
            //     'id'    => 'log_emp',
            //     'type'  => 'text',
            //     'name'  => 'Longitude',
            //     'tab'   => 'info_tab',
            // ),
            array(
                'id'          => 'disab_dif',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab'   => 'diferencias_tab',
                'std'         => array('1'),
            ),
            array(
                'id'    => 'titulo_diferenciais',
                'type'  => 'text',
                'name'  => 'Título dos Diferenciais',
                'tab'   => 'diferencias_tab',
            ),
            array(
                'id'          => 'diferenciais_grupo',
                'type'        => 'post',
                'name'        => 'Diferenciais',
                'post_type'   => 'diferenciais',
                'field_type'  => 'select_advanced',
                'multiple'     => true,
                'placeholder' => 'Diferenciais',
                'tab'        => 'diferencias_tab',
            ),
            // array(
            //     'id'               => 'galeria_empreendimento',
            //     'type'             => 'file_advanced',
            //     'name'             => 'Galeria do Empreendimento',
            //     'desc'             => 'Adicione as fotos da galeria do empreendimento',
            //     'max_file_uploads' => 20,
            //     'tab'              => 'galeria',
            // ),







            array(
                'id'          => 'disab_galeria',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab' => 'galeria',
                'std'         => array('1'),
            ),

            array(
                'id' => 'galeria_grid',
                'type' => 'group',
                'name' => 'Grid Galeria',
                'fields' => array(
                    array(
                        'id' => 'titulo',
                        'type' => 'text',
                        'name' => 'Titulo da Galeria',
                    ),
                    array(
                        'id' => 'imgs',
                        'type' => 'image_advanced',
                        'name' => 'Imagens',
                        'desc' => '',
                        'max_file_uploads' => 9,
                        'max_status' => false,
                    ),
                ),
                'clone' => 1,
                'sort_clone' => 1,
                'default_state' => 'expanded',
                'tab' => 'galeria',
            ),







            array(
                'id'          => 'disab_360',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab' => '360',
                'std'         => array('1'),
            ),
            array(
                'id'    => 'titulo_360',
                'type'  => 'text',
                'name'  => 'Título da sessão 360˚',
                'tab'   => '360',
            ),
            array(
                'id' => '360_info',
                'type' => 'group',
                'name' => 'Grid 360',
                'fields' => array(
                    array(
                        'id' => 'titulo',
                        'type' => 'text',
                        'name' => 'Titulo do Botao',
                    ),
                    array(
                        'id' => 'url',
                        'type' => 'url',
                        'name' => 'url',
                        'desc' => 'link para montar o Iframe',
                       
                    ),
                ),
                'clone' => 1,
                'sort_clone' => 1,
                'default_state' => 'expanded',
                'tab' => '360',
            ),






            array(
                'id'          => 'disab_status',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab'  => 'status_tab',
                'std'         => array('1'),
            ),

            array(
                'id'   => 'status_emp_subtit',
                'type' => 'text',
                'name' => 'Subtítulo',
                'tab'  => 'status_tab',
                'std'  => 'Informações atualizadas em: ',
            ),

            array(
                'id'   => 'status_emp',
                'type' => 'heading',
                'name' => 'Status da Obra',
                'tab'  => 'status_tab',
            ),
            array(
                'id'    => 'previsao_entrega',
                'name'  => 'Previsão de Entrega',
                'type'  => 'date',
                'tab'   => 'status_tab',
            ),
            array(
                'id'    => 'status_geral',
                'name'  => 'Status Geral',
                'type'  => 'range',
                'max'   => 100,
                'step'  => 1,
                'tab'   => 'status_tab',
            ),
            array(
                'id'     => 'status_grupo',
                'type'   => 'group',
                'name'   => 'Status da obra Itens',
                'fields' => array(
                    array(
                        'id'      => 'tit_status',
                        'type'    => 'text',
                        'name'    => 'Título',
                        'columns' => 6,
                    ),
                    array(
                        'id'      => 'porcentagem_status',
                        'name'    => 'Porcentagem',
                        'type'    => 'range',
                        'max'     => 100,
                        'step'    => 1,
                        'columns' => 6,
                    ),
                    // array(
                    //     'id'      => 'galeria_icone',
                    //     'type'    => 'text',
                    //     'name'    => 'Ícone do Status',
                    // ),
                    // array(
                    //     'id'      => 'galeria_status',
                    //     'type'    => 'image_advanced',
                    //     'name'    => 'Galeria do Status',
                    // ),
                ),
                'clone'      => 1,
                'sort_clone' => 1,
                'tab'        => 'status_tab',
            ),
            array(
                'id'               => 'galeria_status',
                'type'             => 'file_advanced',
                'name'             => 'Andamento da Obra',
                'desc'             => 'Adicione as fotos do andamento da obra',
                'max_file_uploads' => 40,
                'tab'              => 'status_tab',
            ),



            array(
                'id'          => 'disab_planta',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab'       => 'plantas_tab',
                'std'         => array('1'),
            ),



            array(
                'id'        => 'plantas_emp',
                'type'      => 'heading',
                'name'      => 'Plantas',
                'tab'       => 'plantas_tab',
            ),
            array(
                'id'        => 'plantas_tit',
                'type'      => 'text',
                'name'      => 'Titulo Área de Plantas',
                'tab'       => 'plantas_tab',
                'desc' => 'Opcional',
            ),
            // array(
            //     'id' => 'bg_planta',
            //     'type' => 'image_advanced',
            //     'name' => 'Plano de fundo',
            //     'max_file_uploads' => 1,
            //     'tab' => 'plantas_tab',
            // ),
            array(
                'id'     => 'plantas_grupo',
                'type'   => 'group',
                'name'   => 'Plantas Itens',
                'fields' => array(
                    array(
                        'id'      => 'tit_planta',
                        'type'    => 'text',
                        'name'    => 'Título',
                        'columns' => 6,
                    ),

                    array(
                        'id'      => 'text_planta',
                        'type'    => 'wysiwyg',
                        'name'    => 'Texto de apoio',
                        
                    ),
                    array(
                        'id'        => 'img_planta',
                        'type'      => 'image_advanced',
                        'name'      => 'Imagem da planta',
                        'max_file_uploads' => 1,
                    ),
                ),
                'clone'         => 1,
                'sort_clone'    => 1,
                'tab'           => 'plantas_tab',
            ),


















            array(
                'id'          => 'disab_local',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab'   => 'local_tab',
                'std'         => array('1'),
            ),
            array(
                'id'    => 'heading_28',
                'type'  => 'heading',
                'name'  => 'Informações de Localização',
                'tab'   => 'local_tab',
            ),
            array(
                'id'    => 'logradouro_emp',
                'type'  => 'text',
                'name'  => 'Logradrou/Rua/Av',
                'tab'   => 'local_tab',
            ),
            array(
                'id'    => 'estado_emp',
                'type'  => 'text',
                'name'  => 'Estado',
                'tab'   => 'local_tab',
            ),
            // array(
            //     'id'    => 'cidade_emp',
            //     'type'  => 'text',
            //     'name'  => 'Cidade',
            //     'tab'   => 'local_tab',
            // ),
            array(
                'id'    => 'bairro_emp',
                'type'  => 'text',
                'name'  => 'Bairro',
                'tab'   => 'local_tab',
            ),












            array(
                'id'          => 'disab_ficha',
                'type'        => 'radio',
                'name'        => 'Desabilitar Área?',
                'options'     => array(
                    '1'       => 'Sim',
                    '2'       => 'Não',
                ),
                'tab'       => 'ficha_tab',
                'std'         => array('1'),
            ),

            array(
                'type'      => 'heading',
                'name'      => 'Ficha Técnica',
                'tab'       => 'ficha_tab',
            ),
            array(
                'id'        => 'ficha_tit',
                'type'      => 'text',
                'name'      => 'Titulo de Cabeçalho',
                'tab'       => 'ficha_tab',
                'desc' => 'Opcional',
            ),

            array(
                'id'        => 'img_ficha',
                'type'      => 'image_advanced',
                'name'      => 'Imagem Lateral',
                'max_file_uploads' => 1,
                'tab'       => 'ficha_tab',
            ),
            // array(
            //     'id' => 'bg_planta',
            //     'type' => 'image_advanced',
            //     'name' => 'Plano de fundo',
            //     'max_file_uploads' => 1,
            //     'tab' => 'ficha_tab',
            // ),
            array(
                'id'     => 'ficha_grupo',
                'type'   => 'group',
                'name'   => 'Itens',
                'fields' => array(

                    array(
                        'id'      => 'titulo',
                        'type'    => 'text',
                        'name'    => 'Título',
                        'columns' => 6,
                    ),

                    array(
                        'id'      => 'texto',
                        'type'    => 'text',
                        'name'    => 'Descrição',
                    ),
                   
                ),
                'clone'         => 1,
                'sort_clone'    => 1,
                'tab'           => 'ficha_tab',
            ),



            array(
                'id'        => 'local_rua',
                'type'      => 'text',
                'name'      => 'Rua',
                'tab'       => 'local_tab',
            ),

            array(
                'id'        => 'local_end',
                'type'      => 'text',
                'name'      => 'bairro - cidade',
                'tab'       => 'local_tab',
            ),








            array(
                'id'        => 'local_url_gm',
                'type'      => 'url',
                'name'      => 'Link Google maps',
                'desc'      => 'Clik para ir ao maps, busque o local e compartilhar, depois cole o link aqui. <a target="_blank" href="https://www.google.com.br/maps/preview">Google Maps</a>',
                'max_file_uploads' => 1,
                'tab'       => 'local_tab',
            ),


            array(
                'id'        => 'local_url_waze',
                'type'      => 'url',
                'name'      => 'Link Waze',
                'desc'      => 'Clik para ir ao waze, busque o local e compartilhar, depois cole o link aqui. <a target="_blank" href="https://www.waze.com/pt-BR/livemap/">Waze</a>',
                'max_file_uploads' => 1,
                'tab'       => 'local_tab',
            ),






            array(
                'id'     => 'loc_grupo',
                'type'   => 'group',
                'name'   => 'Locais Próximos',
                'fields' => array(
                   
                   
                    array(
                        'id'      => 'titulo',
                        'type'    => 'text',
                        'name'    => 'Título',
                    
                    ),
                    array(
                        'id'      => 'texto',
                        'type'    => 'text',
                        'name'    => 'descrição',
                    
                    ),

        
                ),
                'clone'         => 1,
                'sort_clone'    => 1,
                'tab'       => 'local_tab',
            ),





            array(
                'id'        => 'local_iframe',
                'type'      => 'textarea',
                'name'      => 'Mapa (google maps)',
                'desc'      => 'Clik para ir ao maps, busque o local e compartilhar,selecione "Incorporar um mapa", depois cole o codigo aqui. <a target="_blank" href="https://www.google.com.br/maps/preview">Google Maps</a>',
                'max_file_uploads' => 1,
                'tab'       => 'local_tab',
                'sanitize_callback' => 'none',
            ),























            // array(
            //     'id'               => 'bg_slide',
            //     'type'             => 'image_advanced',
            //     'name'             => 'Plano de fundo topo',
            //     'max_file_uploads' => 1,
            //     'tab'              => 'old_tab',
            // ),
            // array(
            //     'id'    => 'tit_topo',
            //     'type'  => 'text',
            //     'name'  => 'Título do Topo',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'          => 'form_interesse',
            //     'type'        => 'radio',
            //     'name'        => 'Remover Form Interesse?',
            //     'options'     => array(
            //         '1'       => 'Sim',
            //         '2'       => 'Não',
            //     ),
            //     'tab'         => 'old_tab',
            //     'std'         => array('2'),
            // ),
            // array(
            //     'id'    => 'info_botao_topo',
            //     'type'  => 'heading',
            //     'name'  => 'Informações do botão',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'      => 'text_do_botao',
            //     'type'    => 'text',
            //     'name'    => 'Texto do botão',
            //     'tab'     => 'old_tab',
            //     'columns' => 4,
            // ),
            // array(
            //     'id'      => 'link_botao_topo',
            //     'type'    => 'text',
            //     'name'    => 'Link do botão',
            //     'tab'     => 'old_tab',
            //     'columns' => 4,
            // ),
            // array(
            //     'id'          => 'tipo_botao_topo',
            //     'type'        => 'radio',
            //     'name'        => 'Tipo do botão',
            //     'options'     =>       array(
            //         '_self'   => 'Interno',
            //         '_targer' => 'Externo',
            //     ),
            //     'tab'         => 'old_tab',
            //     'columns'     => 4,
            //     'std'         => array('_self'),
            // ),
            // array(
            //     'id'    => 'conteudo_emp',
            //     'type'  => 'wysiwyg',
            //     'name'  => 'Contéudo',
            //     'tab'   => 'old_tab',
            //     'raw'   => 1,
            // ),
            // array(
            //     'id'    => 'vagas',
            //     'type'  => 'text',
            //     'name'  => 'Vagas de garagem',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'    => 'suites',
            //     'type'  => 'text',
            //     'name'  => 'Suítes',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'        => 'areas',
            //     'type'      => 'group',
            //     'name'      => 'Áreas (m²)',
            //     'tab'       => 'old_tab',
            //     'fields'    => array(
            //         array(
            //             'id'          => 'area_do_emp',
            //             'type'        => 'text',
            //             'placeholder' => 'ex: 214,36 m²',
            //             'clone'       => 1,
            //         ),
            //     ),
            // ),
            // // array(
            // //     'id'               => '360_emp',
            // //     'type'             => 'text',
            // //     'name'             => 'Link Imagens ',
            // //     'max_file_uploads' => 1,
            // //     'tab'              => 'old_tab',
            // // ),
            // array(
            //     'id'     => 'galeria_360_emp',
            //     'type'   => 'group',
            //     'name'   => '',
            //     'desc'   => 'Dimensão das imagens: <b>1536x1536</b> pixels',
            //     'fields' => array(
            //         array(
            //             'id'               => 'galeria_tit',
            //             'type'             => 'text',
            //             'name'             => '',
            //             'desc'             => 'Nome da Galeria',
            //         ),
            //         array(
            //             'id'               => 'imagens_360',
            //             'type'             => 'image_advanced',
            //             'name'             => '',
            //             'max_file_uploads' => 6,
            //             'desc'             => 'Ordem das imagens: Front, Left, Right, Back, Top, Bottom'
            //         ),
            //     ),
            //     'clone'      => 1,
            //     'sort_clone' => 1,
            //     'tab'        => 'old_tab',
            // ),
            // array(
            //     'id'               => 'tour_emp',
            //     'type'             => 'text',
            //     'name'             => 'Link Tour Virtual',
            //     'tab'              => 'old_tab',
            // ),
            // array(
            //     'id'               => 'folder_emp',
            //     'type'             => 'file_advanced',
            //     'name'             => 'Folder Empreendimento',
            //     'max_file_uploads' => 1,
            //     'tab'              => 'old_tab',
            // ),
            // array(
            //     'id'               => 'ficha_emp',
            //     'type'             => 'file_advanced',
            //     'name'             => 'Ficha Completa',
            //     'max_file_uploads' => 1,
            //     'tab'              => 'old_tab',
            // ),
            
            // array(
            //     'id'    => 'heading_tabela',
            //     'type'  => 'heading',
            //     'name'  => 'Tabelas de Valores',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'     => 'tabelas_emp',
            //     'type'   => 'group',
            //     'name'   => '',
            //     'desc'   => '',
            //     'fields' => array(
            //         array(
            //             'id'               => 'tabela_desc',
            //             'type'             => 'text',
            //             'name'             => '',
            //             'desc'             => 'Descrição do arquivo',
            //         ),
            //         array(
            //             'id'               => 'tabela_file',
            //             'type'             => 'file_advanced',
            //             'name'             => '',
            //             'desc'             => 'Tabela de Valores',
            //             'max_file_uploads' => 1,
            //         ),
            //     ),
            //     'clone'      => 1,
            //     'sort_clone' => 1,
            //     'tab'              => 'old_tab',
            // ),
            // // array(
            // //     'id'               => 'tabela_emp',
            // //     'type'             => 'file_advanced',
            // //     'name'             => 'Tabela de Valores',
            // //     'max_file_uploads' => 1,
            // //     'tab'              => 'old_tab',
            // // ),
            // array(
            //     'id'    => 'titulo_video',
            //     'type'  => 'text',
            //     'name'  => 'Título do Vídeo',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'    => 'youtube_url',
            //     'type'  => 'textarea',
            //     'name'  => 'Iframe do youtube',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'    => 'heading_28',
            //     'type'  => 'heading',
            //     'name'  => 'Informações de Localização',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'    => 'logradouro_emp',
            //     'type'  => 'text',
            //     'name'  => 'Logradrou/Rua/Av',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'    => 'estado_emp',
            //     'type'  => 'text',
            //     'name'  => 'Estado',
            //     'tab'   => 'old_tab',
            // ),
            // // array(
            // //     'id'    => 'cidade_emp',
            // //     'type'  => 'text',
            // //     'name'  => 'Cidade',
            // //     'tab'   => 'old_tab',
            // // ),
            // array(
            //     'id'    => 'bairro_emp',
            //     'type'  => 'text',
            //     'name'  => 'Bairro',
            //     'tab'   => 'old_tab',
            // ),
            
            // // array(
            // //     'id'    => 'lat_emp',
            // //     'type'  => 'text',
            // //     'name'  => 'Latitude',
            // //     'tab'   => 'old_tab',
            // // ),
            // // array(
            // //     'id'    => 'log_emp',
            // //     'type'  => 'text',
            // //     'name'  => 'Longitude',
            // //     'tab'   => 'old_tab',
            // // ),
            // array(
            //     'id'    => 'titulo_diferenciais',
            //     'type'  => 'text',
            //     'name'  => 'Título dos Diferenciais',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'          => 'diferenciais_grupo',
            //     'type'        => 'post',
            //     'name'        => 'Diferenciais',
            //     'post_type'   => 'diferenciais',
            //     'field_type'  => 'select_advanced',
            //     'multiple'     => true,
            //     'placeholder' => 'Diferenciais',
            //     'tab'        => 'old_tab',
            // ),
            // array(
            //     'id'               => 'galeria_empreendimento',
            //     'type'             => 'file_advanced',
            //     'name'             => 'Galeria do Empreendimento',
            //     'desc'             => 'Adicione as fotos da galeria do empreendimento',
            //     'max_file_uploads' => 20,
            //     'tab'              => 'galeria',
            // ),
            // array(
            //     'id'   => 'status_emp',
            //     'type' => 'heading',
            //     'name' => 'Status da Obra',
            //     'tab'  => 'old_tab',
            // ),
            // array(
            //     'id'    => 'previsao_entrega',
            //     'name'  => 'Previsão de Entrega',
            //     'type'  => 'date',
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'    => 'status_geral',
            //     'name'  => 'Status Geral',
            //     'type'  => 'range',
            //     'max'   => 100,
            //     'step'  => 1,
            //     'tab'   => 'old_tab',
            // ),
            // array(
            //     'id'     => 'status_grupo',
            //     'type'   => 'group',
            //     'name'   => 'Status da obra Itens',
            //     'fields' => array(
            //         array(
            //             'id'      => 'tit_status',
            //             'type'    => 'text',
            //             'name'    => 'Título',
            //             'columns' => 6,
            //         ),
            //         array(
            //             'id'      => 'porcentagem_status',
            //             'name'    => 'Porcentagem',
            //             'type'    => 'range',
            //             'max'     => 100,
            //             'step'    => 1,
            //             'columns' => 6,
            //         ),
            //         array(
            //             'id'      => 'galeria_icone',
            //             'type'    => 'text',
            //             'name'    => 'Ícone do Status',
            //         ),
            //         // array(
            //         //     'id'      => 'galeria_status',
            //         //     'type'    => 'image_advanced',
            //         //     'name'    => 'Galeria do Status',
            //         // ),
            //     ),
            //     'clone'      => 1,
            //     'sort_clone' => 1,
            //     'tab'        => 'old_tab',
            // ),
            // array(
            //     'id'               => 'galeria_status',
            //     'type'             => 'file_advanced',
            //     'name'             => 'Andamento da Obra',
            //     'desc'             => 'Adicione as fotos do andamento da obra',
            //     'max_file_uploads' => 40,
            //     'tab'              => 'old_tab',
            // ),
            // array(
            //     'id'        => 'plantas_emp',
            //     'type'      => 'heading',
            //     'name'      => 'Plantas',
            //     'tab'       => 'old_tab',
            // ),
            // array(
            //     'id'        => 'plantas_tit',
            //     'type'      => 'text',
            //     'name'      => 'Titulo Área de Plantas',
            //     'tab'       => 'old_tab',
            // ),
            // array(
            //     'id' => 'bg_planta',
            //     'type' => 'image_advanced',
            //     'name' => 'Plano de fundo',
            //     'max_file_uploads' => 1,
            //     'tab' => 'old_tab',
            // ),
            // array(
            //     'id'     => 'plantas_grupo',
            //     'type'   => 'group',
            //     'name'   => 'Plantas Itens',
            //     'fields' => array(
            //         array(
            //             'id'      => 'tit_planta',
            //             'type'    => 'text',
            //             'name'    => 'Título',
            //             'columns' => 6,
            //         ),
            //         array(
            //             'id'        => 'img_planta',
            //             'type'      => 'image_advanced',
            //             'name'      => 'Imagem da planta',
            //             'columns'   => 6,
            //         ),
            //     ),
            //     'clone'         => 1,
            //     'sort_clone'    => 1,
            //     'tab'           => 'old_tab',
            // ),
















            







        ),
        'tab_style'   => 'left',
        'tab_wrapper' => true,
        'tabs'        => array(
            'topo_tab'  => array(
                'label'     => 'Capa',
                'icon'      => 'dashicons-format-gallery',
            ),
            'info_tab'  => array(
                'label'     => 'Mais Informaçôes',
                'icon'      => 'dashicons-plus',
            ),
            'video_tab'  => array(
                'label'     => 'Video',
                'icon'      => 'dashicons-video-alt3',
            ),
            'diferencias_tab' => array(
                'label'       => 'Diferenciais',
                'icon'        => 'dashicons-chart-bar',
            ),

            'galeria'   => array(
                'label' => 'Galeria do Imóvel',
                'icon'  => 'dashicons-images-alt2',
            ),

            '360' =>     array(
                'label' => 'Imagens 360',
                'icon' => 'dashicons-images-alt',
            ),

            'plantas_tab' =>     array(
                'label' => 'Plantas',
                'icon' => 'dashicons-location-alt',
            ),
            'ficha_tab' =>     array(
                'label' => 'Ficha Técnica',
                'icon' => 'dashicons-editor-ul',
            ),

            'status_tab' => array(
                'label' => 'Status da obra',
                'icon' => 'dashicons-chart-line',
            ),

            'local_tab' => array(
                'label' => 'Localização',
                'icon' => 'dashicons-location',
            ),
            'old_tab' => array(
                'label' => 'Antigos',
                'icon' => 'dashicons-location',
            ),
        ),
    );














    //==============================================
    // SLIDES
    //==============================================

    $meta_boxes[] = array(
        'id' => 'slides',
        'title' => 'Detalhes do Slide',
        'pages' => array('slides'),
        'context' => 'normal',
        'priority' => 'high',

        'fields' => array(

            
            array(
                'id'      => 'slide_texto_ativo',
                'name'    => "Desativar conteudo dinamico",
                'type'    => 'radio',
                'std'     => "N\xc3\xa3o",
                'options' => array(
                    'Sim'        => 'Sim',
                    "N\xc3\xa3o" => "N\xc3\xa3o",
                ),
               
            ),


            array(
                'name'       => 'Alinhamento do conteudo',
                'id'         => "slide_bloco_alinhamento",
                'type'       => 'radio',
                'options'    => array('left' => 'Esquerda', 'center' => 'Centro', 'right' => 'Direita'),
                'std'        => 'left',
                'columns' => 4,
                'admin_columns' => 'after title',
                'visible' => array( 'slide_texto_ativo', "N\xc3\xa3o" ),
            ),



            array(
                'name'       => 'Titulo',
                'id'         => 'slide_title',
                'desc'       => 'Titulo do Slide',
                'type'       => 'textarea',
                'required'   => true,
                'visible' => array( 'slide_texto_ativo', "N\xc3\xa3o" ),
            ),

            array(
                'name'       => 'Descrição',
                'id'         => "slide_desc",
                'desc'       => 'Descrição',
                'type'       => 'wysiwyg',
                'required'   => true,
                'options' => array(
                    'textarea_rows' => 4,
                    'teeny'         => true,
                    'media_buttons' => false,
                ),
                'visible' => array( 'slide_texto_ativo', "N\xc3\xa3o" ),
            ),

            array(
                'type' => 'heading',
                'name' => 'Configurações de Link',
                'visible' => array( 'slide_texto_ativo', "N\xc3\xa3o" ),
            ),

           



            array(
                'name'       => 'Link',
                'id'         => "slide_link",
                'type'       => 'text',
                'required'   => true,
                'columns' => 4,
                'admin_columns' => 'after title',
            
            ),

            array(
                'name'       => 'Target do Link',
                'id'         => "slide_target",
                'type'       => 'radio',
                'options'    => array('_self' => 'Interno', '_blank' => 'Externo'),
                'std'        => '_self',
                'columns' => 4,
                'admin_columns' => 'after title',
               
            ),

            array(
                'name'       => 'Texto Botão',
                'id'         => "slide_btn",
                'type'       => 'text',
                'columns' => 4,
                'admin_columns' => 'after title',
                'visible' => array( 'slide_texto_ativo', "N\xc3\xa3o" ),
            ),
        )
    );



    $meta_boxes[] = array(
        'id'        => 'scripts-do-empreendimento',
        'title'     => 'Scripts adicionais',

        'pages'     =>   array('empreendimento'),
        'context'   => 'normal',
        'priority'  => 'high',
        'autosave'  => true,
        'fields'    => array(
            array(
                'name'    => 'Scripts',
                'type'    => 'heading',
                'desc' => 'CUIDADO! apenas edite o campo se tiver certeza absoluta do que está fazendo!',
            ),

            array(
                'id'      => 'script_head',
                'name'    => 'Head',
                'type'    => 'textarea',
                'sanitize_callback' => 'none',
            ),
            array(
                'id'      => 'script_body',
                'name'    => 'Body',
                'type'    => 'textarea',
                'sanitize_callback' => 'none',
            ),


            array(
                'id'      => 'script_footer',
                'name'    => 'Footer',
                'type'    => 'textarea',
                'sanitize_callback' => 'none',
            ),




            array(
                'name'    => 'Scripts da página de redirecionamento.',
                'type'    => 'heading',
                'desc' => 'os campos a baixo iram aparecer na página de redirecionamento após o cadastro no formulário, recomendado para scripts de evento',
            ),


            array(
                'id'      => 'script_head_redirect',
                'name'    => 'Head',
                'type'    => 'textarea',
                'sanitize_callback' => 'none',
            ),
            array(
                'id'      => 'script_body_redirect',
                'name'    => 'Body',
                'type'    => 'textarea',
                'sanitize_callback' => 'none',
            ),


            array(
                'id'      => 'script_footer_redirect',
                'name'    => 'Footer',
                'type'    => 'textarea',
                'sanitize_callback' => 'none',
            ),

        ),
    );

    //=========================================================================================
    // END DEFINITION OF META BOXES
    //=========================================================================================
    return $meta_boxes;
}
