<?php

add_filter( 'rwmb_meta_boxes', 'wpcf_meta_boxes_pages' );
function wpcf_meta_boxes_pages($meta_boxes) {

    $meta_boxes[] = array (
        'id'        => 'new-thumbnail',
        'title'     => 'Imagem de capa',
        'pages'     => array('page'),
        'context'   => 'side',
        'priority'  => 'high',
        'fields'    => array(
            array(
                'id'               => 'meta-thumbnail_desktop',
                'type'             => 'image_advanced',
                'name'             => 'Capa Desktop',
                'max_file_uploads' => 1,
            ),
            array(
                'id'               => 'meta-thumbnail_mobile',
                'type'             => 'image_advanced',
                'name'             => 'Capa Mobile',
                'max_file_uploads' => 1,
            ),
        ),
    );
//=========================================================================================
// PAGES CONTATO
//=========================================================================================

$meta_boxes[] = array (
    'id'        => 'informacoes-para-mapa',
    'title'     => 'Informações para mapa',
    'pages'     => array('page'),
    'context'   => 'normal',
    'priority'  => 'high',
    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-contato.php','page-venda.php'),
    ),
    'fields'    => array(
        array(
            'id'   => 'address',
            'name' => 'Endereço',
            'type' => 'text',
            'std'  => 'Av. Dom Luís, 500, sl 624  Aldeota - Fortaleza',
        ),
        array(
            'id' => 'map_contato',
            'type' => 'map',
            'name' => 'Map',
            'address_field' => 'address',
            'api_key'       => 'AIzaSyCyRR6NXLgFuiJ1oZu7jwSduBr1CRySbb4',
        ),
    ),
);

//=========================================================================================
// PAGES FORNECEDORES
//=========================================================================================

$meta_boxes[] = array (
    'id'        => 'etapas-fornecedores',
    'title'     => 'Etapas Fornecedores',
    'pages'     =>   array('page'),
    'context'   => 'normal',
    'priority'  => 'high',
    'include' => array(
        'relation'  => 'OR',
        'template'  => 'page-fornecedores.php',
    ),
    'fields'    =>   array(
        array(
            'id'     => 'etapa_for',
            'type'   => 'group',
            'name'   => 'Etapas',
            'fields' => array(
                array(
                    'id'    => 'desc_etapa',
                    'type'  => 'textarea',
                    'name'  => 'Descrição da Etapa',
                ),
            ),
            'clone'      => 1,
            'sort_clone' => 1,
        ),
    ),
);

//=========================================================================================
// PAGES SOBRE
//=========================================================================================

$meta_boxes[] = array(
    'id'       => 'informacoes-sobre',
    'title'    => 'Informações Sobre',
    'pages'    => array('page'),
    'context'  => 'normal',
    'priority' => 'high',
    'include' => array(
        'relation'  => 'OR',
        'template'  => 'page-institucional.php',
    ),
    'fields'   => array(
        array(
            'id'   => 'subtit_sobre',
            'type' => 'text',
            'name' => 'Subtítulo',
        ),
        array(
            'id'               => 'img_bloco_1',
            'type'             => 'image_advanced',
            'name'             => 'Bloco 1 Imagem',
            'max_file_uploads' => 1,
        ),
        array(
            'id'    => 'content_bloco_1',
            'name'  => 'Bloco 1 Conteúdo',
            'type'  => 'wysiwyg',
        ),
        array (
            'id'    => 'divider_5',
            'type'  => 'divider',
            'name'  => 'Divider',
        ),
        array(
            'id'               => 'img_bloco_2',
            'type'             => 'image_advanced',
            'name'             => 'Bloco 2 Imagem',
            'max_file_uploads' => 1,
        ),
        array(
            'id'    => 'content_bloco_2',
            'name'  => 'Bloco 2 Conteúdo',
            'type'  => 'wysiwyg',
        ),
        array(
            'id'    => 'divider_6',
            'type'  => 'divider',
            'name'  => 'Divider',
        ),
        array(
            'id'    => 'iframe_sobre_video',
            'type'  => 'textarea',
            'name'  => 'Iframe do Vídeo',
        ),
        array(
            'id'    => 'heading_8',
            'type'  => 'heading',
            'name'  => 'Informações para Portfólio',
        ),
        array(
            'id'    => 'tit_port',
            'type'  => 'text',
            'name'  => 'Título Portfólio',
        ),
        array(
            'id'    => 'desc_port',
            'type'  => 'textarea',
            'name'  => 'Descrição portfólio',
        ),
        array(
            'id'                => 'arq_brasil_port',
            'type'              => 'file_advanced',
            'name'              => 'Arquivo em Português',
            'max_file_uploads'  => 1,
        ),
        array(
            'id'               => 'arq_eua_port',
            'type'             => 'file_advanced',
            'name'             => 'Arquivo em Inglês',
            'max_file_uploads' => 1,
        ),
  ),
);




//=========================================================================================
// PAGES EMPREENDIMENTOS
//=========================================================================================

$meta_boxes[] = array (
    'id'        => 'key_produto',
    'title'     => 'Key do empreendimento',
    'pages'     =>   array('empreendimento'),
    'context'   => 'side',
    'priority'  => 'high',
    'fields'    =>   array(
        array(
            'id'    => 'key_emp_produto',
            'type'  => 'text',
            'name'  => 'COD. do Empreendimento',
            'desc'  => 'codigo para integração com o sistema Anapro | ProdutoKey',
            'admin_columns' => 'after title'
        ),

        array(
            'id'    => 'key_emp_campanha',
            'type'  => 'text',
            'name'  => 'COD. da Campanha do Empreendimento',
            'desc'  => 'codigo para integração com o sistema Anapro | CampanhaKey',
            'admin_columns' => 'after title'
        ),
    ),
);


//=========================================================================================
// END DEFINITION OF META BOXES
//=========================================================================================
    return $meta_boxes;
}