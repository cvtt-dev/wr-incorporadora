<?php
/*
* AJAX functions
* Desenvolvedor: Bruno Lima
*/

//=========================================================================================
// MAILCHIMP
//=========================================================================================

function mailchimp_ajax_newsletter() {

    $mailchimp  = new mailchimp_form();
    $email      = $_POST['email'];
    $uf         = $_POST['uf'];
    $cidade     = $_POST['cidade'];

    echo json_encode( array(
            'error' => array (
            'add_list'  => $mailchimp->lead($email, $uf, $cidade),
            )
        ));
    exit;
}

add_action('wp_ajax_newsletter', 'mailchimp_ajax_newsletter');
add_action('wp_ajax_nopriv_newsletter', 'mailchimp_ajax_newsletter');

//=========================================================================================
// GET UNIDADES + GET ESTADOS
//=========================================================================================

//GET ESTADOS
function getRegiaoAjax() {

    $id     = $_POST['term'];
    $listas = array();
    $select = array();
    $args = array(
        'child_of' => $id,
        'taxonomy' => 'regiao',
        // 'hide_empty' => true,
        'hierarchical' => true,
    );

    $termchildren = get_terms('regiao', $args );

    foreach ( $termchildren as $term ) {
        if($term->parent == $id) {
            $listas[] = '<li data-id="'.$term->term_id.'" data-value="'.$term->slug.'"><span>'.$term->name.'</span></li>';
            $select[] = '<option value="'.$term->slug.'">'.$term->name.'</option>';
        }
    }

    echo json_encode( array(
        'listas' => array($listas),
        'select' => array($select),
    ));

    exit;
}

add_action('wp_ajax_regiao', 'getRegiaoAjax');
add_action('wp_ajax_nopriv_regiao', 'getRegiaoAjax');

//GET ESTADOS
function getEstadosAjax() {

    $id     = $_POST['pais'];
    $listas = array();
    $select = array();
    $args = array(
        'child_of' => $id,
        'taxonomy' => 'localizacao',
        'hide_empty' => true,
        'hierarchical' => true,
        'depth'  => 1,
    );
    $termchildren = get_terms('localizacao', $args );

    foreach ( $termchildren as $term ) {
        $listas[] = '<li data-value="'.$term->slug.'"><span>'.$term->name.'</span></li>';
        $select[] = '<option value="'.$term->slug.'">'.$term->name.'</option>';
    }

    echo json_encode( array(
        'listas' => array($listas),
        'select' => array($select),
    ));

    exit;
}

add_action('wp_ajax_estados', 'getEstadosAjax');
add_action('wp_ajax_nopriv_estados', 'getEstadosAjax');

//GET UNIDADES
function getUnidadesAjax() {

    $id     = $_POST['pais'];
    $type   = $_POST['tipo'];
    $listas = array();
    $select = array();

    if($id == 0) :
        $args = array('post_type' => 'unidades', 'showposts' => -1, 'orderby' => 'title', 'order' => ASC);
    else :
        $args = array('post_type' => 'unidades',
        'showposts' => -1,
         'tax_query' => array(
            array(
                'taxonomy' => 'localizacao',
                'terms'    => array($id),
                'field'    => 'term_id',
            )
        ),
        'orderby' => 'title',
        'order' => ASC);
    endif;

    $unidades = new WP_Query($args);
    while ($unidades->have_posts()) : $unidades->the_post();
        $listas[] = '<li data-value="'.get_the_id().'"><span>'.get_the_title().'</span></li>';
        $select[] = '<option value="'.get_the_id().'">'.get_the_title().'</option>';
    endwhile;
    wp_reset_postdata();

    echo json_encode( array(
        'listas' => array($listas),
        'select' => array($select),
    ));

    exit;
}

add_action('wp_ajax_unidades', 'getUnidadesAjax');
add_action('wp_ajax_nopriv_unidades', 'getUnidadesAjax');

//=========================================================================================
// GET UF/CIDADES
//=========================================================================================

function getcidadesAjax() {

    global $wpdb;
    $tabela  = $wpdb->prefix."ajaxcidade";
    $estados = $_POST['estado'];

    if($estados == '') : echo '<option value="">Cidade</option>'; else :
        $myrows  = $wpdb->get_results( "SELECT * FROM $tabela WHERE estados_cod_estados=$estados GROUP BY nome ORDER BY nome");

        // print_r($myrows);

        echo '<option value="" selected="selected">Cidade</option>';
        foreach ($myrows as $row) {
            echo '<option value="'.$row->nome.'">'.$row->nome.'</option>';
        }
    endif;
    exit;
}

add_action('wp_ajax_getcidades', 'getcidadesAjax');
add_action('wp_ajax_nopriv_getcidades', 'getcidadesAjax');

    
    
function send_leads_zapier() {

    $url        = 'http://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect';
    $nome       = $_POST['nome'];
    $email      = $_POST['email'];
    $fone       = $_POST['tel'];
    $msg        = $_POST['msg'];
    

    $response = wp_remote_post( $url, array(
        'method'      => 'POST',
        'timeout'     => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking'    => true,
        'headers'     => array(),
        'body'        => array(
            'Key' => 'hQwqhV7ob41',
            'CampanhaKey'=> 'lOZeas28YCE1',
            'ProdutoKey'=> 'J4R-QPgwww1',
            'CanalKey'=> 'RQ8www8OEaM1',
            'Midia'=> 'PostMan',
            'PessoaNome'=> $nome,
            'PessoaEmail'=> $email,
            'Observacoes'=> $msg,
            'KeyIntegradora'=> '2456902B-10A2-4AA7-8549-205AC6FBDEC4',
            'PessoaTelefones[0].DDD'=> '11',
            'PessoaTelefones[0].Numero'=> $fone 
        ),
        'cookies'     => array()
        )
    );

    if ( is_wp_error( $response ) ) {
        $error_message = $response->get_error_message();
        echo "Something went wrong: $error_message";
    } else {
        echo 'Response:<pre>';
        print_r( $response );
        echo '</pre>';
    }
    
    exit;

    
}
//add_action('wp_ajax_send_leads_zapier', 'send_leads_zapier');
//add_action('wp_ajax_nopriv_send_leads_zapier', 'send_leads_zapier');

