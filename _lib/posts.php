<?php
/**
* Custom Post Types
* Desenvolvedor: Bruno Lima
*/

//=========================================================================================
// POST TYPE PORTFOLIO
//=========================================================================================

function post_type_portfolio_register() {
    $labels = array(
        'name' => 'Portfólio',
        'singular_name' => 'Portfolios',
        'menu_name' => 'Portfólios',
        'add_new' => _x('Adicionar Portfólio', 'item'),
        'add_new_item' => __('Adicionar Novo Portfólio'),
        'edit_item' => __('Editar Portfólio'),
        'new_item' => __('Novo Portfólio')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'portfolios'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-media-interactive',
        'supports' => array('title', 'thumbnail')
    );
    register_post_type('portfolios', $args);
}
add_action('init', 'post_type_portfolio_register');

//=========================================================================================
// POST TYPE DIFERENCIAIS
//=========================================================================================

function post_type_diferenciais_register() {
    $labels = array(
        'name' => 'Diferenciais',
        'singular_name' => 'Diferenciais',
        'menu_name' => 'Diferenciais',
        'add_new' => _x('Adicionar Diferenciais', 'item'),
        'add_new_item' => __('Adicionar Novo Diferencial'),
        'edit_item' => __('Editar Diferencial'),
        'new_item' => __('Novo Diferencial')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'diferenciais'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-appearance',
        'supports' => array('title', 'thumbnail')
    );
    register_post_type('diferenciais', $args);
}
add_action('init', 'post_type_diferenciais_register');

//=========================================================================================
// POST TYPE EMPREENDIMENTO
//=========================================================================================

function post_type_empreendimento_register() {
    $labels = array(
        'name' => 'Empreendimento',
        'singular_name' => 'Empreendimento',
        'menu_name' => 'Empreendimentos',
        'add_new' => _x('Adicionar Empreendimento', 'item'),
        'add_new_item' => __('Adicionar Novo Empreendimento'),
        'edit_item' => __('Editar Empreendimento'),
        'new_item' => __('Novo Empreendimento')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'empreendimento'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-building',
        'supports' => array('title', 'thumbnail')
    );
    register_post_type('empreendimento', $args);
}
add_action('init', 'post_type_empreendimento_register');

//=========================================================================================
// POST TYPE NOTICIAS
//=========================================================================================

function post_type_imprensa_register() {
    $labels = array(
        'name' => 'Imprensa',
        'singular_name' => 'Imprensa',
        'menu_name' => 'Imprensa',
        'add_new' => _x('Adicionar Release', 'item'),
        'add_new_item' => __('Adicionar Novo Release'),
        'edit_item' => __('Editar Release'),
        'new_item' => __('Novo Release')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'imprensa'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-format-aside',
        'supports' => array('title','editor', 'thumbnail')
    );
    register_post_type('imprensa', $args);
}
add_action('init', 'post_type_imprensa_register');

function post_type_responsabilidade_register() {
    $labels = array(
        'name' => 'Responsabilidade Social',
        'singular_name' => 'Responsabilidade Social',
        'menu_name' => 'Resp. Social',
        'add_new' => _x('Adicionar Responsabilidade', 'item'),
        'add_new_item' => __('Adicionar Nova Responsabilidade'),
        'edit_item' => __('Editar Responsabilidade'),
        'new_item' => __('Novo Responsabilidade')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'responsabilidade'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-format-aside',
        'supports' => array('title','editor', 'thumbnail')
    );
    register_post_type('responsabilidade', $args);
}
add_action('init', 'post_type_responsabilidade_register');

//=========================================================================================
// POST TYPE SLIDES
//=========================================================================================

function post_type_slide_register() {
    $labels = array(
        'name' => 'Slides',
        'singular_name' => 'Slide',
        'menu_name' => 'Slides',
        'add_new' => _x('Adicionar Slide', 'item'),
        'add_new_item' => __('Adicionar Novo Slide'),
        'edit_item' => __('Editar Slide'),
        'new_item' => __('Novo Slide')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slides'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-images-alt2',
        'supports' => array('title', 'thumbnail')
    );
    register_post_type('slides', $args);
}
add_action('init', 'post_type_slide_register');

//=========================================================================================
// POST TYPE DOWNLOAD
//=========================================================================================

function post_type_download_register() {
    $labels = array(
        'name' => 'Downloads',
        'singular_name' => 'Download',
        'menu_name' => 'Downloads',
        'add_new' => _x('Adicionar Download', 'item'),
        'add_new_item' => __('Adicionar Novo Download'),
        'edit_item' => __('Editar Download'),
        'new_item' => __('Novo Download')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'downloads'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-download',
        'supports' => array('title')
    );
    register_post_type('downloads', $args);
}
add_action('init', 'post_type_download_register');

//=========================================================================================
// POST TYPE DEPOIMENTOS
//=========================================================================================

function post_type_depoimento_register() {
    $labels = array(
        'name' => 'Depoimentos',
        'singular_name' => 'Depoimento',
        'menu_name' => 'Depoimentos',
        'add_new' => _x('Adicionar Depoimento', 'item'),
        'add_new_item' => __('Adicionar Novo Depoimento'),
        'edit_item' => __('Editar Depoimento'),
        'new_item' => __('Novo Depoimento')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'depoimento'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-editor-quote',
        'supports' => array('title')
    );
    register_post_type('depoimentos', $args);
}
add_action('init', 'post_type_depoimento_register');

//=========================================================================================
// POST TYPE PARCEIROS
//=========================================================================================

function post_type_parceiros_register() {
    $labels = array(
        'name' => 'Parceiro',
        'singular_name' => 'Parceiro',
        'menu_name' => 'Parceiros',
        'add_new' => _x('Adicionar Parceiro', 'item'),
        'add_new_item' => __('Adicionar Novo Parceiro'),
        'edit_item' => __('Editar Parceiro'),
        'new_item' => __('Novo Parceiro')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'parceiros'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-groups',
        'supports' => array('title', 'thumbnail')
    );
    register_post_type('parceiros', $args);
}
add_action('init', 'post_type_parceiros_register');

//=========================================================================================
// POST TYPE CARIRI STORE
//=========================================================================================

function post_type_cariri_register() {
    $labels = array(
        'name' => 'Cariri Store',
        'singular_name' => 'Cariri Store',
        'menu_name' => 'Cariri Store',
        'add_new' => _x('Adicionar Empreendimento', 'item'),
        'add_new_item' => __('Adicionar Novo Empreendimento'),
        'edit_item' => __('Editar Empreendimento'),
        'new_item' => __('Novo Empreendimento')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'cariri'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-cart',
        'supports' => array('title', 'thumbnail')
    );
    register_post_type('cariri', $args);
}
add_action('init', 'post_type_cariri_register');