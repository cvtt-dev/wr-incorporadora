<?php /* Template Name: Institucional  */ ?>
<?php
get_template_part('templates/html', 'header');

while (have_posts()) : the_post();
	$subtit = get_post_meta(get_the_id(), 'subtit_sobre', true);
	$img_bloco1 = get_post_meta(get_the_id(), 'img_bloco_1', true);
	$img_bloco1_obj =  wp_get_attachment_image_src($img_bloco1, 'full');
	$cont_bloco1 = get_post_meta(get_the_id(), 'content_bloco_1', true);

	$img_bloco2 = get_post_meta(get_the_id(), 'img_bloco_2', true);
	$img_bloco2_obj =  wp_get_attachment_image_src($img_bloco2, 'full');
	$cont_bloco2 = get_post_meta(get_the_id(), 'content_bloco_2', true);

	$iframe = get_post_meta(get_the_id(), 'iframe_sobre_video', true);

	$titDownload = get_post_meta(get_the_id(), 'tit_port', true);
	$descDownload = get_post_meta(get_the_id(), 'desc_port', true);
	$arqBrDownload = get_post_meta(get_the_id(), 'arq_brasil_port', true);
	$arqEuaDownload = get_post_meta(get_the_id(), 'arq_eua_port', true);
	$capa_mobile = get_post_meta(get_the_ID(), 'meta-thumbnail_mobile', false);
	$capa_desktop = get_post_meta(get_the_ID(), 'meta-thumbnail_desktop', false);
?>

	<article class="pages">
		<header class="header-img">
		<?php
            echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_mobile[0]) . '" class="class-capa-mobile header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_mobile[0]) . '">';
            if ($capa_desktop) {
                echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_desktop[0]) . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_desktop[0]) . '">';
            } else {
                echo '<img width="1920" height="367" alt="" data-src="' . the_post_thumbnail_url() . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . the_post_thumbnail_url() . '">';
            }
            ?>
		</header>

		<section class="container">
			<div class="header-tit">
				<h2 class="tit-border tit-border--center"><?php echo $subtit; ?></h2>
			</div>

			<?php if ($img_bloco1 || $cont_bloco1) : ?>
				<div class="col-text">
					<figure class="col-text__thumb">
						<img src="<?php echo $img_bloco1_obj[0]; ?>" width="<?php echo $img_bloco1_obj[1]; ?>" height="<?php echo $img_bloco1_obj[3]; ?>">
					</figure>

					<div class="col-text__desc">
						<?php echo $cont_bloco1; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if ($img_bloco2 || $cont_bloco2) : ?>
				<div class="col-text col-text--right">
					<figure class="col-text__thumb">
						<img src="<?php echo $img_bloco2_obj[0]; ?>" width="<?php echo $img_bloco2_obj[1]; ?>" height="<?php echo $img_bloco2_obj[3]; ?>">
					</figure>

					<div class="col-text__desc">
						<?php echo $cont_bloco2; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if ($iframe) : ?>
				<div class="box-iframe box-iframe--pad"><?php echo $iframe; ?></div>
			<?php endif; ?>
		</section>

		<?php if ($arqBrDownload || $arqEuaDownload) : ?>
			<section class="cta-download">
				<div class="container">
					<div class="cta-download__flex">
						<div class="cta-download__tit-box">
							<i class="icon icon-livro"></i>
							<h2 class="cta-download__tit"><?php echo $titDownload; ?></h2>
						</div>

						<div class="cta-download__desc">
							<p><?php echo $descDownload; ?></p>
						</div>

						<div class="cta-download__btns">
							<?php if ($arqBrDownload) : ?>
								<a href="<?php echo get_attached_file($arqBrDownload); ?>" class="cta-download__btn"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-brasil.png" alt="Imagme bandeira do Brasil">Baixar</a>
							<?php endif; ?>

							<?php if ($arqEuaDownload) : ?>
								<a href="<?php echo get_attached_file($arqEuaDownload); ?>" class="cta-download__btn"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-eua.png" alt="Imagem bandeira dos Estados Unidos">Download</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>

		<?php  /* DEPOIMENTOS
		$args = array('posts_per_page' => -1, 'post_type' => 'depoimentos');
		$depoimento = new WP_Query($args);
		if ($depoimento->have_posts()) :
	?>
	<section class="depoimentos">
		<div class="container">
			<div class="header-tit">
				<h2 class="tit-border tit-border--center">Depoimentos</h2>
			</div>

			<div class="owl-depoimentos">
				 <?php
				 	while ($depoimento->have_posts()) :$depoimento->the_post();
					 	$descDepoimento = get_post_meta( get_the_id(), 'desc_depoimento', true);
					 	$cargoDepoimento = get_post_meta( get_the_id(), 'cargo_depoimento', true);
				 ?>
				<blockquote class="depoimento">
					<div class="depoimento__mensagem">
						<p><?php echo $descDepoimento; ?></p>
					</div>
					<cite class="depoimento__autor"><b><?php echo get_the_title(); ?> </b>-<?php echo $cargoDepoimento; ?></cite>
				</blockquote>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
	<?php endif; */ ?>

		<?php /* PARCEIROS
		$args = array('posts_per_page' => -1, 'post_type' => 'parceiros');
		$parceiros = new WP_Query($args);
		if ($parceiros->have_posts()) :
	?>
	<section class="parceiros">
		<div class="container">
			<div class="header-tit">
				<h2 class="tit-border tit-border--center">Nossos parceiros</h2>
			</div>

			<div class="owl-parceiros">
				<?php
				    while ( $parceiros->have_posts()) : $parceiros->the_post();
						the_post_thumbnail('full', array('alt' => get_the_title() ));
					endwhile; wp_reset_postdata();
				?>
			</div>
		</div>
	</section>
	<?php endif; */ ?>

		<section class="cta-resp">
			<div class="cta-resp__thumb">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bg-responsabilidade-social.jpg" alt="">
			</div>

			<div class="cta-resp__infos">
				<h2 class="cta-resp__tit">Responsabilidade Social</h2>
				<div class="cta-resp__desc">
					<p>Nossa postura consiste em qualificar constantemente nossos colaboradores, adotando ações que respeitam nossa equipe, clientes e, principalmente, o meio ambiente.</p>
				</div>

				<!-- <a href="<?php //echo get_post_type_archive_link('responsabilidade'); 
								?>" class="cta-resp__btn">Saiba mais</a> -->
			</div>
		</section>

	</article>
<?php endwhile;
wp_reset_postdata(); ?>
<?php //get_template_part('templates/section','imoveis-avulsos'); 
?>
<?php get_template_part('templates/html', 'footer'); ?>