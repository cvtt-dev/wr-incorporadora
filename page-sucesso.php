<?php /* Template Name: Página Sucesso */ ?>
<?php get_template_part('templates/html','header'); ?>

<style>
    .pages {
        height: 520px;
        width: 100%;
        float: left;
    }

    .section-obrigado:before {
        content: '';
        background: rgba(2,2,2,0.5);
        width: 100%;
        height: 100%;
        position: absolute;
    }
    .section-obrigado {
        width: 100%;
        float: left;
        height: 220px;
        position: relative;
        display: flex;
        align-items: center;
    }
    .section-obrigado__title {
        font-weight: 100;
        margin-bottom: 15px;
        text-align: center;
        color: white;
        z-index: 9999;
        position: relative;
    }
    .section-obrigado__desc p {
        text-align: center;        
        color: white;
        z-index: 9999;
        position: relative;
    }
    .section-materiais {
        width: 100%;
        float: left;  
        padding: 60px 0; 
    }
    .section-materiais__title {
        text-align: center;
        margin: 0;
    }
    .section-materiais__ctas {
        display: flex;
        align-items: center;
        justify-content: center;
        max-width: 60%;
        margin: 60px auto;
    }
    .section-materiais__ctas a {    
        margin: 6px;
    }
    .section-materiais__ctas a:hover { 
        background: #b93027;
        color: white;
    }   
</style>
<?php while (have_posts()) : the_post(); 

$thumb      = get_the_post_thumbnail_url(get_the_ID(),'full'); 


$id_emp  = $_GET['pageid'];

?>

<div class="pages">

    <section class="section-obrigado" style="background: url('<?php echo $thumb; ?>');">
        <div class="container">
            <h2 class="section-obrigado__title"><?php the_title(); ?></h2>   
            <div class="section-obrigado__desc"><?php the_content(); ?></div>
        </div>
    </section>
	<section class="section-materiais">
        <div class="container">
            <h2 class="section-materiais__title">Baixe nossos materiais</h2>

            <?php 
                $args = array('post_type' => 'empreendimento', 'p' => $id_emp,'orderby' => 'date', 'order' => 'DESC');
                $emp  = new WP_Query($args);
            ?>

            <?php while ( $emp->have_posts() ) : $emp->the_post(); 
                $tabelas 	= get_post_meta(get_the_ID(), 'tabelas_emp', true);
                $empFolder = get_post_meta(get_the_id(), 'folder_emp', true);
                $empFicha = get_post_meta(get_the_id(), 'ficha_emp', true);
                $btn_coring_tit = get_post_meta(get_the_id(), 'ficha_jkbtn_tit', true);
                $btn_coring_link = get_post_meta(get_the_id(), 'ficha_jkbtn_link', true);

            ?>

            <div class="section-materiais__ctas">
				<?php if ($tabelas) :
                    
                    if (count($tabelas) == 1) : ?>
					
                        <a href="<?php echo wp_get_attachment_url($tabelas[0]['tabela_file'][0]); ?>" target="_blank" download="<?php echo $tabelas[0]['tabela_desc']; ?>" class="wr-btn wr-btn--line wr-btn--download">
						
                    <?php else : ?>
					
                        <a href="#tabelas-modal" data-effect="mfp-move-horizontal" class="wr-btn wr-btn--line wr-btn--download open-modal">
					
                    <?php endif; ?>

						<span>Tabela de vendas</span>
					</a>
					
                    <div id="tabelas-modal" class="tabelas-modal white-popup mfp-with-anim mfp-hide">
                        <div class="form-interesse__flex">
                            <div class="header-tit header-tit--pages">
                                <h2 class="tit-border">Tabelas de Vendas</h2>
                                <div class="header-tit__desc">
                                    Confira abaixo as tabelas de vendas do <?php echo get_the_title(); ?>.
                                </div>
							</div>
							<ul>
							    <?php foreach ($tabelas as $arquivo) :?>
								    <li>
									    <a href="<?php echo wp_get_attachment_url($arquivo['tabela_file'][0]);?>" target="_blank" download="<?php echo $arquivo['tabela_desc'];?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><?php echo $arquivo['tabela_desc']; ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>

				<?php endif; ?>


                <?php if ($empFolder) : ?>
                    <a target="_blank" href="<?php echo wp_get_attachment_url($empFolder); ?>" class="wr-btn wr-btn--line wr-btn--download">
                        Baixar Folder
                    </a>
                <?php endif; ?>

                <?php if ($empFicha) : ?>
                    <a target="_blank" href="<?php echo wp_get_attachment_url($empFicha); ?>" class="wr-btn wr-btn--line wr-btn--download">
                        Baixar Ficha Completa
                    </a>
                <?php endif; ?>

                <?php if ($btn_coring_tit && $btn_coring_link) : ?>
                    <a target="<?php echo $btn_coring_target; ?>" href="<?php echo $btn_coring_link; ?>" class="wr-btn wr-btn--line wr-btn--download">
                        <?php echo $btn_coring_tit; ?>
                    </a>
                <?php endif; ?>

			</div>

            <?php endwhile; wp_reset_postdata();?>
        </div>
	</section>
   
    
</div>
<?php endwhile; ?>

<?php get_template_part('templates/html','footer');?>