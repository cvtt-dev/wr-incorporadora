# WR Engenharia Incorporadora #

![Alt text](https://wrengenharia.com.br/incorporadora/wp-content/themes/wr-engenharia_v2/assets/images/logo.png)

Application scope documentation.

___

## 🚀 Technologies ##

This project was developed with the following technologies:


+ HTML5 ✔
+ SCSS/CSS3 ✔
+ JavaScript/Jquery/Node  ✔
+ Npm ✔
+ Gulp ✔
+ PHP ✔
+ MYSQL ✔

# Integration
+ Anapro

___

## 💻 Desktop
[Imgur](https://imgur.com/QYczPzH)

___

## 🐱‍👤 How To Use ##

To clone and run this application, you'll need Git, Node.js v10.16 or higher + Npm v6.9 or higher installed on your computer. From your command line:

    # Clone this repository
        $ git clone https://bitbucket.org/cvtt-dev/wr-incorporadora/src/master/

    # Install dependencies
        $ npm install

    # Run the app
        $ gulp build
        $ gulp 


___

## 📝 License

This project is under the MIT license. See the [LICENSE](https://github.com/brunolimadevelopment/challenge/blob/master/front-end/SPA/MIT%20License.txt) for more information.

___

Made with ♥ by Bruno Lima - Web Application Developer Pleno at [Convertte](https://www.convertte.com.br/) | 👋 [Get in touch](https://www.linkedin.com/in/bruno-lima-b6a034177/) !