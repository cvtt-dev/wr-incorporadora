<?php /* Template Name: Área do Cliente */ ?>
<?php session_start(); ?>
<?php if(isset($_GET['sair'])) {session_destroy();wp_redirect(get_bloginfo('url'));}?>
<?php
if($_SESSION['xml']) :

    //DADOS
    $xml    = new SimpleXMLElement($_SESSION['xml']);
    $login  = $_SESSION['login'];
    $senha  = $_SESSION['senha'];
    $path   = 'http://servico.wrengenharia.com.br:85';

    //VARS
    $cliente_nome                   = $xml->cliente_nome;
    $cliente_cpf                    = $xml->cliente_cpf;
    $cliente_rg                     = $xml->cliente_rg;
    $cliente_telefone               = $xml->cliente_telefone;
    $cliente_email                  = $xml->cliente_email;
    $cliente_endereco               = $xml->cliente_endereco;
    $cliente_endereco_logradouro    = $xml->cliente_endereco_logradouro;
    $cliente_endereco_numero        = $xml->cliente_endereco_numero;
    $cliente_endereco_compl         = $xml->cliente_endereco_compl;
    $cliente_endereco_pais          = $xml->cliente_endereco_pais;
    $cliente_bairro                 = $xml->cliente_bairro;
    $cliente_cidade                 = $xml->cliente_cidade;
    $cliente_uf                     = $xml->cliente_uf;
    $cliente_cep                    = $xml->cliente_cep;
else :
    wp_redirect(get_bloginfo('url'));
endif;
?>
<?php get_template_part('templates/html','header');?>
<?php while (have_posts()) : the_post(); ?>
<?php // echo 'login '.$_SESSION['login'].' senha '.$_SESSION['senha']; ?>
<div id="editar-popup" class="modal mfp-modal mfp-modal--inscricao mfp-with-anim mfp-hide">
    <h3 class="modal__titulo">Editar Dados</h3>
    <div class="modal__small">Mantenha seus dados atualizados. Preencha os campos com seus dados para que possamos atualizar seu cadastro em nosso sistema.</div>
    <div class="modal__form">
        <?php echo do_shortcode('[contact-form-7 id="209" title="CLI Editar Dados"]'); ?>
    </div>
</div>
<div id="senha-popup" class="modal mfp-modal mfp-modal--inscricao mfp-with-anim mfp-hide">
    <h3 class="modal__titulo">Alterar Senha</h3>
    <div class="modal__small">Utilize uma senha forte mas que seja fácil para memorizar.</div>
    <div class="modal__form">
        <form id="trocar-senha" class="form form--pass" method="POST" onsubmit="return false;">
            <span class="wpcf7-form-control-wrap">
                <input type="password" id="senha" name="senha" value="" class="wpcf7-form-control wpcf7-text" placeholder="Nova Senha" required="required">
            </span>
            <button type="subtmit" class="form__input form__input--btn">Enviar <i class="fa fa-cog fa-spin login__load"></i></button>
        </form>
        <div class="pass__msg"></div>
    </div>
</div>
<div id="assistencia-popup" class="modal mfp-modal mfp-modal--inscricao mfp-with-anim mfp-hide">
    <h3 class="modal__titulo">Assistência Técnia</h3>
    <div class="modal__small">Confirme seus dados e descreva com detalhes o seu problema.</div>
    <div class="modal__form">
        <?php echo do_shortcode('[contact-form-7 id="210" title="CLI Assistência Técnica"]'); ?>
    </div>
</div>

<article class="pages pages--cliente acliente">
    <header class="header-img">
        <?php the_post_thumbnail('full', array('class' => 'header-img__img')); ?>
    </header>
    <div class="user-row">
        <div class="container">
            <div class="user-row__thumb">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </div>
            <div class="user-row__user">
                <div class="user-row__info">
                    Olá, <?php echo $cliente_nome; ?>
                    <span>CPF:<?php echo $cliente_cpf; ?></span>
                </div>
                <ul class="user-row__links">
                    <li><a href="#editar-popup" data-effect="mfp-zoom-in" class="open-modal-cliente"><i class="fa fa-cog" aria-hidden="true"></i> Editar Dados</a></li>
                    <li>
                        <a href="#senha-popup" data-effect="mfp-zoom-in" class="open-modal-pass">
                            <i class="fa fa-key" aria-hidden="true"></i> Altera Senha
                        </a>
                    </li>
                    <li><a href="<?php get_bloginfo('url'); ?>/incorporadora/area-do-cliente?sair=true"><i class="fa fa-power-off" aria-hidden="true"></i> Sair</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="hidden">
        <input type="hidden" class="cliente_nome" value="<?php echo $cliente_nome;?>" />
        <input type="hidden" class="cliente_cpf" value="<?php echo $cliente_cpf;?>" />
        <input type="hidden" class="cliente_rg" value="<?php echo $cliente_rg;?>" />
        <input type="hidden" class="cliente_telefone" value="(<?php echo substr($cliente_telefone, 4);?>" />
        <input type="hidden" class="cliente_email" value="<?php echo $cliente_email;?>" />
        <input type="hidden" class="cliente_endereco" value="<?php echo $cliente_endereco;?>" />
        <input type="hidden" class="cliente_endereco_logradouro" value="<?php echo $cliente_endereco_logradouro;?>" />
        <input type="hidden" class="cliente_endereco_numero" value="<?php echo $cliente_endereco_numero;?>" />
        <input type="hidden" class="cliente_endereco_compl" value="<?php echo $cliente_endereco_compl;?>" />
        <input type="hidden" class="cliente_endereco_pais" value="<?php echo $cliente_endereco_pais;?>" />
        <input type="hidden" class="cliente_bairro" value="<?php echo $cliente_bairro;?>" />
        <input type="hidden" class="cliente_cidade" value="<?php echo $cliente_cidade;?>" />
        <input type="hidden" class="cliente_uf" value="<?php echo $cliente_uf;?>" />
        <input type="hidden" class="cliente_cep" value="<?php echo $cliente_cep;?>" />
    </div>
    <div class="container">
		<div class="acliente__contratos">
            <h3 class="acliente__titulo">
                Contratos
            </h3>
             <div class="accordion accordion--emp">
                <div class="accordion-section">
                    <?php //Contratos
                    $loopC     = 0;
                    $contratos = $xml->listacontratos->contrato_identificador_item;
                    foreach ($contratos as $c) {

                        $codigo             = $c->empreendimento_codigo;
                        $edificacao         = $c->imovel_edificacao;
                        $contrato           = $c->numero_contrato;
                        $data_venda         = $c->contrato_datavenda;
                        $ano_venda          = date('Y', strtotime($data_venda));

                        $get_boleto         = $path.'/cgi/wminformaconcgi.exe/getcob2avia?CLILOG='.$login.'&CLISEN='.$senha.'&EMPREECOD='.$codigo.'&CONTRATO_NUMERO='.$contrato.'&MESANOCOB='.date('m/Y');
                        $xmlBoleto          = GetDadosXml($get_boleto);
                        $xmlBoletoObj       = new SimpleXMLElement($xmlBoleto);

                        $url_boleto = "";
                        foreach ($xmlBoletoObj->listacobrancas->cobranca_item as $b) {
                            $url_link     = $path.'/cgi/wminformaconcgi.exe/geraboleto?AGEFIN='.$b->agefin_id.'&COBNUM='.$b->cobranca_numerodocumento.'&DATA_REF='.$b->cobranca_vencimento.'&FMT=PDF';
                            $url_boleto   .= '<li><a target="_blank" href="'.$url_link.'">'.$b->cobranca_vencimento.': <span>R$ '.$b->cobranca_valor.'</span></a></li>';
                        }

                        $url_planilha   = $path.'/cgi/wminformaconcgi.exe/geraplanilhapdf?CLILOG='.$login.'&CLISEN='.$senha.'&EMPREECOD='.$codigo.'&CONTRATO_NUMERO='.$contrato.'&DATA_REF='.date('d/m/Y');

                        $url_imposto    = $path.'/cgi/wminformaconcgi.exe/gerairpdf?CLILOG='.$login.'&CLISEN='.$senha.'&EMPREECOD='.$codigo.'&CONTRATO_NUMERO='.$contrato.'&DATA_REF=31/12/'.date("Y",strtotime("-1 year"));

                        $url_impostos = "";
                        while ($ano_venda <= '2016') {
                            $url_ir = $path.'/cgi/wminformaconcgi.exe/gerairpdf?CLILOG='.$login.'&CLISEN='.$senha.'&EMPREECOD='.$codigo.'&CONTRATO_NUMERO='.$contrato.'&DATA_REF=31/12/'.$ano_venda;
                            $url_impostos .= '<li><a target="_blank" href="'.$url_ir.'">Ano Base '.$ano_venda.'</a></li>';
                            $ano_venda++;
                        }

                        echo '<a class="accordion-title'.($loopC == 0 ? ' active' : '').'" href="#acc_'.$loopC.'">';
                            echo '<span>'.$c->empreendimento_nome.'</span><span class="small">'.$c->imovel_unidade.'</span>';
                        echo '</a>';

                        echo '<div id="acc_'.$loopC.'" class="accordion-content">';
                            echo '<ul class="lista-servicos">
                                <li class="lista-servicos__item">
                                    <a href="#assistencia-popup" data-contrato="'.$contrato.'" data-effect="mfp-zoom-in" class="open-modal-assistencia">
                                        <i class="fa fa-cog" aria-hidden="true"></i> Assistência Técnica
                                    </a>
                                </li>
                                <li class="lista-servicos__item">
                                    <a href="'.$url_planilha.'" class="planilha" target="_blank">
                                        <i class="fa fa-bar-chart" aria-hidden="true"></i> Planilha Financeira
                                    </a>
                                </li>';
                                echo '<li class="lista-servicos__item lista-servicos__item--dropdown">
                                    <a href="">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i> Demonstrativo IR
                                    </a>
                                    <ul class="dropdown-menu">
                                        <form class="ir-form-action" action="'.get_template_directory_uri().'/_lib/_informacon/forms/imposto-renda.php" method="GET" target="_blank">
                                        <input name="url" type="hidden" value="'.$path.'/cgi/wminformaconcgi.exe/gerairpdf?CLILOG='.$login.'&CLISEN='.$senha.'&EMPREECOD='.$codigo.'&CONTRATO_NUMERO='.$contrato.'&DATA_REF=31/12/"/>
                                            <input name="ano" type="text" class="form-control ir-form" placeholder="Ano Base" required>
                                          <button type="submit" class="ir-btn">Ir</button>
                                        </form>
                                    </ul>
                                </li>';
                                echo ($url_boleto ? '
                                <li class="lista-servicos__item lista-servicos__item--dropdown">
                                    <a href="#" class="boleto">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 2ª via de boleto
                                    </a>
                                    <ul class="dropdown-menu" id="boleto">
                                        '.$url_boleto.'
                                    </ul>
                                </li>' : '<li class="lista-servicos__item lista-servicos__item--disabled"><a class="boleto" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> 2ª via de Boleto</a></li>');
                            echo '</ul>';
                            echo '<div class="accordion-bloco">';
                                echo '<div class="wrap-small">';
                                echo '<span class="small"><b>*</b> O extrato será atualizado com o prazo de 72h após o pagamento da parcela, para que seja efetuada a baixa do pagamento.</span>';

                                echo '<span class="small"><b>**</b> São disponibilizados os boletos somente dentro do mesmo mês de vencimento. As solicitações de boletos fora do mês do vencimento, deverão ser feitas ao setor de Relacionamento com o Cliente da WR Engenharia, para que sejam incluídas as devidas correções. Em caso de <b>boleto vencido</b> basta clicar no link do banco correspondente e digitar o código de barras: <a href="https://www63.bb.com.br/portalbb/boleto/boletos/hc21e,802,3322,10343.bbx" target="_blank">Banco do Brasil</a> - <a href="https://banco.bradesco/html/classic/produtos-servicos/mais-produtos-servicos/segunda-via-boleto.shtm" target="_blank">Bradesco</a> - <a href="https://www.itau.com.br/servicos/boletos/segunda-via/" target="_blank">Itaú</a> - <a href="https://bloquetoexpresso.caixa.gov.br/bloquetoexpresso/index.jsp" target="_blank">Caixa</a>
                                    </span>';
                                echo '</div>';

                                //CAMPOS HIDDEN
                                echo '<div class="hidden">
                                    <input type="hidden" class="'.$contrato.'_imovel_nome" value="'.$c->empreendimento_nome.'" />
                                    <input type="hidden" class="'.$contrato.'_imovel_codigo" value="'.$codigo.'" />
                                    <input type="hidden" class="'.$contrato.'_imovel_unidade" value="'.$c->imovel_unidade.'" />
                                    <input type="hidden" class="'.$contrato.'_imovel_endereco_emp" value="'.$c->empreendimento_endereco->logradouro.' - '.
                                    $c->empreendimento_endereco->bairro.'" />
                                    <input type="hidden" class="'.$contrato.'_imovel_contrato" value="'.$contrato.'" />
                                </div>';
                                //CAMPOS HIDDEN FIM

                                echo '<h3>Detalhes do Empreendimento</h3>
                                    <div class="infos">'.
                                        '<b>Unidade:</b> '.$c->imovel_unidade.'</br>'.
                                        '<div class="imocode"><b>Código:</b> '.$codigo.'</div></br>'.
                                        '<b>Endereço:</b></br>'.
                                        $c->empreendimento_endereco->logradouro.' - '.
                                        $c->empreendimento_endereco->bairro.'</br>'.
                                        $c->empreendimento_endereco->cidade.' - '.
                                        $c->empreendimento_endereco->uf.' - '.
                                        $c->empreendimento_endereco->cep;
                                    echo '</div>';

                                    echo '<h3>Dados do Contrato</h3>
                                    <div class="infos">'.
                                        '<b>Nº do Contrato:</b> '.$contrato.'</br>'.
                                        '<b>Data de Venda:</b> '.$c->contrato_datavenda.'</br>'.
                                        '<b>Valor da Venda:</b> '.$c->contrato_valordavenda;
                                    echo '</div>';
                            echo '</div>';
                        echo '</div>';
                        $loopC++;
                    }

                    if($loopC == 0){
                        echo '<div class="msg-post">
                        Seu contrato ainda está em processamento e em breve será disponibilizado para visualização.
                    </div>';
                    }
                    ?>
                </div>
            </div>
		</div>
		<div class="acliente__menu">
            <h3 class="acliente__titulo">
                Dados do Cliente
            </h3>
            <div class="acliente__lista">
                <ul class="userinfo">
                    <li class="userinfo__item"><i class="fa fa-user" aria-hidden="true"></i></i>Dados Pessoais</li>
                    <li class="userinfo__item userinfo__item--sub">
                    <?php echo $cliente_nome;?></br>
                    CPF: <?php echo $cliente_cpf;?></br>
                    RG: <?php echo $cliente_rg;?>
                    </li>
                    <li class="userinfo__item"><i class="fa fa-phone" aria-hidden="true"></i>Telefone</li>
                    <li class="userinfo__item userinfo__item--sub">(<?php echo substr($cliente_telefone, 4);?></li>
                    <li class="userinfo__item"><i class="fa fa-envelope" aria-hidden="true"></i>Email</li>
                    <li class="userinfo__item userinfo__item--sub"><?php echo $cliente_email;?></li>
                    <li class="userinfo__item"><i class="fa fa-map-marker" aria-hidden="true"></i>Endereço</li>
                    <li class="userinfo__item userinfo__item--sub">
                    <?php echo $cliente_endereco;?></br>
                    <?php echo $cliente_bairro;?> - CEP: <?php echo $cliente_cep;?></br>
                    <?php echo $cliente_cidade;?> - <?php echo $cliente_uf;?> - <?php echo $cliente_endereco_pais;?></br>
                    </li>
                </ul>
            </div>
		</div>
    </div>
</article>

<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>