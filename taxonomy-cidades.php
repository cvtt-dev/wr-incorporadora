<?php
	get_template_part('templates/html','header');
	$cidade 	 = get_queried_object_id();
	$catSlug = $_GET['cat'];

	$categorias = get_terms( array(
		'taxonomy' => 'categorias',
		'hide_empty' => false,
	));
?>
<article class="pages pages--empreendimento">
    <header class="header-img">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/capa-empreendimentos.jpg" alt="">
	</header>
	<div class="abas__header abas__header--branco">
		<div class="container">
			<div class="header-tit header-tit--small">
				<h2 class="abas__tit tit-border">EMPREENDIMENTOS WR: <?php single_term_title(); ?></h2>
			</div>
		</div>
		<ul class="tab tab--cinza filters">
			<li class="tab__item">
				<a href="<?php echo get_term_link($cidade, 'cidades'); ?>" data-ajax="gal" data-ancora="empreendimento-content" title="Todos" class="tab__link active">Todos</a>
			</li>
			<?php
				$i = 1;
				$my_new_array = array();

				foreach ($categorias as $term) {
				    $issue_date  = get_term_meta( $term->term_id, 'tax_ordem', true );
				    $my_new_array[$issue_date] = $term;
				}

				ksort( $my_new_array, SORT_NUMERIC );
				foreach ($my_new_array as $cat ) :
					$isActive = ( $catSlug === $cat->slug ) ? 'active' : '';
					$link = get_term_link($cat->slug, 'categorias');
			?>
			<li class="tab__item">
				<a href="<?php echo get_term_link($cidade, 'cidades'); ?>?cat=<?php echo $cat->slug;?>" class="tab__link" data-ajax="gal" data-ancora="empreendimento-content">
					<?php echo $cat->name; ?>
				</a>
			</li>
			<?php $i++; endforeach; ?>
		</ul>
	</div>
	<?php
		// var_dump($cidade);
		if($catSlug) {
			$args = array(
				'post_type'  => 'empreendimento',
				'posts_per_page' => -1,
				'tax_query' => array(
		        'relation'  => 'AND',
			        array(
			            'taxonomy'         => 'cidades',
			            'field'            => 'term_id',
			            'terms'            => $cidade,
			        ),
			        array(
			            'taxonomy'         => 'categorias',
			            'field'            => 'slug',
			            'terms'            => array($catSlug),
			            'exclude' => array(8),
			        ),
			    ),
			);
		} else {
			$args = array(
				'post_type'  => 'empreendimento',
				'posts_per_page' => -1,
				'tax_query' => array(
		        	'relation' => 'AND',
			        array(
			            'taxonomy' => 'categorias',
			            'field'    => 'term_id',
			            'terms'    => array(8),
			            'operator' => 'NOT IN',
			        ),
			    ),
			);
		}

		global $wp_query; query_posts( array_merge($wp_query->query, $args));
	?>
	<div class="container">
		<div id="ajax-content">
			<div id="empreendimento-content" class="emp-grid">
				<?php
					if (have_posts()) :
						while ( have_posts() ) : the_post();
							get_template_part('templates/empreendimento/html', 'emp-vertical');
						$i++; endwhile; wp_reset_postdata();
					else :
						echo '<div class="msg"><p>Nenhum empreendimento foi encontrado!</p></div>';
					endif;
				?>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('templates/html','footer');?>