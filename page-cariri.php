<?php /* Template Name: Cariri Store  */ ?>
<?php get_template_part('templates/html','header'); ?>
<?php while (have_posts()) : the_post(); ?>
<article class="pages">
    <header class="header-img header-img--cariri">
        <?php the_post_thumbnail('full', array('class' => 'header-img__img')); ?>
    </header>

    <section class="pages pages--empreendimento">
    	<div class="container">    
			<div class="header-tit header-tit--pages">
	            <h2 class="tit-border"><?php the_title();?></h2>
	            <div class="header-tit__desc"><?php the_content();?></div>
        	</div>	
		</div>
	
		<?php
			$args 	= array('post_type' => 'cariri','posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC');			
			$cariri = new WP_Query($args);
		?>
		<div class="container">				
				<div id="lead-modal" class="white-popup mfp-with-anim mfp-hide">
					<div class="form-interesse__flex">
						<div class="header-tit header-tit--pages">
							<h2 class="tit-border">Tenho interesse</h2>
							<div class="header-tit__desc">Preencha seus dados e receba um contato de um de nossos especialistas.</div>
						</div>

						<div class="form form--col-5 form--row">
							<?php 
								echo do_shortcode('[contact-form-7 id="1605" title="EMP - Tenho Interesse - Cariri Store"]');
							?>
						</div>
					</div>
				</div>
				<div id="empreendimento-content" class="emp-grid">
					<?php
						if ($cariri->have_posts()) :
							while ($cariri->have_posts() ) : $cariri->the_post();
								get_template_part('templates/empreendimento/html', 'cariri-store');
							endwhile; wp_reset_postdata();
						else :
							echo '<div class="msg"><p>Nenhum empreendimento foi encontrado!</p></div>';
						endif;
					?>
				</div>
			</div>
		</div>
	</section>
</article>
<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>

