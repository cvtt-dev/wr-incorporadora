<?php get_template_part('templates/html','header');?>
<?php while ( have_posts() ) : the_post();
	$idPost = get_the_id();
?>

<article class="pages pages--responsabilidade">
    <header class="header-img">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/capa-imprensa.jpg" alt="">
	</header>

	<div class="container">
		<div class="header-tit">
			<h2 class="abas__tit tit-border">Assessoria de Imprensa</h2>
		</div>

		<div class="respo-page">
			<section class="respo-page__content">
				<?php get_template_part('templates/responsabilidade/html', 'respo-single'); ?>
				<?php get_template_part('templates/loop-relacionados','imprensa');?>
			</section>

			<?php get_template_part('templates/sidebar', 'blog'); ?>
		</div>
	</div>
</article>

<?php endwhile; ?>
<?php get_template_part('templates/html','footer');?>