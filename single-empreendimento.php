<?php
get_template_part('templates/html', 'header');
the_post();

// TOPO
$imgTopo  = get_post_meta(get_the_id(), 'bg_slide', true);
$titTopo  = get_post_meta(get_the_id(), 'tit_topo', true);
$subTitTopo  = get_post_meta(get_the_id(), 'sub_topo', true);
$textBtn  = get_post_meta(get_the_id(), 'text_do_botao', true);
$linkBtn  = get_post_meta(get_the_id(), 'link_botao_topo', true);
$tipoBtn  = get_post_meta(get_the_id(), 'tipo_botao_topo', true);
$form_int = get_post_meta(get_the_id(), 'form_interesse', true);


$topo_v = get_post_meta(get_the_id(), 'topo_vagas', true);
$topo_a = get_post_meta(get_the_id(), 'topo_areas', true);
$topo_s = get_post_meta(get_the_id(), 'topo_suites', true);


$ytbTitle = get_post_meta(get_the_id(), 'youtube_tit', true);
$ytbText  = get_post_meta(get_the_id(), 'youtube_text', true);
$ytbLink  = get_post_meta(get_the_id(), 'youtube_link', true);
$ytbCapa = get_post_meta(get_the_id(), 'youtube_capa', false);



$btn_coring_target = get_post_meta(get_the_id(), 'ficha_jkbtn_target', true);
$btn_coring_link = get_post_meta(get_the_id(), 'ficha_jkbtn_link', true);
$btn_coring_tit = get_post_meta(get_the_id(), 'ficha_jkbtn_tit', true);







// INFORMAÇÕES
$contentEmp = get_post_meta(get_the_id(), 'conteudo_emp', true);

$logo = get_post_meta(get_the_ID(), 'logo_do_emp', true);
$vagas = get_post_meta(get_the_ID(), 'vagas', true);
$cidade = get_post_meta(get_the_ID(), 'cidade_emp', true);
$areas = get_post_meta(get_the_ID(), 'areas', true);
$quartos = get_post_meta(get_the_ID(), 'suites', true);

if ($areas) : $area = join(' | ', $areas['area_do_emp']);
endif;

$titlevideo = get_post_meta(get_the_id(), 'titulo_video', true);
$iframe 	= get_post_meta(get_the_id(), 'youtube_url', true);
// $tabela 	= get_post_meta(get_the_id(), 'tabela_emp', true);
$tabelas 	= get_post_meta(get_the_id(), 'tabelas_emp', true);

// $emp360 = get_post_meta(get_the_id(), '360_emp', true);
// $emp360 = get_post_meta(get_the_id(), 'galeria_360_emp', true);
$emp360Info = get_post_meta(get_the_id(), '360_info', true);
$titulo_360 = get_post_meta(get_the_id(), 'titulo_360', true);



$empTour = get_post_meta(get_the_id(), 'tour_emp', true);
$empFolder = get_post_meta(get_the_id(), 'folder_emp', true);
$empFicha = get_post_meta(get_the_id(), 'ficha_emp', true);

// var_dump($empFicha);

$logradouro = get_post_meta(get_the_id(), 'logradouro_emp', true);
$estado 	= get_post_meta(get_the_id(), 'estado_emp', true);
$cidade 	= get_post_meta(get_the_id(), 'cidade_emp', true);
$bairro 	= get_post_meta(get_the_id(), 'bairro_emp', true);

// DIFERENCIAS
$titleDiferenciais = get_post_meta(get_the_id(), 'titulo_diferenciais', true);
$grupoDiferenciais = get_post_meta(get_the_id(), 'diferenciais_grupo', false);

//IMG - 360
//$img360 = get_post_meta( get_the_id(), 'img_360', true);

// GALERIA
$galeria = get_post_meta(get_the_id(), 'galeria_empreendimento', false);
$galeriaGrid = get_post_meta(get_the_id(), 'galeria_grid', true);

// STATUS DA OBRA
$status_subtit = get_post_meta(get_the_id(), 'status_emp_subtit', true);

$statusGeral = get_post_meta(get_the_id(), 'status_geral', true);
$statusGrupo = get_post_meta(get_the_id(), 'status_grupo', true);

// ANDAMENTO DA OBRA
$andamentoObra = get_post_meta(get_the_id(), 'galeria_status', false);

// PLANTAS
$bgPlantas = get_post_meta(get_the_id(), 'bg_planta', true);
$grupoPlantas = get_post_meta(get_the_id(), 'plantas_grupo', true);
$tituloPlantas = get_post_meta(get_the_id(), 'plantas_tit', true);

// COD. DO EMPREENDIMENTO | COD. DA CAMPANHA
$cod_emp = get_post_meta(get_the_ID(), 'key_emp_produto', true);
$cod_cam = get_post_meta(get_the_ID(), 'key_emp_campanha', true);


$alphabet =   array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i');

global $post;
$id = get_the_ID();
$cat = get_the_terms($id, 'categorias');





$disabInfo = get_post_meta(get_the_id(), 'disab_info', true);
$disabVideo = get_post_meta(get_the_id(), 'disab_video', true);
$disabDif = get_post_meta(get_the_id(), 'disab_dif', true);
$disabFicha = get_post_meta(get_the_id(), 'disab_ficha', true);
$disabGaleria = get_post_meta(get_the_id(), 'disab_galeria', true);
$disab360 = get_post_meta(get_the_id(), 'disab_360', true);
$disabStatus = get_post_meta(get_the_id(), 'disab_status', true);
$disabPlanta = get_post_meta(get_the_id(), 'disab_planta', true);
$disabLocal = get_post_meta(get_the_id(), 'disab_local', true);


?>




<section class="wr-empreendimento__capa wr-empreendimento__capa-new" style="--bgCapa:url('<?php echo wp_get_attachment_image_url($imgTopo, 'full'); ?>');" id="page-single-empreendimento" data-emp="<?php global $post;
																																																		echo $post->post_name; ?>" data-cod="<?php echo $cod_emp; ?>" data-campanha="<?php echo $cod_cam; ?>">

	<div class="container">

		<figure class="logo">
			<img src="<?php echo wp_get_attachment_url($logo) ?>" alt="">
		</figure>

	</div>
	<figure class="bg-center">

		<img class="" src="<?php echo wp_get_attachment_image_url($imgTopo, 'full'); ?>" alt="">
	</figure>
</section>




<?php if ($disabInfo != 1 && $contentEmp) : ?>
	<section class="wr-empreendimento__content">
		<div class="container">
			<div class="text-side">

				<div class="titulo">
					<!-- <figure class="logo">
						<img src="<?php //echo wp_get_attachment_url($logo) 
									?>" alt="">
					</figure> -->
					<h3>
						<?php echo get_the_title(); ?>
					</h3>
				</div>

				<h3 class="subtit">
					<?php echo $titTopo ? $titTopo : get_the_title(); ?>
					<span>
						<?php echo $subTitTopo; ?>
					</span>
				</h3>
				<ul class="grid-icon">
					<li class="item">
						<i class="icone icon-pin-line">

						</i>
						<h4 class="cidade">
							<?php echo $bairro; ?>
							<span>



								<?php echo $cidade ? $cidade : $estado; ?>

							</span>
						</h4>
					</li>
					<?php if ($quartos) : ?>
						<li class="item">
							<i class="icone icon-cama-new">

							</i>
							<h3>

								Quartos
								<span>
									<?php echo $quartos; ?>
								</span>
							</h3>
						</li>
					<?php endif; ?>

					<?php if ($vagas) : ?>
						<li class="item">
							<i class="icone icon-car">

							</i>
							<h3>

								Vagas
								<span>
									<?php echo $vagas; ?>
								</span>
							</h3>
						</li>
					<?php endif; ?>

					<?php if ($area) : ?>
						<li class="item">
							<i class="icone icon-icon-dimensao">

							</i>
							<h3>

								Metragem
								<span>
									<?php echo $area; ?>
								</span>
							</h3>
						</li>
					<?php endif; ?>
				</ul>

				<div class="texto">
					<?php echo $contentEmp; ?>
				</div>


				<!-- <?php //if (!$tabelas && $tabela) : 
						?>
				<a target="_blank" href="<?php //echo wp_get_attachment_url($tabela ); 
											?>" class="wr-btn wr-btn--line wr-btn--download">
					Baixar Tabela de vendas
				</a>
			<?php// endif; ?> -->

			</div>

			<figure class="predio">
				<img src="<?php echo get_the_post_thumbnail_url($id, 'FULL'); ?>" att>
			</figure>


		</div>

		<div class="container">
			<div class="btn-wrap">
				<?php if ($tabelas) : ?>

					<?php if (count($tabelas) == 1) : ?>
						<a href="<?php echo wp_get_attachment_url($tabelas[0]['tabela_file'][0]); ?>" target="_blank" download="<?php echo $tabelas[0]['tabela_desc']; ?>" class="wr-btn wr-btn--line wr-btn--download">
						<?php else : ?>
							<a href="#tabelas-modal" data-effect="mfp-move-horizontal" class="wr-btn wr-btn--line wr-btn--download open-modal">
							<?php endif; ?>

							<span>Tabela de vendas</span>
							</a>
							<div id="tabelas-modal" class="tabelas-modal white-popup mfp-with-anim mfp-hide">
								<div class="form-interesse__flex">
									<div class="header-tit header-tit--pages">
										<h2 class="tit-border">Tabelas de Vendas</h2>
										<div class="header-tit__desc">
											Confira abaixo as tabelas de vendas do <?php echo get_the_title(); ?>.
										</div>
									</div>
									<ul>
										<?php foreach ($tabelas as $arquivo) : ?>
											<li>
												<a href="<?php echo wp_get_attachment_url($arquivo['tabela_file'][0]); ?>" target="_blank" download="<?php echo $arquivo['tabela_desc']; ?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><?php echo $arquivo['tabela_desc']; ?></a>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>

						<?php endif; ?>


						<?php if ($empFolder) : ?>
							<a target="_blank" href="<?php echo wp_get_attachment_url($empFolder); ?>" class="wr-btn wr-btn--line wr-btn--download">
								Baixar Folder
							</a>
						<?php endif; ?>

						<?php if ($empFicha) : ?>
							<a target="_blank" href="<?php echo wp_get_attachment_url($empFicha); ?>" class="wr-btn wr-btn--line wr-btn--download">
								Baixar Ficha Completa
							</a>
						<?php endif; ?>

						<?php if ($btn_coring_tit && $btn_coring_link) : ?>
							<a target="<?php echo $btn_coring_target; ?>" href="<?php echo $btn_coring_link; ?>" class="wr-btn wr-btn--line wr-btn--download">
								<?php echo $btn_coring_tit; ?>
							</a>
						<?php endif; ?>

						<?php if ($form_int != 1) : ?>
							<button class="wr-btn auto-scroll component-empreendimento-pop" scroll-to=".form.form--col-5">
								Tenho Interesse
								</buttoa>
							<?php endif; ?>
			</div>
		</div>
	</section>
<?php
endif;

if ($disabVideo != 1 && $ytbLink) :
?>

	<div id="popup-video" class="mfp-modal mfp-modal--video mfp-hide mfp-iframe-scaler"></div>
	<section class="wr-empreendimento__video">
		<div class="container">
			<div class="card">
				<header class="wr-headline wr-headline--left">
					<span class="wr-headline__line">
						assista ao vídeo
					</span>
					<h3 class="wr-headline__title">
						<?php echo $ytbTitle; ?>
					</h3>
				</header>
				<div class="text">
					<?php echo $ytbText; ?>
				</div>
			</div>



			<div class="thumb-video">
				<img src="<?php echo wp_get_attachment_image_url($ytbCapa[0], "FULL"); ?>" alt="">

				<?php

				parse_str(parse_url($ytbLink, PHP_URL_QUERY), $my_url_ytb);

				?>
				<a class="play-btn open-modal-video" data-video="<?php echo $my_url_ytb['v']; ?>" data-effect="mfp-zoom-in" href="#popup-video">
					<i class="icone icone-play">

					</i>
					Aperte aqui <br>para dar play
				</a>

			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ($disabDif != 1 && $grupoDiferenciais[1]) : ?>
	<section class="wr-empreendimento__diferenciais">
		<div class="container">

			<header class="wr-headline wr-headline--center">
				<span class="wr-headline__line">
					Diferenciais
				</span>
				<h3 class="wr-headline__title">
					<?php echo $titleDiferenciais; ?>
				</h3>
			</header>

			<ul class="grid-dif">


				<?php foreach ($grupoDiferenciais as $diferenciais) : ?>

					<li class="item">
						<figure class="thumbnail">
							<?php echo get_the_post_thumbnail($diferenciais, 'full'); ?>
						</figure>
						<p class="title">
							<?php echo get_the_title($diferenciais); ?>
						</p>
					</li>




				<?php endforeach; ?>







			</ul>
		</div>
	</section>
<?php endif; ?>

<?php if ($disabGaleria != 1 && $galeriaGrid[1]) : ?>
	<section class="wr-empreendimento__galeria">
		<div class="container">

			<header class="wr-headline wr-headline--center">
				<h3 class="wr-headline__title">
					Galeria <?php echo get_the_title(); ?>
				</h3>
			</header>
			<div class="wrap-tabs">


				<ul class="tab-selector">

					<?php $a = 0;
					foreach ($galeriaGrid as $item) :
						echo $a < 1 ? '<li seletor="tab-' . $a . '" class="wr-btn wr-btn--white-tab active">' . $item['titulo'] . '</li>' : '<li seletor="tab-' . $a . '" class="wr-btn wr-btn--white-tab">' . $item['titulo'] . '</li>';
						$a++;
					endforeach; ?>

				</ul>



				<div class="content-tabs">

					<?php $b = 0;
					foreach ($galeriaGrid as $item) : ?>

						<?php
						echo $b < 1 ? '<div id="tab-' . $b . '" class="content active">' : '<div id="tab-' . $b . '" class="content">';

						if (count($item['imgs']) <= 3) {
							$quantFotos = 3;
						} else if (count($item['imgs']) >= 4 && count($item['imgs']) <= 6) {

							$quantFotos = 6;
						} else {

							$quantFotos = 9;
						}
						?>

						<div class="grid-<?php echo $quantFotos; ?>  grid-container zoom-gallery">



							<?php $c = 0;
							foreach ($item['imgs'] as $img) : ?>


								<a href="<?php echo wp_get_attachment_image_url($img, "full"); ?>" title="<?php echo get_the_title($img); ?>" class="frame-galeria <?php echo $alphabet[$c]; ?>">
									<span class="title-image">
										<?php
										echo get_the_title($img)
										?>
										<i class="icon-arrow-up"></i>
									</span>
									<?php if ($c == 3) : ?>
										<img src="<?php echo wp_get_attachment_image_url($img, "large"); ?>" alt="">
									<?php else : ?>
										<img src="<?php echo wp_get_attachment_image_url($img, "medium"); ?>" alt="">
									<?php endif; ?>
								</a>


							<?php $c++;
							endforeach; ?>
						</div>
				</div>


			<?php $b++;
					endforeach; ?>
			</div>

		</div>
	</section>
<?php endif; ?>















<?php if ($disab360 != 1 && $emp360Info[1]) : ?>

	<section class="wr-empreendimento__360">
		<div class="container">

			<header class="wr-headline wr-headline--center">
				<h3 class="wr-headline__title">
					<?php echo $titulo_360; ?></h3>
			</header>
			<div class="wrap-tabs">

				<ul class="tab-selector">

					<?php $a = 0;
					foreach ($emp360Info as $item) :
						echo $a < 1 ? '<li seletor="tab-' . $a . '" class="wr-btn wr-btn--white-tab active">' . $item['titulo'] . '</li>' : '<li seletor="tab-' . $a . '" class="wr-btn wr-btn--white-tab">' . $item['titulo'] . '</li>';
						$a++;
					endforeach; ?>

				</ul>





				<div class="content-tabs">


					<?php $b = 0;
					foreach ($emp360Info as $item) :
						echo $b < 1 ? '<div id="tab-' . $b . '" class="content active"><iframe src="' . $item['url'] . '" style="border:0px #ffffff none;" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="600px" width="100%" allowfullscreen></iframe></div>' : '<div id="tab-' . $b . '" class="content"><iframe src="' . $item['url'] . '" style="border:0px #ffffff none;" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="600px" width="100%" allowfullscreen></iframe></div>';
						$b++;
					endforeach; ?>


				</div>

			</div>
		</div>
	</section>


<?php endif; ?>



















<?php if ($disabPlanta != 1 && $grupoPlantas[1]) : ?>
	<section class="wr-empreendimento__plantas">
		<div class="container">
			<header class="wr-headline wr-headline--center">
				<h3 class="wr-headline__title">
					<?php echo $tituloPlantas ? $tituloPlantas : 'Opções de Plantas' ?>
				</h3>
			</header>
			<ul class="slides-tabs">



				<?php $a = 1;
				foreach ($grupoPlantas as $item) {
					echo $a == 1 ? '<li class="target wr-btn wr-btn--white-tab active" data-slide="' . $a . '">' . $item['tit_planta'] . '</li>' : '<li class="target wr-btn wr-btn--white-tab" data-slide="' . $a . '">' . $item['tit_planta'] . '</li>';
					$a++;
				} ?>



			</ul>
			<div class="slider-wrap">
				<div class="slides slick-plantas">




					<?php $b = 0;
					foreach ($grupoPlantas as $item) : ?>




						<div class="slide-conteudo">
							<a class="imagem-planta image-popup-planta" href="<?php echo wp_get_attachment_image_url($item['img_planta'][0], 'FULL') ?>">
								<img src="<?php echo wp_get_attachment_image_url($item['img_planta'][0], 'FULL') ?>" alt="">
							</a>
							<div class="slide-text">
								<h4 class="titulo-planta">
									<?php echo $item['tit_planta']; ?>
								</h4>

								<p class="conteudo">
									<?php echo $item['text_planta']; ?>
								</p>

								<div class="btn-wrap"><a href="<?php echo wp_get_attachment_image_url($item['img_planta'][0], 'FULL') ?>" download="<?php echo $item['tit_planta']; ?>" class="wr-btn  wr-btn--line wr-btn--download">Baixar Planta</a>
								</div>
							</div>
						</div>


					<?php endforeach; ?>




				</div>
				<ul class="nav-slider">
					<li class="prev"></li>
					<li class="next"></li>
				</ul>
			</div>
		</div>
	</section>

<?php endif; ?>


<?php

$fichaTit = get_post_meta(get_the_id(), 'ficha_tit', true);
$fichaThumb = get_post_meta(get_the_id(), 'img_ficha', false);
$fichaGruup = get_post_meta(get_the_id(), 'ficha_grupo', true);

?>

<?php if ($disabFicha != 1 && $fichaGruup[1]) : ?>
	<section class="wr-empreendimento__ficha">
		<div class="wrap-ficha">
			<header class="wr-headline">
				<span class="wr-headline__line">
					ficha técnica
				</span>
				<h3 class="wr-headline__title">
					<?php echo $fichaTit ? $fichaTit : 'Confira os detalhes que fazem a diferença' ?>
				</h3>
			</header>



			<figure class="left-figure">
				<img src="<?php echo  wp_get_attachment_image_url($fichaThumb[0], "madiun") ?>" alt="">
			</figure>
			<div class="right-content">


				<?php foreach ($fichaGruup as $item) : ?>

					<h4 class="title">
						<?php echo $item['titulo'] ?>
						<span>
							<?php echo $item['texto'] ?>
						</span>
					</h4>
				<?php endforeach; ?>


			</div>
		</div>
	</section>
<?php endif; ?>
<?php if ($disabStatus != 1 && $statusGrupo[1]) : ?>
	<section class="wr-empreendimento__status">
		<div class="container">
			<header class="wr-headline headline">
				<span class="wr-headline__line">
					status da obra
				</span>
				<h3 class="wr-headline__title">
					Acompanhe aqui as informações referente ao status do projeto
				</h3>
				<p class="wr-headline__subtitle">

				<?= $status_subtit? $status_subtit : '';?>
				
				</p>
			</header>


			<ul class="grid-load">


				<?php $i = 1;
				foreach ($statusGrupo as $status) : ?>





					<li class="item">
						<h5 class="title">
							<?php echo $status['tit_status']; ?>
						</h5>
						<div class="barra">
							<div class="barra-bg">
								<div class="vermelho" style="width:<?php echo $status['porcentagem_status']; ?>%;""></div>
						</div>
						<div class=" porcentagem">
									<?php echo $status['porcentagem_status']; ?>%
								</div>
							</div>
					</li>

				<?php $i++;
				endforeach; ?>













			</ul>




		</div>
	</section>
<?php endif; ?>
<?php
$locRua = get_post_meta(get_the_id(), 'local_rua', true);
$locCity = get_post_meta(get_the_id(), 'local_end', true);
$locGM = get_post_meta(get_the_id(), 'local_url_gm', true);
$locWZ = get_post_meta(get_the_id(), 'local_url_waze', true);
$locProx = get_post_meta(get_the_id(), 'loc_grupo', true);
$locIframe = get_post_meta(get_the_id(), 'local_iframe', true);
?>
<?php
if ($disabLocal != 1 && $locIframe) :
?>
	<section class="wr-empreendimento__localizacao">
		<div class="container">

			<header class="wr-headline wr-headline--center">
				<span class="wr-headline__line">
					Localização
				</span>
				<h3 class="wr-headline__title">
					Perto de tudo que você precisa
				</h3>

			</header>

			<div class="header-loc">
				<h5 class="map-titulo">
					<?php echo $locRua; ?>
					<span>
						<?php echo $locCity; ?>

					</span>
				</h5>
				<div class="wrap-btn">
					<?php echo $locGM ? '<a target="_blank" href="' . $locGM . '" class="wr-btn wr-btn--black">Navegar no Maps</a>' : ''; ?>
					<?php echo $locWZ ? '<a target="_blank" href="' . $locWZ . '" class="wr-btn wr-btn--black">Abrir no Waze</a>' : ''; ?>
				</div>
			</div>
		</div>
		<?php

		if ($locProx) : ?>

			<div class="map-slider-wrap">
				<ul class="nav-slider">
					<li class="prev"></li>
					<li class="next"></li>
				</ul>
				<div class="map-slider">


					<?php foreach ($locProx as $item) : ?>



						<div class="item">

							<h6 class="title">
								<?php echo $item['titulo']; ?>
							</h6>
							<p class="content">
								<?php echo $item['texto']; ?>
							</p>
						</div>

					<?php endforeach; ?>





				</div>




			</div>

		<?php endif; ?>

		<div class="iframe-mapa container">
			<?php
			echo $locIframe;
			?>

		</div>

	</section>
<?php endif; ?>
<?php if ($form_int != 1) : ?>
	<section class="wr-empreendimento__form form-interesse" id="form-interesse">
		<div class="container">
			<div class="form-interesse__flex">
				<header class="wr-headline wr-headline--center wr-headline--white">
					<span class="wr-headline__line">
						Tem interesse? então tire suas dúvidas
					</span>
					<h3 class="wr-headline__title">
						Fale com nosso time de vendas
					</h3>

				</header>

				<div class="form form--col-5" data-emp="<?php echo get_the_title(); ?>">
					<?php echo do_shortcode('[contact-form-7 id="165" title="EMP - Tenho Interesse"]'); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php get_template_part('templates/html', 'footer'); ?>