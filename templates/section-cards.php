<section class="cards">
	<div class="container">
		<div class="noticias__header">
			<h2 class="tit-border">Saiba mais sobre a WR Engenharia</h2>

			<!-- <div class="box-setas">
				<a href="#" class="seta seta--left seta--not"></a>
				<a href="#" class="seta seta--right seta--not"></a>
			</div> -->
		</div>
		<div class="cards__flex">
			<!-- <div class="noticias">
				<div id="owl-noticias" class="owl-noticias">
					<?php
						// $args = array('post_type' => 'post');
						// $noticia = new WP_Query($args);
						// while( $noticia->have_posts() ) : $noticia->the_post();
					?>
					<div class="noticia">
						<figure class="noticia__thumb">
							<a href="<?php //echo get_the_permalink(); ?>" class="noticia__link">
								<?php //owllazy( get_the_id(), 'thumb-340x245',  get_the_title()); ?>
								<?php //the_post_thumbnail('thumb-340x245'); ?>
							</a>
						</figure>

						<div class="noticia__infos">
							<div class="noticias__descritivo">
								<h2 class="noticia__tit">
									<a href="<?php //echo get_permalink(); ?>" class="noticia__link">
										<?php //echo get_the_title(); ?>
									</a>
								</h2>
								<div class="noticia__desc">
									<p><?php //echo get_excerpt_nbtn(69); ?></p>
								</div>
							</div>

							<a href="<?php //echo get_permalink(); ?>" class="noticia__btn">Saiba mais</a>
						</div>
					</div>
					<?php// endwhile; wp_reset_postdata(); ?>

				</div>
			</div> -->
			<div class="cards__news">
			<div class="card">
					<div class="card__thumb">
						<img width="354" height="404" src="<?php echo get_template_directory_uri(); ?>/assets/images/card3.jpg" alt="">
					</div>

					<div class="card__infos">
						<h2 class="card__tit">Fale com a WR</h2>
						<a href="<?php echo get_home_url(); ?>/contato/" class="card__btn">Acessar</a>
					</div>
				</div>	
			<div class="card">
					<div class="card__thumb">
						<img width="354" height="404" src="<?php echo get_template_directory_uri(); ?>/assets/images/card1.jpg" alt="">
					</div>

					<div class="card__infos">
						<h2 class="card__tit">Confira opções de imóveis avulsos</h2>
						<a href="<?php echo get_term_link('avulsos', 'categorias' ); ?>" class="card__btn">Ver Imóveis</a>
					</div>
				</div>

				<div class="card">
					<div class="card__thumb">
						<img width="354" height="404" src="<?php echo get_template_directory_uri(); ?>/assets/images/card2.jpg" alt="">
					</div>

					<div class="card__infos">
						<h2 class="card__tit">Veja nossos vídeos no canal virtual</h2>
						<a href="http://www.youtube.com/channel/UCXVZOHJyNLibL0dDvxzHgYw" target="_blank" class="card__btn">Ver vídeos</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>