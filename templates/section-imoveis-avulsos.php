<?php

	if ( !is_singular('empreendimento') && !is_front_page() && !is_home() ) :

	$args = array(
		'post_type'  => 'empreendimento',
	    'tax_query' => array(
	        'relation'  => 'OR',
	        array(
	            'taxonomy'         => 'categorias',
	            'field'            => 'slug',
	            'terms'            => 'avulsos',
	        ),
	    ),
	);

	$loop = new WP_Query( $args );
	if ($loop->have_posts()) :
?>
<section class="avulsos">
	<div class="container">
		<div class="avulsos__flex">
			<div class="header-tit">
				<h2 class="tit-border">Imóveis Avulsos</h2>
			</div>

			<div class="emp-grid owl-avulsos">
				<?php
					while ( $loop->have_posts() ) : $loop->the_post();
				?>
					<?php get_template_part('templates/empreendimento/html', 'emp-avulsos'); ?>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>
<?php endif; endif; ?>