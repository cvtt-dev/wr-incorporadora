<?php $settings = get_option( 'options_gerais'); 
    // COD. DO EMPREENDIMENTO | COD. DA CAMPANHA
$cod_emp = get_post_meta( get_the_ID(), 'key_emp_produto', true);
$cod_cam = get_post_meta( get_the_ID(), 'key_emp_campanha', true);

global $post;

$strmidia      = 'Site Wr: '. $post->post_title;
$strmidia_home = 'Site Wr: Avulsos Juazeiro';

?>

<div class="barra-top">
    <div class="container">
        <div class="barra-top__flex">
            <div class="barra-top__item barra-top__item--tel">
                <div class="barra-top__icon">
                    <i class="icon icon-fone"></i>
                </div>

                <div class="barra-top__desc">
                    <?php
                    $i=1;
                    foreach ( $settings['group_ends'] as $end ) :
                        if ( $i > 2 ) { break; }
                    ?>
                    <address>
                        <small class="barra-top__tit">Ligue Agora <?php echo $end['end_cidade']; ?>:</small>
                        <strong class="barra-top__tit2"><?php echo $end['end_telefone']; ?></strong>
                    </address>
                    <?php $i++; endforeach; ?>
                </div>
            </div>

            <div class="barra-top__item barra-top__item--whats">
                <div class="barra-top__icon">
                    <i class="icon icon-whatsapp"></i>
                </div>

                <div class="barra-top__desc">
                    <address>
                        <small class="barra-top__tit">Whatsapp:</small>
                        <strong class="barra-top__tit2">
                            <?php
                                $whatsapp = $settings['settings_whatsapp'];
                                $search    = explode(",","-,., ");
                                $replace   = explode(",","");
                            ?>
                            <a href="https://api.whatsapp.com/send?1=pt_BR&phone=55<?php echo   str_replace($search, $replace, $whatsapp); ?>" target="_blank"><?php echo $whatsapp; ?></a>
                        </strong>
                    </address>
                </div>
            </div>
            
            <?php if(!is_front_page() && !is_archive()): ?>            
                <div class="barra-top__item barra-top__item--chat">
                    <div class="barra-top__icon">
                        <i class="icon icon-headset"></i>
                    </div>

                    <div class="barra-top__desc">
                        <address>
                            
                            <a href="https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=qo71iUga7bk1&keyIntegradora=FF083C78-C876-4D34-AE90-F5598F93BF6A&keyAgencia=fa590925-b8ad-4027-8f43-8c86a2e8e79c&strDir=wrengenharia&campanha=<?php echo $cod_cam; ?>&canal=47gNJL2lQr01&produto=<?php echo $cod_emp; ?>&strmidia=<?php echo $strmidia; ?>&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=true&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=" target="_blank" style="color:white;"><strong class="barra-top__tit2">Consultor <br> Online</strong></a>
                        </address>
                    </div>
                </div>
            <?php else: ?>
                <div class="barra-top__item barra-top__item--chat">
                    <div class="barra-top__icon">
                        <i class="icon icon-headset"></i>
                    </div>

                    <div class="barra-top__desc">
                        <address>
                            <a href="https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=qo71iUga7bk1&keyIntegradora=FF083C78-C876-4D34-AE90-F5598F93BF6A&keyAgencia=fa590925-b8ad-4027-8f43-8c86a2e8e79c&strDir=wrengenharia&campanha=Vog7zM7PAvk1&canal=47gNJL2lQr01&produto=ZXNMtGkoh9s1&strmidia=<?php echo $strmidia_home; ?>&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=true&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=" target="_blank" style="color:white;"><strong class="barra-top__tit2">Consultor <br> Online</strong></a>
                        </address>
                    </div>
                </div>
            <?php endif; ?>
          
        </div>
    </div>
</div>