<?php
get_template_part('templates/html', 'head');
$settings = get_option('options_gerais');
?>
<?php session_start(); ?>
<style>
    .painel__thumb {
        z-index: -1;
    }
    .link-constr:after {
    display: none;
}

.link-constr {
    border-left: 1px solid;
}
.box-form.box-form-mobile {
    display:none;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    margin-top: 50px;
}

.box-form.box-form-mobile a {
    width: 100%;
    margin: 10px 0;
}

.topo__toggle.on+.topo__menuD {
    flex-direction: column;
    }

    .topo__toggle.on+.topo__menuD
.box-form.box-form-mobile,
 .topo__toggle.on+.topo__menuD .topo__menu{
     position:relative;
 }
 
 .topo__menuD #area-do-cliente {
    position: fixed;
    transform: translate(-50%, -50%);
    top: 50%;
    left: 50%;
}

@media only screen and (max-width: 767px){
   .box-form.box-form-mobile {
   display:flex;
} 
}

</style>
<header class="topo">
    <div class="container">
        <div class="topo__flex">
            <h1 class="topo__logo">
                <a href="<?php echo get_site_url(); ?>" class="logo" title="Ir para página Inicial">
                    <img width="136" height="81" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Logo WR Engenharia" />
                </a>
            </h1>

            <div class="topo__infos">
                <div class="topo__action">
                    <div class="box-form">
                        <a href="<?php echo get_bloginfo('url'); ?>/area-do-cliente" class="btn-area btn-area__cliente <?php if ($_SESSION['xml']) : echo 'btn-area__cliente--logado';
                                                                                                                        endif; ?>"><i class="icon icon-pessoas"></i> Área do Cliente</a>
                                                                                <a href="https://wrengenharia.com.br/construtora/" class="btn-area link-constr">WR Construtora
</a>                                        
                        <form id="area-do-cliente" class="form form--login login" method="POST" onsubmit="return false;">
                            <div class="login__msg"></div>
                            <input id="login" type="text" name="cpf-login" class="form__input" placeholder="CPF ou Código do Cliente" />
                            <input id="senha" type="password" name="senha" class="form__input" placeholder="Senha ou CPF" />
                            <button type="subtmit" class="form__input form__input--btn">Enviar <i class="fa fa-cog fa-spin login__load"></i></button>
                        </form>
                    </div>
                    <?php /* ?>
                        <a href="#" class="btn-area btn-area__corretor"><i class="icon icon-mala"></i> Área do Corretor</a>
                    <?php */ ?>
                    <ul class="midia">
                        <?php if ($settings['settings_facebook_url']) : ?>
                        <li class="midia__item">
                            <a href="<?php echo $settings['settings_facebook_url']; ?>" target="_blank" class="midia__link">
                                <i class="icon icon-facebook"></i>
                            </a>
                        </li>
                        <?php endif; ?>

                        <?php if ($settings['settings_instagram_url']) : ?>
                        <li class="midia__item">
                            <a href="<?php echo $settings['settings_instagram_url']; ?>" target="_blank" class="midia__link">
                                <i class="icon icon-instagram"></i>
                            </a>
                        </li>
                        <?php endif; ?>

                        <?php if ($settings['settings_youtube_url']) : ?>
                        <li class="midia__item">
                            <a href="<?php echo $settings['settings_youtube_url']; ?>" target="_blank" class="midia__link">
                                <i class="icon icon-youtube"></i>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <a href="#menu" class="topo__toggle"><span></span></a>
                <div class="topo__menuD">
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'menu_1',
                            'menu_class'     => 'menu__nav',
                            'container_class' => 'topo__menu',
                        )
                    );
                    ?>
                    <?php //wp_nav_menu( array('theme_location' => 'menu_3', 'menu_class' => 'menu__nav menu__nav--mobile')); 
                    ?>
                    <?php /* ?>
                    <a href="#" class="btn-search"><i class="icon icon-search"></i></a>
                    <?php */ ?>
              <div class="box-form box-form-mobile">
                        <a href="<?php echo get_bloginfo('url'); ?>/area-do-cliente" class="btn-area btn-area__cliente <?php if ($_SESSION['xml']) : echo 'btn-area__cliente--logado';
                                                                                                                        endif; ?>"><i class="icon icon-pessoas"></i> Área do Cliente</a>
                                                                                <a href="https://wrengenharia.com.br/construtora/" class="btn-area link-constr">WR Construtora
</a>                                        
                        <form id="area-do-cliente" class="form form--login login" method="POST" onsubmit="return false;">
                            <div class="login__msg"></div>
                            <input id="login" type="text" name="cpf-login" class="form__input" placeholder="CPF ou Código do Cliente" />
                            <input id="senha" type="password" name="senha" class="form__input" placeholder="Senha ou CPF" />
                            <button type="subtmit" class="form__input form__input--btn">Enviar <i class="fa fa-cog fa-spin login__load"></i></button>
                        </form>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</header>
<main class="main">