<section class="cta-unidade">	
	<div class="container">		
		<div class="cta-unidade__flex">
			<i class="icon icon-mapa-ponto"></i>
			<h2 class="cta-unidade__tit">Encontre o Emilio Ribas mais próximo de você</h2>
			<div class="cta-unidade__desc">
				<p>Agilize seu atendimento em nossas unidades: pré-agende aqui os exames de Análises Clínicas, Imagem e Provas Funcionais. Agende também sua Coleta Domiciliar</p>
			</div>

			<a href="<?php echo get_post_type_archive_link('unidades'); ?>" class="cta-unidade__btn">Ver unidades</a>
		</div>
	</div>
</section>