<div class="relacionados">
    <div class="header-tit">
        <h2 class="abas__tit tit-border">Artigos Relacionados</h2>
    </div>

    <div class="respo-page">
        <section class="respo-page__content">
            <?php
            $exclude_id = get_the_ID();
            $categoria = get_the_category($post->id);
            $idCategoria = $categoria[0]->term_id;

            $loop = new WP_Query( array( 'post_type' => 'post', 'cat' => $idCategoria, 'showposts' => 3, 'post__not_in' => array($exclude_id), 'orderby' => 'rand') );
            while ( $loop->have_posts()) : $loop->the_post();
                get_template_part('templates/responsabilidade/html', 'respo-card');
            endwhile;
            ?>
        </section>
    </div>
</div>