
<div class="infos-contato">
    <address class="infos-contato__tel">
        <i class="icon icon-celular"></i>
        <strong>85 3457.2000</strong>
    </address>

    <div class="infos-contato__horario">
        <i class="icon icon-relogio"></i>

        <select name="#" class="select">
            <option value="">Horário de funcionamento</option>
        </select>               
    </div>

    <div class="infos-contato__como-chegar">
        <i class="icon icon-mapa-ponto"></i>

        <select name="#" class="select">
            <option value="">Como chegar</option>
        </select>
    </div>
</div>