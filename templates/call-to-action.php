<?php 
	$args = array(
    	'posts_per_page'=> 1, 
    	'post_type' => 'page',
    	'p' => 55
    );
    $cta = new WP_Query($args);

    while ($cta->have_posts()) : $cta->the_post();

    $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
<section class="cta" style="background-image: url(<?php echo $url; ?>)">
	<div class="cta__infos">
		<h3 class="cta__tit"><?php echo get_the_title(); ?></h3>
		<div class="cta__desc">
			<?php echo get_the_content(); ?>
		</div>

		<a href="<?php echo get_permalink(); ?>" class="cta__btn">Saiba mais</a>
	</div>
</section>
<?php endwhile; wp_reset_postdata(); ?>	