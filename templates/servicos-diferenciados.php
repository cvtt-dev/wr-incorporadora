<?php //slides frontpage
    $args = array(
    	'posts_per_page'=> 12, 
    	'post_type' => 'servicos',
    	'meta_query' => array(
			array(
				'key'     => 'is_diferenciado',
				'value'   => '1',
				'compare' => 'IN',
			),
		),
    );
    $servicos = new WP_Query($args); 
?>

<?php if ($servicos->have_posts()) : ?>
<section class="servicos-dif">	
	<div class="container">

		<header class="servicos-dif__header">
			<h2 class="tit-border">Serviços diferenciados</h2>

			<div class="box-setas">
				<a href="#" class="seta seta--left seta--dif"></a>
				<a href="#" class="seta seta--right seta--dif"></a>
			</div>
		</header>
		
		<div id="owl-serv-dif" class="owl-serv-dif">
			<?php 
				while ($servicos->have_posts()) : $servicos->the_post();
				$serv_link        = get_post_meta( get_the_id(), 'opt_link', true);
	            $serv_target      = get_post_meta( get_the_id(), 'opt_target', true);
	            $serv_btn         = get_post_meta( get_the_id(), 'opt_text', true);
			?>
			<div class="servico">
				<figure class="servico__thumb">					
					<?php echo thumblazy(get_the_id(), 'thumb-265x265', 'fade', get_the_title() ); ?>
				</figure>

				<div class="servico__infos">					
					<div class="servicos__header">
						<h3 class="servico__tit"><?php echo get_the_title(); ?></h3>
						<div class="servico__desc">
							<?php echo get_excerpt_nbtn(174); ?>
						</div>
					</div>
					
					<?php if ($serv_link) : ?>
						<a class="servico__btn" href="<?php echo $serv_link; ?>" target="<?php echo $serv_target; ?>"><?php echo $serv_btn; ?></a>
					<?php endif; ?>
				</div>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>		
		</div>
	</div>
</section>
<?php endif; ?>