<?php
$args = array('post_type' => 'page', 'p' => 1641);
$page = new WP_Query( $args );
while( $page->have_posts() ) : $page->the_post();
?>
<section class="cta">
	<div class="container">
		<div class="cta__flex">
			<div class="cta__infos">
				<h2 class="cta__tit"><?php echo get_the_title(); ?></h2>
				<div class="cta__desc">
					<?php the_content(); ?>
				</div>
				<?php /* ?>
				<a href="<?php echo get_permalink(get_page_by_path('cariri-store')); ?>" class="cta__btn">Saiba mais</a>
				<?php */?>
				<a href="<?php echo get_permalink(get_page_by_path('institucional')); ?>" class="cta__btn">Saiba mais</a>
			</div>
		</div>
	</div>

	<figure class="cta__thumbs">
		<?php the_post_thumbnail('full'); ?>
	</figure>
</section>
<?php endwhile; wp_reset_postdata(); ?>