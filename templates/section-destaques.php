<?php
	$i = 1;
	$args = array(
		'posts_per_page'=> 3,
		'post_type' => 'empreendimento',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'destaque_emp',
				'value' => '1',
				'compare' => '='
			),
		),
	);

	$imovel = new WP_Query($args);
	if ($imovel->have_posts()) :
?>

<section class="destaques">
	<div class="container">
		<header class="tit-header">
			<h2 class="tit-border">Destaques WR Engenharia</h2>
			<a href="<?php echo get_post_type_archive_link('empreendimento'); ?>" class="btn-ver-outras">Ver outras</a>
		</header>

		<?php
			$i=1;
			while ( $imovel->have_posts()) : $imovel->the_post();
			$cat = get_the_terms( get_the_ID(), 'categorias' );
			$vagas = get_post_meta(get_the_ID(), 'vagas', true);
			$cidade = get_the_terms( get_the_ID(), 'cidades' );
			//$cidade = get_post_meta(get_the_ID(), 'cidade_emp', true);
			$areas = get_post_meta(get_the_ID(), 'areas', true);
			$status = get_post_meta(get_the_ID(), 'status_geral', true);
			$quartos = get_post_meta(get_the_ID(), 'suites', true);
			$cor_emp = get_post_meta(get_the_ID(), 'cor_emp', true);
			$galerias = get_post_meta(get_the_ID(), 'galeria_empreendimento', false);
			$area = join(' | ', $areas['area_do_emp']);

			// var_dump($imovel->post_count);
		?>
		<div class="emp <?php echo ( $i % 2 == 0) ? 'emp--right': ''; ?>" style="--color:<?php echo $cor_emp; ?> ">

			<div class="emp__infos">
				<strong class="emp__cat"><?php echo $cat[0]->name; ?></strong>

				<h2 class="emp__tit">
					<a href="<?php echo get_the_permalink(); ?>" class="emp__tit__link"><?php echo get_the_title(); ?></a>
				</h2>

				<?php if ( $vagas || $area || $quartos ) : ?>
				<ul class="icones">
					<?php if ( $cidade ) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-pin"></i></div>
						<span class="icones__tit"><?php echo $cidade[0]->name; ?></span>
					</li>
					<?php endif; ?>

					<?php if ( $vagas ) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-carro"></i></div>
						<span class="icones__tit"><?php echo $vagas; ?></span>
					</li>
					<?php endif; ?>

					<?php if ( $area ) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-regua"></i></div>
						<span class="icones__tit"><?php echo $area; ?></span>
					</li>
					<?php endif; ?>

					<?php if ( $quartos ) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-cama"></i></div>
						<span class="icones__tit"><?php echo $quartos; ?></span>
					</li>
					<?php endif; ?>
				</ul>
				<?php endif; ?>
			</div>
            <div class="emp__galeria">


<?php //if ($status == 0){}else if($status > 0 && $status < 100){
//echo '<span class="status-slide">status: '.$status.'%</span>';
//}else {
  //   echo '<span class="status-slide">status: Obra Concluida</span>';
//}?>
		
		



		<a href="<?php echo get_the_permalink(); ?>">
	
		<img class="img-full" src="<?php echo wp_get_attachment_image_url( $galerias[0], 'thumb-800x325', ''); ?>" alt="">
		</a>

				<!-- <div class="emp__owl owl-galeria">
					<?php //$counter = 0; foreach ( $galerias as $gal ) : ?>
					<a href="<?php //echo get_the_permalink(); ?>"><?php //owllazy( $gal, 'thumb-800x325', ''); ?></a>
					<?php //if ($counter >= 5) 
    						//	break;
    						//$counter++; ?>
					<?php //endforeach; ?>
				</div> -->




			</div>
		</div>
		<?php $i++; endwhile; wp_reset_postdata(); ?>

		<!-- <?php// if ($imovel->post_count > 2) : ?>
		<footer class="btn-group btn-group--center">
			<a href="<?php //echo get_post_type_archive_link('empreendimento'); ?>" class="btn btn--pri">Ver todos</a>
		</footer>
		<?php //endif; ?> -->
	</div>
</section>
<?php endif; ?>



<style>
.emp__galeria{
    position: relative;
}
span.status-slide {
    position: absolute;
    top: 10px;
    left: 20px;
    z-index: 90;
    color: #fff;
    background: #b93027;
    padding: 5px 20px;
    border-radius: 50px;
}

</style>