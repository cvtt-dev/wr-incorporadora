<?php
$cat = get_the_terms( get_the_ID(), 'categorias' );
$vagas = get_post_meta(get_the_ID(), 'vagas', true);
// $cidade = get_post_meta(get_the_ID(), 'cidade_emp', true);
$cidade = get_the_terms( get_the_ID(), 'cidades' );
$areas = get_post_meta(get_the_ID(), 'areas', true);
$quartos = get_post_meta(get_the_ID(), 'suites', true);

if ( $areas ) : $area = join(' | ', $areas['area_do_emp']); endif;

$logo = get_post_meta(get_the_ID(), 'logo_do_emp', true);
$logoImg = wp_get_attachment_image($logo, 'thumbnail');

$galerias = get_post_meta(get_the_ID(), 'galeria_empreendimento', false);
$img = wp_get_attachment_image($galerias[0], 'thumb-315x400');

$catSlugs = array();

foreach ($cat as $c ) {
	$catSlugs[] = $c->slug;
}

$filterSlugs = join(' ', $catSlugs);

?>
<div class="emp-vert" data-filter="<?php echo $filterSlugs; ?>">
	<!-- <?php if ($logoImg) : ?>
	<a href="<?php echo get_the_permalink(); ?>" class="emp-vert__logo"><?php echo $logoImg; ?></a>
	<?php endif; ?> -->

	<a href="<?php echo get_the_permalink(); ?>" class="emp-vert__thumb">
		<?php if(!has_post_thumbnail() && !$img){echo '<img src="'.get_template_directory_uri(). '/assets/images/sem-midia-emp-vert.jpg" />';} elseif(!has_post_thumbnail()){ echo $img;} else {the_post_thumbnail('full');} ?>
		<span class="emp-vert__thumb__btn">Saiba Mais</span>
	</a>

	<div class="emp-vert__infos">
		<h2 class="emp-vert__tit">
			<a href="<?php echo get_the_permalink(); ?>" class="emp-vert__tit__link"><?php echo get_the_title(); ?></a>
		</h2>

		<?php if ( $cidade || $vagas || $area || $quartos ) : ?>
		<ul class="icones">
			<?php if ( $cidade ) : ?>
			<li class="icones__item">
				<div class="icones__icon"><i class="icon icon-pin"></i></div>
				<span class="icones__tit"><?php echo $cidade[0]->name; ?></span>
			</li>
			<?php endif; ?>

			<?php if ( $vagas ) : ?>
			<li class="icones__item">
				<div class="icones__icon"><i class="icon icon-carro"></i></div>
				<span class="icones__tit"><?php echo $vagas; ?></span>
			</li>
			<?php endif; ?>

			<?php if ( $area ) : ?>
			<li class="icones__item">
				<div class="icones__icon"><i class="icon icon-regua"></i></div>
				<span class="icones__tit"><?php echo $area; ?></span>
			</li>
			<?php endif; ?>

			<?php if ( $quartos ) : ?>
			<li class="icones__item">
				<div class="icones__icon"><i class="icon icon-cama"></i></div>
				<span class="icones__tit"><?php echo $quartos; ?></span>
			</li>
			<?php endif; ?>
		</ul>
		<?php endif; ?>
	</div>

	<a href="<?php echo get_the_permalink(); ?>" class="emp-vert__btn">Saiba mais</a>
</div>