<section class="wr-empreendimento__capa" style="--bgCapa:url('<?php echo wp_get_attachment_image_url($imgTopo, 'full'); ?>');" id="page-single-empreendimento" data-emp="<?php global $post;
																																											echo $post->post_name; ?>" data-cod="<?php echo $cod_emp; ?>" data-campanha="<?php echo $cod_cam; ?>">
	<div class="container">
		<div class="initial-info">
			<p class="cidade">
				<?php echo $bairro;
				echo ($cidade) ? ' - ' . $cidade : ' - ' . $estado; ?>
			</p>
			<h3 class="tit">
				<?php echo $titTopo ? $titTopo : get_the_title(); ?>
				<span>
					<?php echo $subTitTopo; ?>
				</span>
			</h3>
			<ul class="icones">
				<?php if ($topo_s) : ?>
					<li>
						<i class="icon-cama-new">

						</i> <?php echo $topo_s; ?>
					</li>
				<?php endif; ?>
				<?php if ($topo_a) : ?>
					<li>
						<i class="icon-icon-dimensao">

						</i> <?php echo $topo_a; ?>
					</li>
				<?php endif; ?>
				<?php if ($topo_v) : ?>
					<li>
						<i class="icon-car">

						</i> <?php echo $topo_v; ?>
					</li>
				<?php endif; ?>
			</ul>

		</div>
	</div>
</section>