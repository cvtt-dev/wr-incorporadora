<?php $preco  = get_post_meta(get_the_id(), 'cariri_preco', true); ?>
<?php $code   = get_post_meta(get_the_id(), 'cod_crm', true ); ?>
<?php $filial = get_post_meta(get_the_id(), 'filial_crm', true ); ?>
<?php

	$bairro    = get_post_meta(get_the_id(), 'bairro', true);
	$vagas     = get_post_meta(get_the_id(), 'vagas_cariri', true);
	$areas = get_post_meta(get_the_ID(), 'areas_cariri', true);
	// $area = $areas['area_do_emp_cariri'][0];

	if ( $areas ) : $area = join(' | ', $areas['area_do_emp_cariri']); endif;

	$areaCount = count($areas['area_do_emp_cariri']);
	
	 ?>
<div class="emp-vert emp-vert--cariri">	
	<div class="emp-vert__thumb">
		<?php if(!has_post_thumbnail() && !$img){echo '<img width="315" height="400" src="'.get_template_directory_uri(). '/assets/images/sem-midia-emp-vert.jpg" />';} elseif(!has_post_thumbnail()){ echo $img;} else {the_post_thumbnail('full');} ?>
	</div>
	<div class="emp-vert__infos">
		<h2 class="emp-vert__tit"><?php the_title(); ?></h2>
	</div>

	<div class="emp-gal__desc">
				<?php if ($bairro || $vagas || $area || $quartos) : ?>
				<ul class="icones">
					<?php if ($bairro) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-pin"></i></div>
						<span class="icones__tit"><?php echo $bairro; ?></span>
					</li>
					<?php endif; ?>

					<?php if ($vagas) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-carro"></i></div>
						<span class="icones__tit"><?php echo $vagas; ?></span>
					</li>
					<?php endif; ?>

					<?php if ($area) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-regua"></i></div>
						<span class="icones__tit"><?php echo $area; ?></span>
					</li>
					<?php endif; ?>
				</ul>
				<?php endif; ?>
			</div>

	<a href="#lead-modal" data-title="<?php echo get_the_title(); ?>" data-code="<?php echo $code; ?>" data-filial="<?php echo $filial; ?>" data-effect="mfp-move-horizontal" class="emp-vert__btn open-modal-cariri">Saiba mais</a>
</div>