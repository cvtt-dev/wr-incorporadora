<?php
$cor = get_post_meta(get_the_ID(), 'cor_emp', true);
$vagas = get_post_meta(get_the_ID(), 'vagas', true);
// $cidade = get_post_meta(get_the_ID(), 'cidade_emp', true);
$cidade = get_the_terms( get_the_ID(), 'cidades' );
$quartos = get_post_meta(get_the_ID(), 'suites', true);
$galerias = get_post_meta(get_the_ID(), 'galeria_empreendimento', false);

$logo = get_post_meta(get_the_ID(), 'logo_do_emp', true);
$logoImg = wp_get_attachment_image($logo, 'thumb-125x125');

$areas = get_post_meta(get_the_ID(), 'areas', true);
$area = $areas['area_do_emp'][0];
$areaCount = count($areas['area_do_emp']);
?>
<div class="emp-gal animated fadeIn">
	<div class="emp-gal__headline">
		<?php if ($logoImg) : ?>
		<figure class="emp-gal__logo"><?php echo $logoImg; ?></figure>
		<?php endif; ?>

		<div class="emp-gal__infos">
			<h3 class="emp-gal__tit">
				<a href="<?php echo get_the_permalink(); ?>" class="emp-gal__tit__link"><?php echo get_the_title(); ?></a>
			</h3>

			<div class="emp-gal__desc">
				<?php if ($cidade || $vagas || $area || $quartos) : ?>
				<ul class="icones">
					<?php if ($cidade) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-pin"></i></div>
						<span class="icones__tit"><?php echo $cidade[0]->name; ?></span>
					</li>
					<?php endif; ?>

					<?php if ($vagas) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-carro"></i></div>
						<span class="icones__tit"><?php echo $vagas; ?></span>
					</li>
					<?php endif; ?>

					<?php if ($area) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-regua"></i></div>
						<span class="icones__tit"><?php echo $area; if ( $areaCount > 1 ) { echo '...'; }; ?></span>
					</li>
					<?php endif; ?>

					<?php if ($quartos) : ?>
					<li class="icones__item">
						<div class="icones__icon"><i class="icon icon-cama"></i></div>
						<span class="icones__tit"><?php echo $quartos; ?></span>
					</li>
					<?php endif; ?>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="emp-gal__images">
		<?php
		$x = 0;
		$countGal = count($galerias);
		foreach($galerias as $gal) :
			($x == 0) ?
			$imgGrande = wp_get_attachment_image($gal, 'thumb-565x310') :
			$imgThumb = wp_get_attachment_image($gal, 'thumb-140x110');
		?>
			<?php if ($x == 0) : ?>
			<div class="emp-gal__images__big">
				<?php echo $imgGrande; ?>
			</div>
			<?php else : ?>

				<?php if ($x == 1) : ?>
				<div class="emp-gal__images__thumbs">
				<?php endif; ?>

					<?php if ($x <= 3) : ?>
					<div class="emp-gal__images__thumbs__col">
						<?php echo $imgThumb; ?>
					</div>
					<?php endif; ?>

			<?php endif; ?>

		<?php $x++; endforeach; ?>

			<div class="emp-gal__images__thumbs__col">
				<a href="<?php echo get_the_permalink(); ?>" class="emp-gal__images__btn" style="background-color: <?php echo $cor; ?>;">Saiba mais</a>
			</div>

			<?php if ($x == $countGal) : ?>
			</div>
			<?php endif; ?>
	</div>

</div>