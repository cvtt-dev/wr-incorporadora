<section class="cta-corretor">
	<div class="container">
		<div class="cta-corretor__flex">

			<div class="cta-corretor__tit-box">
				<i class="icon icon-executivo"></i>
				<h2 class="cta-corretor__tit">Área exclusiva do corretor</h2>
			</div>			

			<div class="cta-corretor__desc">
				<p>Empresa do ramo da construção e incorporação de empreendimentos diversos, a WR Engenharia firma-se no.</p>
			</div>

			<a href="#" class="cta-corretor__btn">Entrar</a>
		</div>
	</div>
</section>