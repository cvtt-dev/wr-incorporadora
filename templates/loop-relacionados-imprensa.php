<?php
$exclude_id = get_the_ID();
$loop = new WP_Query( array( 'post_type' => 'imprensa', 'showposts' => 3, 'post__not_in' => array($exclude_id), 'orderby' => 'rand') );
if($loop->have_posts()): ?>
<div class="relacionados">
    <div class="header-tit">
        <h2 class="abas__tit tit-border">Outros Informativos</h2>
    </div>

    <div class="respo-page">
        <section class="respo-page__content">
            <?php //loop
            while ( $loop->have_posts()) : $loop->the_post();
                get_template_part('templates/responsabilidade/html', 'respo-card');
            endwhile; wp_reset_query(); ?>
        </section>
    </div>
</div>
<?php endif; ?>