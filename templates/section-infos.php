<section class="infos">
	<div class="container">
			
		<div class="infos-geral">			
			<div class="infos-geral__item">
				<div class="infos-geral__flex">				
					<div class="infos-geral__icon">
						<i class="icon icon-pessoas"></i>
					</div>

					<div class="infos-geral__content">					
						<strong class="infos-geral__tag">Para você médico</strong>
						<h2 class="infos-geral__tit">Canal do Médico</h2>
						<div class="infos-geral__desc">
							<p>Manual de exames, resultados, cursos, novidades e um conteúdo voltado todo para os médicos parceiros do laboratório.</p>
						</div>

						<a href="#" class="infos-geral__btn">Acesse</a>
					</div>
				</div>
				
			</div>

			<div class="infos-geral__item">
				<div class="infos-geral__flex">				
					<div class="infos-geral__icon">
						<i class="icon icon-telefone"></i>
					</div>

					<div class="infos-geral__content">					
						<strong class="infos-geral__tag">Para você cliente</strong>
						<h2 class="infos-geral__tit">Canal do atendimento 85 3457.2000</h2>
						<div class="infos-geral__desc">
							<p>85 98200.4751 | 85 98130.4596 | 85 98130.4596</p>
						</div>

						<a href="#" class="infos-geral__btn">Saiba mais</a>
					</div>
				</div>
			</div>

			<div class="infos-geral__item infos-geral__item--resultado">
				<div class="infos-geral__flex">
					<div class="infos-geral__content">					
						<strong class="infos-geral__tag">Praticidade para você</strong>
						<h2 class="infos-geral__tit">Resultado do seu exame</h2>
						
						<form action="#" id="form-resultado" class="form form--result">
							<input type="text" class="form__input"  type="text" name="login" placeholder="Login" />
							<input type="password" class="form__input"  name="senha" placeholder="Senha" />
							
							<div class="form__action">
								<button  type="submit" class="form__btn">Acesse</button>
								<a href="#" class="form__btn form__btn--line">Esqueci minha senha</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>