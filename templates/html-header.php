<?php
get_template_part('templates/html', 'head');
$settings = get_option('options_gerais');
?>
<?php session_start(); ?>

<header class="wr-header">
    <div class=" wr-header__top">
      <div class="wrap">

   
       <a href="<?php echo get_bloginfo('url'); ?>/area-do-cliente" class="cliente btn-area__cliente <?php if ($_SESSION['xml']) : echo 'btn-area__cliente--logado'; endif; ?>">
           Área do Cliente</a>
        <form id="area-do-cliente" class="form form--login login" method="POST" onsubmit="return false;">
            <div class="login__msg"></div>
            <input id="login" type="text" name="cpf-login" class="form__input" placeholder="CPF ou Código do Cliente" />
            <input id="senha" type="password" name="senha" class="form__input" placeholder="Senha ou CPF" />
            <button type="subtmit" class="form__input form__input--btn">Enviar</button>
        </form>


        <ul class="midia midia--cinza">
					<?php if ($settings['settings_facebook_url']) : ?>
						<li class="midia__item">
							<a href="<?php echo $settings['settings_facebook_url']; ?>" target="_blank" class="midia__link">
								<i class="icon icon-facebook"></i>
							</a>
						</li>
					<?php endif; ?>

					<?php if ($settings['settings_instagram_url']) : ?>
						<li class="midia__item">
							<a href="<?php echo $settings['settings_instagram_url']; ?>" target="_blank" class="midia__link">
								<i class="icon icon-instagram"></i>
							</a>
						</li>
					<?php endif; ?>

					<?php if ($settings['settings_youtube_url']) : ?>
						<li class="midia__item">
							<a href="<?php echo $settings['settings_youtube_url']; ?>" target="_blank" class="midia__link">
								<i class="icon icon-youtube"></i>
							</a>
						</li>
					<?php endif; ?>
				</ul>

      </div>
    </div>
    <div class="container wr-header__container wr-header__container--bottom">

        <h1 class="wr-header__logo">
            <a href="<?php echo get_site_url(); ?>" class="logo" title="Ir para página Inicial">
                <img width="136" height="81" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Logo WR Engenharia" />
            </a>
        </h1>





        <div class="wr-menu">
            <?php wp_nav_menu(
                array(
                    'theme_location' => 'menu_1',
                    'menu_class'     => 'menu__nav',
                    'container_class' => 'topo__menu',
                )
            );
            ?>

        </div>


        <a href="https://wrengenharia.com.br/construtora/" class="wr-btn wr-btn--top dn-mobile">WR Construtora
        </a>


    </div>

    <a href="#menu" class="topo__toggle"><span></span></a>
    <div class="topo__menuD">
        <?php wp_nav_menu(
            array(
                'theme_location' => 'menu_1',
                'menu_class'     => 'menu__nav',
                'container_class' => 'topo__menu',
            )
        );
        ?>

        <div style="margin-top: 30px;">

            <a href="<?php echo get_bloginfo('url');
                        ?>/area-do-cliente" class="btn-area btn-area__cliente <?php if ($_SESSION['xml']) : echo 'btn-area__cliente--logado';
                                                                                endif;
                                                                                ?>"><i class="icon icon-pessoas"></i> Área do Cliente</a>

            <form id="area-do-cliente2" class="form form--login login" method="POST" onsubmit="return false;">
                <div class="login__msg"></div>
                <input id="login2" type="text" name="cpf-login" class="form__input" placeholder="CPF ou Código do Cliente" />
                <input id="senha2" type="password" name="senha" class="form__input" placeholder="Senha ou CPF" />
                <button type="subtmit" class="form__input form__input--btn">Enviar</button>
            </form>

            <a href="https:wrengenharia.com.br/construtora/" class="wr-btn wr-btn--top">WR Construtora
            </a>
        </div>

    </div>
</header>
<div class="footer-target"></div>
<main class="main">