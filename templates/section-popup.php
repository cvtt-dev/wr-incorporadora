<a href="#popup-cariri" data-effect="mfp-zoom-in" class="popup-link" style="display: none;">Abrir Popup</a>
<div id="popup-cariri" class="mfp-modal mfp-modal--table mfp-modal--cariri mfp-with-anim mfp-hide" style="background: transparent;">    
    <a href="<?php echo get_permalink(get_page_by_path('cariri-store')); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/popup-cariri.png">    
    </a>
</div>