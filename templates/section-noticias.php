<?php 
$args = array(
	'post_type' => 'noticias',
	'showposts'	=> 9,
); 

$noticias = new WP_Query( $args ); 

if ( $noticias->have_posts() ) :
?>

<section class="destaques">
	<div class="container">			
		<h2 class="tit-border">Destaques Emilio Ribas</h2>
		
		<div class="destaques__flex">
			<div class="destaques__desc">
				<p>Pensando na comodidade e no conforto de seus clientes, o Emilio Ribas oferece o Atendtimento Domiciliar para que você possa cuidar da sua saúde no conforto de sua residência.</p>
			</div>

			<div class="box-setas">
				<a href="#" class="seta seta--left seta--dest"></a>
				<a href="#" class="seta seta--right seta--dest"></a>
			</div>
		</div>

		<!-- Noticias -->
		<div id="owl-noticias" class="noticias">
			<?php while( $noticias->have_posts() ) : $noticias->the_post(); ?>
			<div class="noticia">
				<figure class="noticia__thumb">
					<a href="<?php echo get_permalink(); ?>" class="noticia__link">						
						<?php echo thumblazy( get_the_id(), 'thumb-365x190', 'fade', get_the_title()); ?>
					</a>
				</figure>

				<div class="noticia__infos">
					<h2 class="noticia__tit">
						<a href="<?php echo get_permalink(); ?>" class="noticia__link">
							<?php echo get_the_title(); ?>
						</a>						
					</h2>

					<div class="noticia__desc">
						<?php echo get_excerpt_nbtn(90);?>
					</div>

					<a href="<?php echo get_permalink(); ?>" class="noticia__btn">Saiba mais</a>
				</div>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</div>
</section>
<?php endif; ?>