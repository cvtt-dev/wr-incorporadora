<!-- <?php //$scriptFooter = get_post_meta(get_the_ID(), 'script_footer', true); 
		?>
<div class="barra-mobile">
	<a href="#phone-modal" data-effect="mfp-move-horizontal" class="open-modal"> <i class="icon icon-fone"></i></a>
	<a href="https://api.whatsapp.com/send?1=pt_BR&amp;phone=5588992152976" target="_blank"> <i class="icon icon-whatsapp"></i></a>
	<a href="#"><i class="icon icon-headset"></i></a>
</div> -->

<?php get_template_part('templates/html', 'float-chat'); ?>
<?php // get_template_part('templates/html', 'barra-top'); 
?>
<?php //get_template_part('templates/section', 'newsletter'); 
?>
<?php //get_template_part('templates/form', 'mobile'); 
?>

</main>

<?php $settings = get_option('options_gerais'); ?>


<?php
//$$scriptFooter = get_post_meta(get_the_ID(), 'script_footer', true);
//$scriptFooter_rdrct = get_post_meta($_GET["pageid"], 'script_footer_redirect', true);

?>

<style>
	.contatos-modal-wrap {
		width: 100%;
		display: grid;
		grid-template-areas:
			". .";
		grid-template-columns: 1fr 1fr;
		grid-column-gap: 11px;
		margin: 40px 0;
	}

	.contatos-modal-wrap a {
		background: red;
		padding: 30px;
		display: flex;
		align-items: center;
		justify-content: center;
		color: #fff;
	}

	.barra-mobile {
		/* background-color: $vermelho; */
		display: none;
		justify-content: space-around;
		overflow: hidden;
		position: -webkit-sticky;
		position: sticky;
		bottom: 0;
	}
	.emp-vert__thumb {
    max-height: 380px;
    overflow: hidden;
}
	.barra-mobile a {
		color: #fff;
		padding: 20px 30px;
		background-color: #d01430;
		cursor: pointer;
		width: 33.333%;
		display: flex;
		align-items: center;
		justify-content: center;
		margin: 0 1px;
	}

	.barra-mobile.animated.is-fixed.fadeInUp {
		position: fixed;
		bottom: 0;
		width: 100%;
		z-index: 98;
	}


	.emp__galeria {

		height: 12.8125rem;
	}

	.emp__galeria .img-full {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.class-capa-mobile {
		display: none;
	}

	.class-capa-desktop {
		display: block;
	}


	.class-capa-mobile,
	.class-capa-desktop {
		object-fit: cover;
		height: 100%;
		width: 100%;
	}

	@media (max-width: 767px) {
		.class-capa-mobile {
			display: block;
		}

		.class-capa-desktop {
			display: none;
		}

	}

	@media only screen and (max-width: 1022px) {
		.barra-mobile {

			display: flex;
		}

		.emp__infos .icones {
			display: flex;
			flex-direction: column;
			margin: 30px 0;
		}
	}

	@media only screen and (min-width: 767px) {
		.emp__galeria {
			height: auto;
		}

		.emp__galeria {

			height: auto;
		}

	}




@media (max-width: 920px) {
.emp-grid {
    width: 100%;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 20px;
    flex-wrap: wrap;
    margin-bottom: 3.75rem;
    flex-direction: column;
}

}
@media (max-width: 576px) {
.emp-grid {
    width: 100%;
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: 20px;
    flex-wrap: wrap;
    margin-bottom: 3.75rem;
    flex-direction: column;
}

}
	
	.component-pop-up-form{
	display:none;
    position: fixed;
    flex-direction: column;
    justify-content: center;
    position: fixed;
    width: 100%;
    top: 0;
    margin: 0;
    height: 100%;
    background: rgb(0,0,0,0.8);
    z-index: 99999999999999999999999999999999;
    text-align: center;
	}
	
	.component-section-close{
		top: 0;
		position: absolute;
		right: 28px;
		background: #ff0101;
		color: #fff;
		border-radius: 0;
		width: 30px;
		font-size: 1rem;
    	padding: 6px;
		cursor:pointer;
	}
	
	.component-section-close:hover{
		background: #980202;
	}

</style>








<div id="phone-modal" class="white-popup mfp-with-anim mfp-hide">
	<div class="form-interesse__flex">
		<div class="header-tit header-tit--pages">
			<h2 class="tit-border">Entre em Contato</h2>
			<div class="header-tit__desc">Escolha a unidade mais proxima e entre em contato com a </br>WR Engenharia.</div>
		</div>
		<div class="contatos-modal-wrap">
			<a href="tel:+55 (85)3458.1246">Fortaleza</a>
			<a href="tel:+55 (88)2101.0050">Juazeiro do Norte</a>
		</div>

	</div>
</div>












<footer class="rdp <?php echo (wp_is_mobile() ? 'rdp--mobile' : '') ?>">
	<div class="container">
		<div class="rdp__flex">
			<?php if (!wp_is_mobile()) : ?>
				<div class="rdp__infos">
					<a href="<?php echo get_site_url(); ?>">
						<h2 class="logo">
							<img width="128" height="76" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-rdp.png" alt="">
						</h2>
					</a>

					<div class="rdp__end">
						<i class="icon icon-pin"></i>

						<div class="rdp__end-itens">
							<?php foreach ($settings['group_ends'] as $end) : ?>
								<address class="end">
									<p><?php echo $end["end_logradouro"] . ' - ' . $end["end_bairro"] . ' - ' . $end["end_cidade"] . ' - ' . $end["end_estado"] . ' - CEP: ' . $end["end_cep"]; ?></p>
									<?php if ($end["end_telefone"]) : ?>
										<strong class="end__tel">Tel: <?php echo $end["end_telefone"]; ?></strong>
									<?php endif; ?>
								</address>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>


			<a href="#" class="btn-resp-rdp">Exibir Menu</a>

			<div class="rdp__menus <?php echo (wp_is_mobile() ? 'rdp__menus--mobile' : '') ?>">
				<nav class="menu menu--rdp">
					<?php wp_nav_menu(array('theme_location' => 'menu_2', 'menu_class' => 'menu-rdp')); ?>
				</nav>
			</div>
		</div>
	</div>
	<?php if (wp_is_mobile()) : ?>
		<div class="rdp__infos rdp__infos--mobile">
			<div class="">
				<div class="rdp__end-itens">
					<?php foreach ($settings['group_ends'] as $end) : ?>
						<address class="end">
							<p><?php echo $end["end_logradouro"] . ' - ' . $end["end_bairro"] . ' - ' . $end["end_cidade"] . ' - ' . $end["end_estado"] . ' - CEP: ' . $end["end_cep"]; ?></p>
							<?php if ($end["end_telefone"]) : ?>
								<strong class="end__tel">Tel: <a href="tel:<?php echo $end["end_telefone"]; ?>"><?php echo $end["end_telefone"]; ?></a></strong>
							<?php endif; ?>
						</address>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="rdp__cred">
		<div class="container">
			<div class="rdp__cred__flex">
				<p>© Todos os direitos reservados para WR Engenharia <?php echo date("Y"); ?>.</p>

				<ul class="midia midia--cinza">
					<?php if ($settings['settings_facebook_url']) : ?>
						<li class="midia__item">
							<a href="<?php echo $settings['settings_facebook_url']; ?>" target="_blank" class="midia__link">
								<i class="icon icon-facebook"></i>
							</a>
						</li>
					<?php endif; ?>

					<?php if ($settings['settings_instagram_url']) : ?>
						<li class="midia__item">
							<a href="<?php echo $settings['settings_instagram_url']; ?>" target="_blank" class="midia__link">
								<i class="icon icon-instagram"></i>
							</a>
						</li>
					<?php endif; ?>

					<?php if ($settings['settings_youtube_url']) : ?>
						<li class="midia__item">
							<a href="<?php echo $settings['settings_youtube_url']; ?>" target="_blank" class="midia__link">
								<i class="icon icon-youtube"></i>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
	<div class="cred">
		<a href="http://www.convertte.com.br" target="_blank" class="cred__logo" title="Convertte: Agência digital em Fortaleza">Desenvolvido por <span class="empresa notranslate"></span></a>
	</div>
</footer>


<div id="popup-orange" class="mfp-modal mfp-modal--orange mfp-modal--big mfp-modal--table mfp-with-anim mfp-hide ">
	<div class="msg-form">
		<span class="close-btn"><b>X</b></span>
	</div>


	<div class="mfp-modal-coldir">
		<div class="box-center">
			<h3 class="title-modal">Deseja receber notificações?</h3>
			<?php echo do_shortcode('[contact-form-7 id="2211" title="Modal Lead Blog"]'); ?>
			<a class="popup-modal-dismiss" href="javascript:void(0)">Não exibir novamente.</a>
		</div>
	</div>
</div>



<!-- 
// SCRIPT DO POPUP 
<script type="text/javascript">
jQuery(function($){
	$(document).ready(function(){
		  	
		var _html = document.documentElement;
		var hasModalOrange = $.cookie('modalOrange');


		_html.addEventListener('mouseleave', function(e){
			
			var teste = $('body > div').hasClass('mfp-ready');

			if(!teste) {				
				if ( e.clientY < 20 ) {

					if( !hasModalOrange  ) {
						
						activeModal('#popup-orange');
					}				
				}						
			}

					
		});

	    function activeModal(modal){
	    	$.magnificPopup.open({
			  	items: {
			      src: modal
			    },
				type: 'inline',
				preloader: false,    
				modal: true,
				removalDelay: 300,
				callbacks: {
			    
			    beforeOpen: function() {
			        this.st.mainClass = 'mfp-zoom-in';
			    }


			  },
			});
	    }

		$(document).on('click', '.close-btn', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});

		$(document).on('click', '.popup-modal-dismiss', function (x) {
			x.preventDefault();
			$.magnificPopup.close();
			
			if( !hasModalOrange ) {
				$.cookie('modalOrange', 'yes', {expires: 1, path: '/'});
				hasModalOrange = $.cookie('modalOrange');
			
			}	
			
		}); 


   		

		 
	});  
});
</script> -->

<?php //echo $scriptFooter; ?>


<?php if (isset($_GET['pageid'])) {
	echo $scriptFooter_rdrct;
} ?>

<?php wp_footer(); ?>

<!-- Integração Anapro CRM -->
    <script>
        jQuery(function($) {
            $(document).ready(function() {

                var url_forms = $('.logo').attr('href');
                var cod_key = $('#page-single-empreendimento').attr('data-cod');
                var cod_camp = $('#page-single-empreendimento').attr('data-campanha');


                //function sendWebHookTest() {
                  //  location = 'https://webhook.site/78e6f2f9-7875-4be4-a4e9-5d6006a0552e';
               // }

                function postDadosCrm() {
                    var form_page = $('.wpcf7').parent().attr('data-emp');
                    var name = $('#nome').val();
                    var email = $('#email').val();
                    var fone = $('#telefone').val();
                    var msg = $('#msg').val();
                    var midia = 'Site WR : ' + form_page;

                    $.ajax({
                        "url": "http://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect",
                        "type": "POST",
                        "dataType": "json",
                        "data": {
                            "Key": "qo71iUga7bk1",
                            "TagAtalho": "",
                            "CampanhaKey": cod_camp,
                            "ProdutoKey": cod_key,
                            "CanalKey": "2WUgVVkqtqI1",
                            "Midia": midia,
                            "Peca": "",
                            "UsuarioEmail": "",
                            "GrupoPeca": "",
                            "CampanhaPeca": "",
                            "PessoaNome": name,
                            "PessoaSexo": "",
                            "ValidarEmail": "false",
                            "PessoaEmail": email,
                            "ValidarTelefone": "false",
                            "PessoaTelefones": [{
                                "Tipo": "OUTR",
                                "Numero": fone,
                                "Ramal": null
                            }, ],
                            "Observacoes": msg,
                            "KeyExterno": "",
                            "UsarKeyExterno": "false",
                            "KeyIntegradora": "FF083C78-C876-4D34-AE90-F5598F93BF6A",
                            "KeyAgencia": "fa590925-b8ad-4027-8f43-8c86a2e8e79c"
                        },

                        beforeSend: function() {
                            console.log('enviando...');
                        },

                        success: function(res) {
                            console.log(res);
                            var page_Empreendimento = $("#page-single-empreendimento").data('emp');
                            location = url_forms + '/solicitacao-enviada-com-sucesso/?origem-empreendimento=' + page_Empreendimento + '&pageid=' + <?php echo get_the_ID(); ?>;

                        }
                    });

                    return false;
                }
                
               

                document.addEventListener('wpcf7mailsent', function(event) {
                    if ('3528' == event.detail.contactFormId) {
                        alert("The contact form ID is 123.");
                        // do something productive
                    };
                    // FORM POPUP E TENHO INTERESSE
                    if ('165' == event.detail.contactFormId) {
                        postDadosCrm();
                        
                        //var url_forms      = $(".wr-header__logo a").attr("href");
                        //var empreendimento = $("#page-single-empreendimento").data("emp");
                        
                        //location = url_forms + "/solicitacao-enviada-com-sucesso/?origem=empreendimento=" +empreendimento;
            
                    }



                }, false);
            });
        });
    </script>
<div class="component-pop-up-form">
    <header class="wr-headline wr-headline--center wr-headline--white" style="margin-top:9%;">
        <span class="wr-headline__line">
            Tem interesse? então tire suas dúvidas
        </span>
        <h3 class="wr-headline__title">
            Fale com nosso time de vendas
        </h3>

    </header>
    <div class="form form--col-5" data-emp="<?php echo get_the_title(); ?>">
        <?php echo do_shortcode('[contact-form-7 id="165" title="EMP - Tenho Interesse"]'); ?>
    </div>
    <p class="component-section-close">X</p>
</div>

<script>
jQuery(function($) {
    $(document).ready(function() {

        $('.component-empreendimento-pop').on('click', function (e) {
            e.preventDefault();
            var b = $('.component-pop-up-form').css("display");
            // if(b === "none"){
                $('.component-pop-up-form').show();
                // $('.component-pop-up-form').css({display, "flex"});
            // }
        })
    })

    

    $(document).ready(function() {
        $('.component-section-close').on('click', function (e) {
            $('.component-pop-up-form').hide();
        });

	})

})

        
</script>
</body>

</html>