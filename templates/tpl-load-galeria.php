<?php
	// get_template_part('templates/html','header');
	/* Template Name: Load Carousel */
	$categorias = get_terms(array(
		'taxonomy' => 'categorias',
		'hide_empty' => false
	));

	foreach ( $categorias as $cat ) :
?>

<?php
	$i = 1;
	$args = array(
		'post_type'  => 'empreendimento',
	    'tax_query' => array(
	        'relation'  => 'OR',
	        array(
	            'taxonomy'         => 'categorias',
	            'field'            => 'slug',
	            'terms'            => array($cat->slug),
	        ),
	    ),
	);
	$emp = new WP_Query($args);
?>
<div id="<?php echo $cat->slug; ?>">
	<div class="owl-emp-gal">
		<?php
		while ($emp->have_posts()) : $emp->the_post();
			get_template_part('templates/empreendimento/html', 'emp-galeria');
		$i++; endwhile; wp_reset_postdata(); ?>
	</div>
</div>

<?php endforeach; ?>

<?php //get_template_part('templates/html','footer');?>