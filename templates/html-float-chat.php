<?php

$settings = get_option('options_gerais');

// COD. DO EMPREENDIMENTO | COD. DA CAMPANHA
$cod_emp = get_post_meta( get_the_ID(), 'key_emp_produto', true);
$cod_cam = get_post_meta( get_the_ID(), 'key_emp_campanha', true);
?>

<div class="wr-float-chat onView">
    <div class="text-wrap">

        <p class="titulo">Precisa de ajuda?
        </p>
        <p class="subtit">
            Fale agora com nossos corretores!
        </p>
    </div>
    <div class="icon-wrap">


        <?php
        $whatsapp = $settings['settings_whatsapp'];
        $search    = explode(",", "-,., ");
        $replace   = explode(",", "");
        ?>





        <a class="wpp" href="https://api.whatsapp.com/send?1=pt_BR&phone=55<?php echo   str_replace($search, $replace, $whatsapp); ?>" target="_blank"></a>


        <?php if (!is_front_page() && !is_archive()) : ?>
            <a class="chat open-in-window" link-to-open="https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=qo71iUga7bk1&keyIntegradora=FF083C78-C876-4D34-AE90-F5598F93BF6A&keyAgencia=fa590925-b8ad-4027-8f43-8c86a2e8e79c&strDir=wrengenharia&campanha=<?php echo $cod_cam; ?>&canal=47gNJL2lQr01&produto=<?php echo $cod_emp; ?>&strmidia=<?php echo $strmidia; ?>&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=true&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=" title-window="Chat - WR Engenharia" href="#"></a>


        <?php else : ?>

            <a class="chat open-in-window" link-to-open="https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=qo71iUga7bk1&keyIntegradora=FF083C78-C876-4D34-AE90-F5598F93BF6A&keyAgencia=fa590925-b8ad-4027-8f43-8c86a2e8e79c&strDir=wrengenharia&campanha=Vog7zM7PAvk1&canal=47gNJL2lQr01&produto=ZXNMtGkoh9s1&strmidia=<?php echo $strmidia_home; ?>&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=true&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=" title-window="Chat - WR Engenharia" href="#"></a>


        <?php endif; ?>






    </div>
</div>

<script>
    function PopupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var systemZoom = width / window.screen.availWidth;
        var left = (width - w) / 2 / systemZoom + dualScreenLeft
        var top = (height - h) / 2 / systemZoom + dualScreenTop
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) newWindow.focus();
    }

    jQuery(function($) {
        $(document).ready(function() {



            $.fn.isVisible = function() {
                // Current distance from the top of the page
                var windowScrollTopView = $(window).scrollTop();

                // Current distance from the top of the page, plus the height of the window
                var windowBottomView = windowScrollTopView + $(window).height();

                // Element distance from top
                var elemTop = $(this).offset().top;

                // Element distance from top, plus the height of the element
                var elemBottom = elemTop + $(this).height();

                return ((elemBottom <= windowBottomView) && (elemTop >= windowScrollTopView));
            }
            $(window).scroll(function() {
                if ($(".footer-target").isVisible()) {
                    $(".wr-float-chat").addClass('onView');
                } else {
                    $(".wr-float-chat").removeClass('onView');
                }
            });








            $('.open-in-window').on('click', function(evt) {
                evt.preventDefault()
                var url = $(this).attr('link-to-open');
                var title = $(this).attr('title-window');
                var size = $(this).attr('size-window');
                PopupCenter(url, title, '570', '670');
            })
        })
    })
</script>