<?php 
	$img = get_post_meta( get_the_id(), 'imagem_topo', true);
	$imgObj =  wp_get_attachment_image_src($img, 'full');
?>
<header class="header-img">
    <img src="<?php echo $imgObj[0]; ?>" width="<?php echo $imgObj[1]; ?>" height="<?php echo $imgObj[2]; ?>">
</header>