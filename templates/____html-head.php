<?php
// COD. CUSTON SCRIPTS 
$scriptHead = get_post_meta(get_the_ID(), 'script_head', true);
$scriptBody = get_post_meta(get_the_ID(), 'script_body', true);

?>


<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="google-site-verification" content="bk7Ag1jIiacSE-roNXMOJqZ89A526c7JosrIfsfF0FY" />
    <link rel="alternate" href="<?php echo get_bloginfo('url'); ?>" hreflang="pt-BR" />
    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">

    <?php /**/ ?>
    <?php $dados_crm    = get_post_meta(get_the_id(), 'cod_crm', true); ?>
    <?php $dados_filial = get_post_meta(get_the_id(), 'filial_crm', true); ?>

    <?php if (!$dados_filial && is_tax('cidades', 'fortaleza')) : $dados_filial = '643';
    endif; ?>

    <!-- <script src="https://wr.housecrm.com.br/track/origem.js" async></script>
    <?php /**/ ?>
    <script>
    var hc_dominio_chat = 'wr.housecrm.com.br';
    var hc_filial = '<?php //echo ($dados_filial ? $dados_filial : "");
                        ?>';
    var hc_empreendimento = '<?php //echo ($dados_crm ? $dados_crm : ""); 
                                ?>';
    var hc_color = 'red';
    var hc_https = 1;

    function leadEmailSuaHouse() {
        var $form = document.querySelector('.wpcf7'),
            $nome = document.getElementById('nome').value,
            $email = document.getElementById('email').value,
            $fone = document.getElementById('telefone').value,
            $msg = document.getElementById('msg').value;

        var ret = hc_envia_mensagem(hc_empreendimento, $nome, $email, '', $fone, $msg, hc_filial);
        var obj = JSON.parse(ret);

        if (obj['sucesso']) {
            var href = window.location.href;
            location.href = href + "?sucessocontato";
        }
    }

    function leadCaririStoreSuaHouse() {
        var $hc_empreendimento = document.getElementById('emp_code').value,
            $hc_filial = document.getElementById('filial_code').value,
            $form = document.querySelector('.wpcf7'),
            $nome = document.getElementById('nome').value,
            $email = document.getElementById('email').value,
            $fone = document.getElementById('telefone').value,
            $msg = document.getElementById('msg').value;

        var ret = hc_envia_mensagem($hc_empreendimento, $nome, $email, '', $fone, $msg, $hc_filial);
        var obj = JSON.parse(ret);

        if (obj['sucesso']) {
            var href = window.location.href;
            location.href = href + "?sucessocariri";
        }
    }
    </script> -->
    <!-- Google Tag Manager -->
    <script>
        /*=========================================================================================
// INICIO MAIN JS
========================================================================================= */
        jQuery(function($) {
            $(document).ready(function() {
                function aspectRatio(selector) {
                    height = $(selector).width() * 9 / 16;
                    $(selector).css('height', height + 'px');



                };

                function squareDiv(selector) {


                    $(selector).each(function() {
                        $(this).css('height', $(this).width() + 'px');
                    });


                }


                $(window).on('load resize', function() {
                    aspectRatio('.wr-empreendimento__video .container .thumb-video')
                    // aspectRatio('.mfp-content')
                    // squareDiv('.grid-container');

                });






            })
        })
    </script>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TXH2DX4');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Integração Anapro CRM -->
    <script type="text/javascript">
        jQuery(function($) {
            $(document).ready(function() {

                var url_forms = $('.logo').attr('href');
                var cod_key = $('#page-single-empreendimento').attr('data-cod');
                var cod_camp = $('#page-single-empreendimento').attr('data-campanha');

                function postDadosCrm() {
                    var form_page = $('.wpcf7').parent().attr('data-emp');
                    var name = $('#nome').val();
                    var email = $('#email').val();
                    var fone = $('#telefone').val();
                    var msg = $('#msg').val();
                    var midia = 'Site WR : ' + form_page;

                    $.ajax({
                        "url": "http://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect",
                        "type": "POST",
                        "dataType": "json",
                        "data": {
                            "Key": "qo71iUga7bk1",
                            "TagAtalho": "",
                            "CampanhaKey": cod_camp,
                            "ProdutoKey": cod_key,
                            "CanalKey": "2WUgVVkqtqI1",
                            "Midia": midia,
                            "Peca": "",
                            "UsuarioEmail": "",
                            "GrupoPeca": "",
                            "CampanhaPeca": "",
                            "PessoaNome": name,
                            "PessoaSexo": "",
                            "ValidarEmail": "false",
                            "PessoaEmail": email,
                            "ValidarTelefone": "false",
                            "PessoaTelefones": [{
                                "Tipo": "OUTR",
                                "Numero": fone,
                                "Ramal": null
                            }, ],
                            "Observacoes": msg,
                            "KeyExterno": "",
                            "UsarKeyExterno": "false",
                            "KeyIntegradora": "FF083C78-C876-4D34-AE90-F5598F93BF6A",
                            "KeyAgencia": "fa590925-b8ad-4027-8f43-8c86a2e8e79c"
                        },

                        beforeSend: function() {
                            //console.log('enviando...');
                        },

                        success: function(res) {
                            //console.log(res);
                            var page_Empreendimento = $("#page-single-empreendimento").data('emp');
                            location = url_forms + '/solicitacao-enviada-com-sucesso/?origem=empreendimento=' + page_Empreendimento;

                        }
                    });

                    return false;
                }

                document.addEventListener('wpcf7mailsent', function(event) {

                    // FORM POPUP E TENHO INTERESSE
                    if ('165' == event.detail.contactFormId) {
                        postDadosCrm();
                    }


                }, false);
            });
        });
    </script>
    <!-- Facebook Pixel Code -->

    <script>
        ! function(f, b, e, v, n, t, s)

        {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?

                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };

            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';

            n.queue = [];
            t = b.createElement(e);
            t.async = !0;

            t.src = v;
            s = b.getElementsByTagName(e)[0];

            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',

            'https://connect.facebook.net/en_US/fbevents.js');


        fbq('init', '685618165627589');

        fbq('track', 'PageView');
    </script>

    <noscript>

        <img height="1" width="1" src="https://www.facebook.com/tr?id=685618165627589&ev=PageView

&noscript=1" />

    </noscript>

    <!-- End Facebook Pixel Code -->

    <?php echo $scriptHead; ?>

</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TXH2DX4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php echo $scriptBody; ?>