<?php $urlEmp = get_template_directory_uri().'/assets/images/emp/'; ?>
<?php if(!wp_is_mobile()): ?>
<section class="abas abas--imoveis">
	<header class="abas__header">
		<h2 class="abas__tit">Imóveis WR Engenharia</h2>

		<ul class="tab">
			<?php
				$i = 1;
				$categorias = get_terms( array(
				    'taxonomy' => 'categorias',
				    'hide_empty' => false,
				    'exclude' => array(8),
				));
				$my_new_array = array();
				foreach ($categorias as $term) {
				    $issue_date  = get_term_meta( $term->term_id, 'tax_ordem', true );
				    $my_new_array[$issue_date] = $term;
				}
				ksort( $my_new_array, SORT_NUMERIC );
				foreach ( $my_new_array as $cat ) :
			?>
			<li class="tab__item">
				<a href="<?php echo get_page_link(194); ?>" data-ajax="gal" data-ancora="<?php echo $cat->slug; ?>" class="tab__link <?php echo ( $i == 1 ) ? 'active': ''; ?>">
					<?php echo $cat->name; ?>
				</a>
			</li>
			<?php $i++; endforeach; ?>
			<li class="tab__item">
				<a  class="tab__link" href="<?php echo get_term_link('avulsos', 'categorias' ); ?>">Avulsos</a>
			</li>
		</ul>
	</header>
	<div class="container">
		<div id="ajax-content"></div>
		<?php //get_template_part('templates/tpl', 'load-galeria'); ?>
	</div>
</section>
<?php endif; ?>