<aside class="respo-page__sidebar">
    <ul class="list list--cat">
    <h3 class="list__tit">Notícias WR</h3>
    <?php //PEGA LISTA DE CATEGORIAS DE RECEITAS
        $args = array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => false);
        $categorias = get_categories($args);
        if (!empty($categorias) && !is_wp_error($categorias)) {
            foreach ( $categorias as $categoria ) {
                echo "<li class='list__item'>";
                echo "<a href='".get_category_link($categoria->term_id)."' class='list__link'>" . $categoria->name . "</a>";
                echo "</li>";
            }
        }
    ?>
    </ul>
    <ul class="list-midia">
        <h3 class="list__tit">Siga a WR</h3>
        <li class="list-midia__item">
            <a href="https://www.facebook.com/wrengenhariaoficial" target="_blank"><i class="fa fa-facebook"></i></a>
        </li>
        <li class="list-midia__item">
            <a href="https://instagram.com/wrengenharia/" target="_blank">
                <i class="fa fa-instagram"></i>
                </a>
        </li>
        <li class="list-midia__item">
            <a href="http://www.youtube.com/channel/UCXVZOHJyNLibL0dDvxzHgYw" target="_blank"><i class="fa fa-youtube"></i></a></li>
        <li class="list-midia__item">
            <a href="#" target="_blanc"><i class="fa fa-linkedin"></i></a></li>
    </ul>
    <div class="ultimos-artigos">
        <h3 class="ultimos-artigos__tit">Últimos Artigos</h3>
        <?php
            $exclude_id = get_the_ID();
            $lidos = new WP_Query(array('post_type'=> 'post', 'posts_per_page' => 4,  'post__not_in' => array($exclude_id), 'orderby' => 'date', 'order' => 'DESC'));
            while ($lidos->have_posts()) : $lidos->the_post();
        ?>
        <div class="ultimos-artigos__itens">
            <h4 class="info">
                <a href="<?php the_permalink(); ?>" class="link" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h4>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
</aside>