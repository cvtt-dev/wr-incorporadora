<?php //slides frontpage
$args = array('posts_per_page' => -1, 'post_type' => 'slides');
$header = new WP_Query($args);
?>
<div class="topo__banner">
    <div id="owl-slides" class="slides">
        <?php while ($header->have_posts()) : $header->the_post();
            $slide_desc        = get_post_meta(get_the_id(), 'slide_desc', true);
            $slide_aligne       = get_post_meta(get_the_id(), 'slide_bloco_alinhamento', true);
            $slide_tex_ativo       = get_post_meta(get_the_id(), 'slide_texto_ativo', true);
            $slide_title       = get_post_meta(get_the_id(), 'slide_title', true);
            $slide_link        = get_post_meta(get_the_id(), 'slide_link', true);
            $slide_target      = get_post_meta(get_the_id(), 'slide_target', true);
            $slide_btn         = get_post_meta(get_the_id(), 'slide_btn', true);
        ?>
            <div class="slides__box">
                <div class="slides__infos">

                    <?php if ($slide_tex_ativo != "Sim") : ?>
                        <div class="container">
                            <div class="slides__bloco slides__bloco--<?php echo $slide_aligne;?>">
                                <h3 class="slides__titulo slides__titulo--small"><?php echo get_the_title(); ?></h3>
                                <h2 class="slides__titulo"><?php echo $slide_title; ?></h2>
                                <div class="slides__desc"><?php echo $slide_desc; ?></div>

                                <a class="slides__btn" href="<?php echo $slide_link; ?>" target="<?php echo $slide_target; ?>"><?php echo $slide_btn; ?></a>
                            </div>
                        </div>
                    <?php else : ?>
                        <a class="btn_full_slide" href="<?php echo $slide_link; ?>" target="<?php echo $slide_target; ?>"></a>
                    <?php endif; ?>

                </div>
                <div class="slides__thumb <?php echo $slide_tex_ativo == "Sim" ? "remove_shadow" : "";?>" style="--bgImg:url(<?php echo the_post_thumbnail_url( 'medium' ); ?>)">
                    <?php //owllazy( get_the_id(), 'full',  get_the_title()); 
                    ?>
                    <?php the_post_thumbnail('full');  ?>
                    <?php //the_post_thumbnail('', array('alt' => , 'title' => get_the_title()));
                    ?>
                </div>
            </div>
        <?php endwhile;
        wp_reset_postdata(); ?>
    </div>
</div>


<style>a.btn_full_slide {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 9999;
}</style>