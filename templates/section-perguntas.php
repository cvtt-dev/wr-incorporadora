<section class="perguntas box-gray">
	<div class="container">		
		<div class="perguntas__flex">
			<?php 
				$args = array(
					'post_type' => 'page',
					'p' => 65,
					'post_per_page' => 1
				); 
				$pesquisas = new WP_Query($args);
				if ($pesquisas->have_posts()) :					
				while($pesquisas->have_posts()) : $pesquisas->the_post(); 
			?>
			<div class="pesquisa">
				<figure class="pesquisa__thumb">
					<?php echo thumblazy(get_the_id(), 'full', 'fade', get_the_title());?>
				</figure>

				<div class="pesquisa__infos">
					<h2 class="pesquisa__tit"><?php echo get_the_title(); ?></h2>
					<div class="pesquisa__desc">
						<?php echo get_the_content(); ?>
					</div>

					<a href="<?php echo get_the_permalink(); ?>" class="pesquisa__btn">Responder Pesquisa</a>
				</div>
			</div>
			<?php endwhile; wp_reset_postdata(); endif; ?>

			<div class="perguntas__col">
				<div class="perguntas__col__header">
					<strong class="perguntas__subtit">Perguntas frequentes</strong>
					
					<div class="perguntas__headline">
						<h2 class="perguntas__tit">Tire todas as suas dúvidas sobre nosso procendimento.</h2>
						<a href="<?php echo get_post_type_archive_link('perguntas'); ?>" class="perguntas__btn">Ler todas</a>
					</div>
				</div>
				
				<?php 
					$args = array(
						'post_type' => 'perguntas',
						'showposts' => 4,						
					);
					$perguntas = new WP_Query($args);
					if ($perguntas->have_posts()) :
				?>
				<div class="accordion">
					<div class="accordion-section">
						<?php 
							$i=1;
							while($perguntas->have_posts()) : $perguntas->the_post(); 
						?>
						<a class="accordion-title <?php echo ( $i == 1 ) ? 'active': ''; ?>" href="#acc_<?php echo $i;?>">
							<strong class="ac"><?php echo get_the_title(); ?></strong>
							<span class="accordion-title-icon"></span>
						</a>

						<div id="acc_<?php echo $i;?>" class="accordion-content">
						     <?php echo get_the_content(); ?>
						</div>
						<?php $i++; endwhile; wp_reset_postdata(); ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>