<div class="area-cliente-box">
    <div class="box-form">
    <a href="<?php echo get_bloginfo('url');?>/area-do-cliente" class="btn-area btn-area__cliente <?php if($_SESSION['xml']) : echo 'btn-area__cliente--logado'; endif; ?>"><i class="icon icon-pessoas"></i> Área do Cliente</a>
    <form id="area-do-cliente2" class="form form--login login" method="POST" onsubmit="return false;">
        <div class="login__msg"></div>
        <input id="login2" type="text" name="cpf-login" class="form__input" placeholder="CPF ou Código do Cliente"/>
        <input id="senha2" type="password" name="senha" class="form__input" placeholder="Senha" />
        <button type="subtmit" class="form__input form__input--btn">Enviar <i class="fa fa-cog fa-spin login__load"></i></button>
    </form>
    </div>
</div>