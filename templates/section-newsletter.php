<section class="newsletter">
	<div class="container">		
		<div class="newsletter__flex">			
			<div class="newsletter__tit-box">
				<i class="icon icon-carta"></i>	
				<h2 class="newsletter__tit">Cadastre-se para receber novidades e condições especiais</h2>
			</div>

			<div class="newsletter__form">
				<div class="form">
					<?php echo do_shortcode('[contact-form-7 id="21" title="FORM - Newsletter"]'); ?>
				</div>
			</div>
		</div>
	</div>
</section>