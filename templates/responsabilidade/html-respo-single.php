<?php
	$tit = get_the_title();
	$link = get_the_permalink();
	$data = get_the_date('d/m/Y');
?>
<div class="resp-card resp-card--single">
	<div class="resp-card__thumb resp-card__thumb--single <?php if (!has_post_thumbnail()) {echo 'resp-card__thumb--noimage';} ?>">
		<?php
			if ( has_post_thumbnail() ) {
			    the_post_thumbnail('thumb-745x380', array('class' => 'resp-card__thumb__img'));
			} else {
			    echo '<img src="'.get_template_directory_uri().'/assets/images/thumbnail-default.jpg" class="resp-card__thumb__img" width="745" height="380" />';
			}
		?>
	</div>

	<?php if ($data) : ?>
	<div class="resp-card__calendar">
		<i class="icon icon-calendar"></i>
		<strong class="resp-card__calendar__tit"><?php echo $data; ?></strong>
	</div>
	<?php endif; ?>

	<h2 class="resp-card__tit"><?php echo $tit; ?></h2>

	<div class="resp-card__desc">
		<?php the_content(); ?>
	</div>
</div>