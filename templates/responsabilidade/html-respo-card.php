<?php
	$tit = get_the_title();
	$link = get_the_permalink();
	$data = get_the_date('d/m/Y');
?>
<div class="resp-card">
	<div class="resp-card__thumb">
		<a href="<?php echo $link; ?>" title="<?php echo $tit; ?>">
		<?php
			if ( has_post_thumbnail() ) {
			    the_post_thumbnail('thumb-340x245', array('class' => 'resp-card__thumb__img'));
			} else {
			    echo '<img src="'.get_template_directory_uri(). '/assets/images/thumbnail-default.jpg" class="resp-card__thumb__img" />';
			}
		?>
		</a>
	</div>

	<?php if ($data) : ?>
	<div class="resp-card__calendar">
		<i class="icon icon-calendar"></i>
		<strong class="resp-card__calendar__tit"><?php echo $data; ?></strong>
	</div>
	<?php endif; ?>
	<div class="resp-card__infos">
		<h2 class="resp-card__tit">
			<a href="<?php echo $link; ?>" title="<?php echo $tit; ?>" class="resp-card__tit__link"><?php echo $tit; ?></a>
		</h2>
		<div class="resp-card__desc">
			<?php echo get_excerpt_nbtn('120'); ?>
		</div>
	</div>
	<a href="<?php echo $link; ?>" class="resp-card__btn">Ler mais</a>
</div>