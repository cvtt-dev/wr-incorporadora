<?php get_template_part('templates/html','header'); ?>
<article class="pages pages--empreendimento">
    <header class="header-img">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/capa-empreendimentos.jpg" alt="">
	</header>
	<div class="container">
		<div class="abas__header abas__header--branco">
			<div class="header-tit header-tit--small">
				<h2 class="abas__tit tit-border">EMPREENDIMENTOS WR: <?php single_term_title(); ?></h2>
			</div>
		</div>
	</div>
	<?php
		$args = array('post_type'  => 'empreendimento', 'posts_per_page' => -1);
		global $wp_query; query_posts( array_merge($wp_query->query, $args));
	?>
	<div class="container">
		<div id="empreendimento-content" class="emp-grid animated fadeIn">
			<?php
				if (have_posts()) :
					while ( have_posts() ) : the_post();
						get_template_part('templates/empreendimento/html', 'emp-vertical');
					$i++; endwhile; wp_reset_postdata();
				else :
					echo '<div class="msg"><p>Nenhum empreendimento foi encontrado!</p></div>';
				endif;
			?>
		</div>
	</div>
</article>
<?php get_template_part('templates/html','footer');?>