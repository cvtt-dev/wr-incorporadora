<?php /* Template Name: Portfólio  */ ?>
<?php
get_template_part('templates/html', 'header');
$capa_mobile = get_post_meta(get_the_ID(), 'meta-thumbnail_mobile', false);
$capa_desktop = get_post_meta(get_the_ID(), 'meta-thumbnail_desktop', false);
?>

<article class="page-portfolio">
	<header class="header-img">
	<?php
            echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_mobile[0]) . '" class="class-capa-mobile header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_mobile[0]) . '">';
            if ($capa_desktop) {
                echo '<img width="1920" height="367" alt="" data-src="' . wp_get_attachment_url($capa_desktop[0]) . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . wp_get_attachment_url($capa_desktop[0]) . '">';
            } else {
                echo '<img width="1920" height="367" alt="" data-src="' . the_post_thumbnail_url() . '" class="class-capa-desktop header-img__img wp-post-image lazyloaded" src="' . the_post_thumbnail_url() . '">';
            }
            ?>
	</header>
	<section class="portfolio">

		<div class="container">
			<div class="header-tit">
				<h2 class="tit-border">Portfólio</h2>
			</div>
			<ul class="lista-portfolio">

				<?php
				$args = array('posts_per_page' => -1, 'post_type' => 'portfolios', 'meta_key' => 'lancamento_emp', 'orderby' => 'meta_value', 'order' => 'DESC');
				$postfolio = new WP_Query($args);
				$datas =  array();
				while ($postfolio->have_posts()) : $postfolio->the_post();
					$data = get_post_meta(get_the_ID(), 'lancamento_emp', true);
					$logo_do_emp = get_post_meta(get_the_ID(), 'logo_do_emp', true);
					$newdata = date('Y', strtotime($data));

					// echo $newdata;
					$datas[$newdata][] = get_the_ID();

				endwhile;

				foreach ($datas as $data => $d) {

					//	echo '<h3 class="ano-portfolio">'.$data.'</h3>';


					foreach ($d as $data) {
						echo '<li>
							<figure>
								' . get_the_post_thumbnail($data, 'full', array('alt' => get_the_title())) . '								
								<div class="date-portfolio">
									<h3>' . get_the_title($data) . '</h3>								
								</div>
							</figure>				
						</li>';
					}
				}



				wp_reset_postdata(); ?>


			</ul>
		</div>
	</section>



</article>


<?php get_template_part('templates/html', 'footer'); ?>