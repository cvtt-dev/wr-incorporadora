<?php /* Template Name: Fornecedores  */ ?>
<?php
get_template_part('templates/html','header');
the_post();
$instrucoes = get_post_meta( get_the_id(), 'etapa_for', true);
?>

<article class="pages pages--fornecedores">
    <header class="header-img">
        <?php the_post_thumbnail('full', array('class' => 'header-img__img')); ?>
    </header>

    <div class="container">
        <div class="header-tit header-tit--pages">
            <h2 class="tit-border"><?php the_title();?></h2>

            <div class="header-tit__desc">
            	<?php the_content();?>
            </div>
        </div>

        <div class="fornecedores-pages">
			<div class="fornecedores-pages__instrucoes">
				<h2 class="fornecedores-pages__tit">Siga as Instruções abaixo:</h2>

				<ol class="instrucoes">
					<?php foreach ( $instrucoes as $ins ) : ?>
					<li class="instrucoes__item"><?php echo $ins['desc_etapa']; ?></li>
					<?php endforeach; ?>
				</ol>
			</div>

			<aside class="fornecedores-pages__download">
				<div class="fornecedores-pages__download__header">
					<h2 class="tit tit--destaque">Download</h2>
					<h3 class="tit tit--subtit">Ficha de fornecedores</h3>
				</div>

				<ul class="download">
					<?php
						$args = array('post_type' => 'downloads', 'showposts' => -1);
						$download = new WP_Query( $args );

						while ($download->have_posts()) : $download->the_post();
							$arq_download = get_post_meta( get_the_id(), 'arquivo_download', true);

					?>
					<li clas="download__item">
						<a href="<?php echo wp_get_attachment_url($arq_download); ?>" download="<?php echo get_the_title(); ?>" target="_blank" class="download__link">
							<i class="icon icon-download"></i>
							<b><?php echo get_the_title(); ?></b>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</aside>
        </div>
    </div>
</article>

<?php get_template_part('templates/html','footer');?>